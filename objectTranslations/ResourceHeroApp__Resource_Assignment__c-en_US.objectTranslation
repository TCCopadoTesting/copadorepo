<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldSets>
        <label><!-- Assignments List --></label>
        <name>Assignments_List</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Record Time Tracking Default --></label>
        <name>Record_Time_Tracking_Default</name>
    </fieldSets>
    <fields>
        <label><!-- Account --></label>
        <name>Account__c</name>
        <relationshipLabel><!-- Resource Assignments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Forecast Week Start Date --></label>
        <name>Forecast_Week_Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Internal Time Code --></label>
        <name>Internal_Time_Code__c</name>
        <relationshipLabel><!-- Resource Assignments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- KKNR + Opportunity --></label>
        <name>KKNR_Opportunity__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Account --></label>
        <name>Opportunity_Account__c</name>
    </fields>
    <fields>
        <label><!-- Original Forecast Update --></label>
        <name>Original_Forecast_Update__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>ResourceHeroApp_Status__c</name>
        <picklistValues>
            <masterLabel>Booked</masterLabel>
            <translation><!-- Booked --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Planned</masterLabel>
            <translation><!-- Planned --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Actual Amount --></label>
        <name>ResourceHeroApp__Actual_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Actual Hours --></label>
        <name>ResourceHeroApp__Actual_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Forecast Amount --></label>
        <name>ResourceHeroApp__Amount__c</name>
    </fields>
    <fields>
        <label><!-- Archive --></label>
        <name>ResourceHeroApp__Archive__c</name>
    </fields>
    <fields>
        <label><!-- Assigned To --></label>
        <name>ResourceHeroApp__Assigned_To__c</name>
    </fields>
    <fields>
        <help><!-- This field is used in conjunction with the Forecast Mode field to determine how many hours will be distributed between the Start Date and the End Date.  Making updates to this field may overwrite existing forecasts. --></help>
        <label><!-- Auto Forecast Hours --></label>
        <name>ResourceHeroApp__Auto_Forecast_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Copy From ID --></label>
        <name>ResourceHeroApp__Copy_From_ID__c</name>
    </fields>
    <fields>
        <help><!-- Making updates to this field may overwrite existing forecasts. --></help>
        <label><!-- End Date --></label>
        <name>ResourceHeroApp__End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Exclude from Utilization Coloring --></label>
        <name>ResourceHeroApp__Exclude_from_Utilization_Coloring__c</name>
    </fields>
    <fields>
        <label><!-- Forecast Count --></label>
        <name>ResourceHeroApp__Forecast_Count__c</name>
    </fields>
    <fields>
        <help><!-- This field determines how the entered Auto Forecast Hours will be distributed between the Start Date and the End Date.  Making updates to this field may overwrite existing forecasts. --></help>
        <label><!-- Forecast Mode --></label>
        <name>ResourceHeroApp__Forecast_Mode__c</name>
        <picklistValues>
            <masterLabel>Per Day</masterLabel>
            <translation><!-- Per Day --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Per Week</masterLabel>
            <translation><!-- Per Week --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Total</masterLabel>
            <translation><!-- Total --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Holiday Time --></label>
        <name>ResourceHeroApp__Holiday_Time__c</name>
        <relationshipLabel><!-- Resource Assignments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Forecast Hours --></label>
        <name>ResourceHeroApp__Hours__c</name>
    </fields>
    <fields>
        <label><!-- Last Modified Forecast Date --></label>
        <name>ResourceHeroApp__Last_Modified_Forecast_Date__c</name>
    </fields>
    <fields>
        <label><!-- Make Read Only --></label>
        <name>ResourceHeroApp__Make_Read_Only__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity --></label>
        <name>ResourceHeroApp__Opportunity__c</name>
        <relationshipLabel><!-- Resource Assignments --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- WARNING:  By checking this field, all existing forecasts for this assignment will be overwritten.  If this field is not checked, only the forecasts within the Start and End Date will be affected. --></help>
        <label><!-- Overwrite All Existing Forecasts --></label>
        <name>ResourceHeroApp__Overwrite_All_Existing_Forecasts__c</name>
    </fields>
    <fields>
        <label><!-- PTO Time --></label>
        <name>ResourceHeroApp__PTO_Time__c</name>
        <relationshipLabel><!-- Resource Assignments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Rate --></label>
        <name>ResourceHeroApp__Rate__c</name>
    </fields>
    <fields>
        <label><!-- Related Record Type --></label>
        <name>ResourceHeroApp__Related_Record_Type__c</name>
    </fields>
    <fields>
        <label><!-- Related Record --></label>
        <name>ResourceHeroApp__Related_Record__c</name>
    </fields>
    <fields>
        <label><!-- Related Record / Type --></label>
        <name>ResourceHeroApp__Related_Record_and_Type__c</name>
    </fields>
    <fields>
        <label><!-- Remaining Forecast Amount --></label>
        <name>ResourceHeroApp__Remaining_Forecast_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Remaining Forecast Hours --></label>
        <name>ResourceHeroApp__Remaining_Forecast_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>ResourceHeroApp__Resource__c</name>
        <relationshipLabel><!-- Resource Assignments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Role --></label>
        <name>ResourceHeroApp__Role__c</name>
        <picklistValues>
            <masterLabel>Advisor</masterLabel>
            <translation><!-- Advisor --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>BPA</masterLabel>
            <translation><!-- BPA --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Bid Manager</masterLabel>
            <translation><!-- Bid Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Architect</masterLabel>
            <translation><!-- Customer Architect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Opportunity Owner</masterLabel>
            <translation><!-- Opportunity Owner --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other</masterLabel>
            <translation><!-- Other --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Solution Architect</masterLabel>
            <translation><!-- Solution Architect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Specialist - Architect</masterLabel>
            <translation><!-- Specialist - Architect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Specialist - Invoicing</masterLabel>
            <translation><!-- Specialist - Invoicing --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Specialist - Network</masterLabel>
            <translation><!-- Specialist - Network --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Specialist - Telephony Platform</masterLabel>
            <translation><!-- Specialist - Telephony Platform --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Specialist - Traffic</masterLabel>
            <translation><!-- Specialist - Traffic --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Sr. Business Architect</masterLabel>
            <translation><!-- Sr. Business Architect --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Making updates to this field may overwrite existing forecasts. --></help>
        <label><!-- Start Date --></label>
        <name>ResourceHeroApp__Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- relatedId --></label>
        <name>ResourceHeroApp__relatedId__c</name>
    </fields>
    <fields>
        <label><!-- Resource Team --></label>
        <name>Resource_Team__c</name>
    </fields>
    <fields>
        <label><!-- User is Resource? --></label>
        <name>User_is_Resource__c</name>
    </fields>
    <validationRules>
        <errorMessage><!-- Rate must be greater or equal to zero --></errorMessage>
        <name>ResourceHeroApp__Rate_must_be_positive</name>
    </validationRules>
    <webLinks>
        <label><!-- Manage_Skills --></label>
        <name>RHA_Skills__Manage_Skills</name>
    </webLinks>
</CustomObjectTranslation>
