({
    submitClick : function(component, event, helper) {
        var recordId = component.get('v.recordId'); 
        var action = component.get("c.GettargetObjectId");
        action.setParams({ recordId : recordId }); 
        action.setCallback(this, function(response){      
            var state = response.getState(); 
            var resultantId = response.getReturnValue();
            var resultsToast = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                var navEvt = $A.get("e.force:editRecord");
                navEvt.setParams({
                    "recordId": resultantId
                });
                navEvt.fire();
            }else{
                resultsToast.setParams({
                    "title": "Error",
                    "message": 'An issue has occured.Please Contact System Administrator',
                    "type": "error"
                });
                resultsToast.fire();
            }
        });
        $A.enqueueAction(action); 
    }
    ,
    showSpinner : function (component, event, helper) {        
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-hide');
        $A.util.addClass(spinner, 'slds-show');  
    }
    ,
    hideSpinner  : function (component, event, helper) { 
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');    
    }
})