({
	doInit : function(component, event, helper) {
		var action = component.get("c.getlistofpipelinedopportunitycygate");
        action.setCallback(this, function(response){ 
			var result = response.getReturnValue();
            component.set('v.opportunity', result);
            var heightcount = 60 + (25*(result.length));
			component.set("v.dynamicheight",heightcount);
            
            
        });
        $A.enqueueAction(action);
	}
})