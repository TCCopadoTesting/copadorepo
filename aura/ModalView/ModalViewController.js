({
    openModel: function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getId");
        action.setParams({ Id : recordId }); 
        action.setCallback(this, function(response){      
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(result);
            if (state === 'SUCCESS'){
                component.set("v.AccId",response.getReturnValue().AccountId);
                component.set("v.isOpen", true);
            }
        });
        $A.enqueueAction(action); 
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    }
})