/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RHA_TL_RelatedRecordDelete_RA_Batch implements Database.Batchable<SObject> {
    global Set<String> rr_reference;
    global RHA_TL_RelatedRecordDelete_RA_Batch(Set<String> c_rr_reference) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
