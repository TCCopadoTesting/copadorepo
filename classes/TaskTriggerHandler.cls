/**
About
-----
Description: TaskTriggerHandler on Task

Update History
--------------

Create date: 2016-03-23
Created Mar 2016 - N.G Created
Updated Sep 2016 - A.N Functionality from TaskBeforeInsert, TaskAfterInsert and TaskBeforeUpdate has been refactored and merged into this class
Updated Sep 2016 - V.M The PR methods and functionality
Updated Sep 2016 - A.N New methods updateOpportunityOnTask and updateCampaignOnTask created
Updated Oct 2016 - P.P Case 2090 Changed Error for Tasks Related to Other Termimnated Account so that it does not show up for PR profiles
Updated June 2016 - P.P Case 3094 Added method updateContactActivities  

Issues / TODOs
--------------

Replace error messages in validation methods with custom labels.

*/

public class TaskTriggerHandler {
  
    private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    Boolean deleteInstance = False;
    Boolean insertInstance = False;
    Boolean updateInstance = False;

    public TaskTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void OnBeforeInsert(List<Task> newList, Map<ID, Task> newMap){ 
        System.debug('TaskTriggerHandler.OnBeforeInsert starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        validateContactIsActive(newList);
        validateAccountNotTerminated(newList);
        updateOpportunityOnTask(newList, null);
        updateCampaignOnTask(newList, null);
    }

    public void OnAfterInsert(List<Task> newList, Map<ID, Task> newMap){
        System.debug('TaskTriggerHandler.OnAfterInsert starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        updateAccountContactedDate(newList, null);
        updateCampaignMemberStatuses(newList);
    
        PerformanceUtils.updateYearlyTaskScore(newList);
        PerformanceUtils.checkSambesokForTask(newList);
          
        PR_createFollowUpTask(newList);
        PR_setPrNotInterested(newList);
        PR_addContactToCampaign(PR_validateHasAccountContact(newList));
        deleteInstance = False;
        insertInstance = True;
        updateContactActivities(newList,null,insertInstance,deleteInstance);
    }

    public void OnBeforeUpdate(List<Task> newList, Map<ID, Task> newMap, List<Task> oldList, Map<ID, Task> oldMap){
        System.debug('TaskTriggerHandler.OnBeforeUpdate starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        updateOpportunityOnTask(newList, oldMap);
        updateCampaignOnTask(newList, oldMap);
        updateAccountContactedDate(newList, oldMap);
    }

    public void OnAfterUpdate(List<Task> newList, Map<ID, Task> newMap, List<Task> oldList, Map<ID, Task> oldMap){
        System.debug('TaskTriggerHandler.OnAfterUpdate starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        PerformanceUtils.updateYearlyTaskScore(newList);
        PerformanceUtils.checkSambesokForTask(newList);
          
        PR_setPrNotInterested(newList);
        PR_addContactToCampaign(PR_validateHasAccountContact(newList));
        
        deleteInstance = False;
        insertInstance = False;
        updateContactActivities(newList,oldMap,InsertInstance,deleteInstance);
    }

    public void OnAfterDelete(List<Task> oldList, Map<ID, Task> oldMap){
        System.debug('TaskTriggerHandler.OnAfterDelete starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        PerformanceUtils.updateYearlyTaskScore(oldList);
        deleteInstance = True;
        insertInstance = False;
        updateContactActivities(oldList,oldMap, insertInstance,deleteInstance);
    }

    //CampaignMember Status
    public static final String PR_CAMPAIGN_STATUS = 'Ringt';
    
    //Call Activity Call Status values    
    public static final String STATUS_CALL_BACK = 'Ring tillbaka';
    public static final String STATUS_CONTACTED = 'Kontaktad';
    public static final String STATUS_WILL_CONTACT = 'Ska kontaktas';

    // TASK PICKLIST STRING DEFINITIONS
    // Status Picklist
    public static final String STATUS_CLOSED = 'Stängd';
    public static final String STATUS_OPEN = 'Öppen';

    // Type Picklist
    public static final String TYPE_PHONE = 'Telefon';
    public static final String TYPE_PHYSICAL = 'Fysiskt';
    public static final String TYPE_WEB = 'Web';
    public static final String TYPE_APSIS = 'Apsis';

    // Subject Picklist
    public static final String SUBJECT_CALL = 'Call';
    
    //SME Task Type Picklist values
    public static final string TYPE_SALES = 'Säljuppföljning';
    public static final string TYPE_NEXT = 'Nästa steg';

    // Validate that contact related to the new task is not set as inactive
    private static void validateContactIsActive(List<Task> newList){
        Set<Id> whoIds = new Set<Id>();
        
        for(Task t : newList){
            whoIds.add(t.WhoId);
        }

        if(!whoIds.isEmpty()){
            // Create a map over all contacts related to the new Tasks by querying the whoId's
            Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id, Inaktiv__c from Contact where Id in :whoIds]);
            
            // If inactive contact, the Task should not be saved. An error message will be presented to the user.
            for (Task t : newList){
                Contact con = contactMap.get(t.WhoId);
                if(con != null && con.Inaktiv__c){
                    t.addError(Label.Task_Validation_Contact_Inactive_Error_Message);
                }
            }
        }
    }

    // Validate that parent account for the account related to the new task is not terminated
    private static void validateAccountNotTerminated(List<Task> newList){
        Set<Id> whatIds = new Set<Id>();
        Id uProfileId = UserInfo.getProfileId();
        Id uUserId = UserInfo.getUserId();
        
        User u = new User(Id = uUserId);
        u.ProfileId = uProfileId;
        
        for (Task t : newList){
            whatIds.add(t.WhatId);
        }

        if(!whatIds.isEmpty()){
            // Query for Parent Account which needs to be blocked for Task Creation
            Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, Parent.Kundkonto_Nr__c from Account WHERE Id IN: whatIds]);
            String accRef = Label.Account_to_Bypass_for_Tasks;
            
            // If terminated account, the Task should not be saved. An error message will be presented to the user.
            for (Task t : newList){
                Account acc = accountMap.get(t.WhatId);
                if(acc != null && acc.Parent != null && (acc.Parent.Kundkonto_Nr__c == accRef && !SEUtility.isPRUser(u))){                
                    t.addError(Label.Task_Validation_Account_Terminated_Error_Msg);
                }
            }
        }
    }

    // Author A.N: Update the task.Opportunity lookup field if the WhatId is an opportunity
    private static void updateOpportunityOnTask(List<Task> newList, Map<ID, Task> oldMap){
        String oppPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
        
        for(Task t : newList){
            // If insert, update task.Opportunity field if WhatId is Opportunity
            if(oldMap == null && t.WhatId != null && String.valueOf(t.WhatId).substring(0,3) == oppPrefix){
                t.Opportunity__c = t.WhatId;
            } else if(oldMap != null && t.WhatId != oldMap.get(t.Id).WhatId){
                // If update, check if the WhatId has changed. If yes, check whether or not the new WhatId is an opportunity.
                if(t.WhatId == null || String.valueOf(t.WhatId).substring(0,3) != oppPrefix){
                    t.Opportunity__c = null;
                } else {
                    t.Opportunity__c = t.WhatId;
                }
            }
        }
    }

    // Author A.N: Update the task.Campaign lookup field:
    // - if the WhatId is a campaign, set this as the Campaign lookup field
    // - if the WhatId is an opportunity and opportunity.CampaignId is not null, set this as the Campaign lookup field
    private static void updateCampaignOnTask(List<Task> newList, Map<ID, Task> oldMap){
        String campPrefix = Campaign.sObjectType.getDescribe().getKeyPrefix();
        String oppPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
        List<Task> setCampaignFromOppList = new List<Task>();
        Set<Id> oppIds = new Set<Id>();
        
        for(Task t : newList){
            if(oldMap == null && t.WhatId != null){
                // If insert, update task.Campaign field if WhatId is Campaign
                if(String.valueOf(t.WhatId).substring(0,3) == campPrefix){
                    t.Campaign__c = t.WhatId;
                } else if(t.Opportunity__c != null){
                    setCampaignFromOppList.add(t);
                    oppIds.add(t.Opportunity__c);
                }
            } else if(oldMap != null && t.WhatId != null && t.WhatId != oldMap.get(t.Id).WhatId){ // If update, check if the WhatId has changed.
                // If the new WhatId is Campaign set this as task.Campaign
                if(String.valueOf(t.WhatId).substring(0,3) == campPrefix){
                    t.Campaign__c = t.WhatId;
                // If the new WhatId is Opportunity as task.Campaign based on Opportunity Campaign
                } else if(String.valueOf(t.WhatId).substring(0,3) == oppPrefix){
                    setCampaignFromOppList.add(t);
                    oppIds.add(t.Opportunity__c);
                }
            }
        }

        if(!setCampaignFromOppList.isEmpty()){
            Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, CampaignId FROM Opportunity WHERE Id in :oppIds]);
            for(Task t : setCampaignFromOppList){
                Opportunity opp = oppMap.get(t.Opportunity__c);
                if(opp != null && opp.CampaignId != null){
                    t.Campaign__c = opp.CampaignId;
                }
            }
        }
    }

    // Check if task has been closed, if so we might need to update the Contacted__c field on the related account. 
    private static void updateAccountContactedDate(List<Task> newList, Map<ID, Task> oldMap){
        Map<Id, DateTime> targetedAccounts = new Map<Id, DateTime>();

        for(Task t : newList){
            if(t.AccountId != null && t.Status != null && t.Type != null){
              if(t.Status == STATUS_CLOSED && (oldMap == null || (oldMap != null && oldMap.get(t.Id).Status != STATUS_CLOSED))){
                    if((t.Type == TYPE_PHONE || t.Type == TYPE_PHYSICAL || t.Type == TYPE_WEB) && t.SME_Task_Type__c != TYPE_SALES && t.SME_Task_Type__c!= TYPE_NEXT  && t.Call_Status__c != STATUS_CALL_BACK ){
                        if(t.ActivityDate != null){
                            targetedAccounts.put(t.AccountId, t.ActivityDate);
                        } else {
                            targetedAccounts.put(t.AccountId, System.now());
                        }
                    }
                }
            }
        }
        if(!targetedAccounts.isEmpty()){
            SEUtility.setAccountContactedDate(targetedAccounts);
        }
    }

    // If task type is 'Call' and it is connected to a contact and related to a campaign, update Campaign member statuses.
    private static void updateCampaignMemberStatuses(List<Task> newList){
        //Retrieve the object prefix for Campaign
        String campObjPrefix = Campaign.sObjectType.getDescribe().getKeyPrefix();
        Set<Id> conIds = new Set<Id>(); // Contact Ids
        Set<Id> campIds = new Set<Id>(); // Campaign Ids

        for(Task t : newList){
            // If the task is connected to a contact AND of type Call AND it relates to a Campaign-> Add the id of the task to a set
            if(t.WhoId != null && t.WhatId != null && t.Subject != null){
                if(t.Subject == SUBJECT_CALL && String.valueOf(t.WhatId).substring(0,3) == campObjPrefix){
                    // If the campaign is a Phone Campaign
                    conIds.add(t.WhoId);
                    campIds.add(t.WhatId);    
                }
            }
        }

        // If there are more than zero related contacts to the tasks of type call 
        if(conIds.size() != 0){
            // List all the CampaignMembers
            List<CampaignMember> cmList = [SELECT Id, Campaign.Type, Campaign.RecordTypeId FROM CampaignMember WHERE campaign.isActive = true AND (Campaign.Type = :TYPE_PHONE OR Campaign.Type = :TYPE_APSIS) AND ContactId IN :conIds AND CampaignId IN :campIds];
            for(CampaignMember cm : cmList){

                //Get the first 
                Campaign_Member_Statuses__c cms = SEUtility.getCampaignMemberStatus(cm.Campaign.Type, cm.Campaign.RecordTypeId);
                if(cms == null) continue; // No custom setting found, skipping to the next itereation of the for loop

                if(cms.Status_1_Responded__c) cm.Status = cms.Status_1_Name__c;
                if(cms.Status_2_Responded__c) cm.Status = cms.Status_2_Name__c;
                if(cms.Status_3_Responded__c) cm.Status = cms.Status_3_Name__c;
                if(cms.Status_4_Responded__c) cm.Status = cms.Status_4_Name__c;
                if(cms.Status_5_Responded__c) cm.Status = cms.Status_5_Name__c;
                if(cms.Status_6_Responded__c) cm.Status = cms.Status_6_Name__c;
                if(cms.Status_7_Responded__c) cm.Status = cms.Status_7_Name__c;
                if(cms.Status_8_Responded__c) cm.Status = cms.Status_8_Name__c;
            }

            if(!cmList.isEmpty()){
                update cmList;                
            }
        }
    }
    
    //Creates PR Follow up tasks
    private static void PR_createFollowUpTask(List<Task> newList){
        List<Task> followUpTasks = new List<Task>();
        
        for(Task t : newList){
            if(t.Call_Activity__c != null && t.Type == TYPE_PHONE && t.Call_Status__c == STATUS_CALL_BACK && t.Status == STATUS_CLOSED) {
                Task ft = t.clone();
                ft.Call_Status__c = STATUS_CONTACTED;
                ft.Status = STATUS_OPEN;
                
                if(t.Call_Back_Date__c != null){
                    ft.ActivityDate = t.Call_Back_Date__c.date(); 
                }else{
                    ft.ActivityDate = Date.Today();
                }
                
                followUpTasks.add(ft);
            }
        }
        if(!followUpTasks.isEmpty()){
            insert followUpTasks;
        }
    }
    
    //Adds selected Contacts to Campaign
    private static void PR_addContactToCampaign(Map<Id, Task> newMap){
        
        //TaskId, CallActivityId
        Map<Id, Id> taskCallActivityMap = new Map<Id, Id>();
        Map<Id, Task> prFilteredMap = new Map<Id, Task>();    
        
        //If the task matches the criteria of being from PR and PR Call Status set to true
        for(Task t : newMap.values()){
            if(t.Call_Activity__c != null && t.Call_Status__c == STATUS_CONTACTED && t.Status == STATUS_CLOSED && t.PR_Not_Interested__c != True){
                prFilteredMap.put(t.Id, t);
                taskCallActivityMap.put(t.id, t.Call_Activity__c); 
            }           
        }
        
        //Map Call Activity to the Campaign
        //CallActivityId, CampaignId
        Map<Id, Id> callActiCampMap = new Map<Id, Id>();
        if(!taskCallActivityMap.isEmpty()){
            for(Call_Activity__c ca :[SELECT Campaign__c, Id FROM Call_Activity__c WHERE ID IN :taskCallActivityMap.values()]){
                callActiCampMap.put(ca.Id, ca.Campaign__c);
            }
        }        
        if(!prFilteredMap.isEmpty() && !callActiCampMap.isEmpty()){
            
            //ContactId, Contact
            Map<Id, Contact> addedContacts = new Map<Id, Contact>();
            
            //RelationId , TaskId
            Map<Id, Id> taskRelations = new Map<Id, Id>();
            for(TaskRelation tr : [SELECT RelationId, TaskId FROM TaskRelation WHERE TaskId IN :prFilteredMap.keySet() AND IsWhat = False]){
                taskRelations.put(tr.RelationId, tr.TaskId);
            }
            
            if(!taskRelations.isEmpty()){
                //Find all added contacts
                for(Contact c :[Select Id FROM Contact WHERE Id IN :taskRelations.keySet()]){
                    addedContacts.put(c.Id, c);
                }
                //Remove them from the addedContact list if they are already a campaign member
                for(CampaignMember cm : [SELECT ContactId FROM CampaignMember WHERE ContactId IN :addedContacts.keyset() AND CampaignId IN :callActiCampMap.values()]){
                    if(addedContacts.containsKey(cm.ContactId)){
                        addedContacts.remove(cm.ContactId);
                    }
                }
            }
            if(!addedContacts.isEmpty()){
                List<CampaignMember> campaignMemberList = new List<CampaignMember>();
                
                //For each contact we want to add to the campaign
                for(Id cId  :addedContacts.keySet()){
                    CampaignMember cm = new CampaignMember();
                    cm.ContactId = cId;
                    
                    //To make sure that this code works even if we would update contacts related to two different campaigns we need to map what contact belongs to what campaign
                    cm.CampaignId =  callActiCampMap.get(taskCallActivityMap.get(taskRelations.get(cId)));
                    cm.Status = PR_CAMPAIGN_STATUS;
                    
                    campaignMemberList.add(cm);
                }
                
                //Add the rest as CampaignMember
                insert campaignMemberList;
            }        
        }
    }
    
    //Validates the Contacts to fulfill the critera for PR
    private static Map<Id, Task> PR_validateHasAccountContact(List<Task> newList){
        //Map containing validated Tasks
        Map<Id, Task> validatedTasks = new Map<Id, Task>();
        
        List<Task> prTasks = new List<Task>();
        
        //Check if it's PR task and if WhoId is not Empty
        for(Task t : newList){
            if(t.Call_Activity__c != null && t.Call_Status__c == STATUS_CONTACTED && t.Status == STATUS_CLOSED && t.PR_Not_Interested__c != True){
                //If no contacted added, validation error
                if(t.WhoId == null){
                    t.addError(Label.caValidationLogContact);
                }else{
                    //else add to PRtasks
                    prTasks.add(t);
                }   
            }
        }
        
        if(!prTasks.isEmpty()){
            //Get CallActivities and map them to Tasks so we can see which Call Activity belongs to the Task
            //CallActivityId, TaskId
            Map<Id, Id> callActivityTaskMap = new Map<Id, Id> ();
            for(Task t : prTasks){
                callActivityTaskMap.put(t.Call_Activity__c, t.Id);
            }
            
            //Get the Account from the Call Activity to check if it matches the selected Contacts Account
            //AccountId, CallActivityId
            Map<Id, Id> callActivityAccountMap = new Map<Id, Id>();
            for(Call_Activity__c ca :[SELECT Id, Account__c FROM Call_Activity__c WHERE ID IN :callActivityTaskMap.keySet()]){
                callActivityAccountMap.put(ca.Id, ca.Account__c);
            }
            
            //Here we check if the Call Activity Account is the same as task AccountId, which is set by selected Contact (Internal Salesforce Logic)
            for(Task t : prTasks){
                if(callActivityAccountMap.get(t.Call_Activity__c) != t.AccountId){
                    t.addError(Label.caValidationAccountMismatch);
                }else{
                    validatedTasks.put(t.Id, t);
                }
            }
        }
        
        //We return the validated Tasks
        return validatedTasks;
    }
    
    //Updates the Call Activity if PR not interested is set
    private static void PR_setPrNotInterested(List<Task> newList){
        Set<Id> caSet = new Set<Id>();
        
        for(Task t : newList){
            if(t.Call_Activity__c != null && t.Status == STATUS_CLOSED && t.PR_Not_Interested__c == True){
                caSet.add(t.Call_Activity__c);
            }
        }
        if(!caSet.isEmpty()){
            List<Call_Activity__c> caList = new List<Call_Activity__c>();
            
            for(Call_Activity__c ca : [SELECT Id, Is_PR_Not_Interested__c FROM Call_Activity__c WHERE Id IN :caSet AND Is_PR_Not_Interested__c = FALSE]){
                ca.Is_PR_Not_Interested__c = true;
                caList.add(ca);
            }
            
            if(!caList.isEmpty()){
               update caList;
            }
        }
    }
    
        
    //method to mark Activities related to user group on Contacts
    public static void updateContactActivities(List<Task> taskList, Map<Id,Task> oldMap,Boolean isInsert,Boolean isDelete){
        
        Schema.DescribeSObjectResult contactDescribe = Contact.sObjectType.getDescribe();
        string contactPrefix = contactDescribe.getKeyPrefix();
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        List<Id> contactIdList = new List<Id>();
        Map<Id,Set<String>> contactSegmentMap = new Map<Id,Set<String>>();
        Map<Id,Set<String>> contactTaskSegmentMap = new Map<Id,Set<String>>();
        Set<Id> taskSet = new Set<Id>();
        List<TaskWhoRelation> TWRList = new List<TaskWhoRelation>();
        Boolean booleanupdate = False;
        List<Contact> contactUpdateList = new List<Contact>();
        Map<Id,Task> taskRelationMap = new Map<Id,Task>();
        Map<Id,String> roleSegmentMap = new Map<Id,String>();
        
        for(Contact_Management_Role_Set__c crms : Contact_Management_Role_Set__c.getAll().Values()){
            roleSegmentMap.put(crms.Role_Id__c,crms.Segment__c);
        }
        //can use this instead of following
        //if(recEvent.WhoId.getSObjectType() == Contact.sObjectType)
        for(task t:taskList){
            //works for insert statements
            if(isInsert == True){
                if(t.WhoId != Null){
                    if(string.valueof(t.WhoId).substring(0,3) == contactPrefix){
                        contactIdList.add(t.WhoId);
                        taskSet.add(t.Id);
                    }
                }
            }
            else{
                system.debug('not Insert');
                if(t.WhoId != Null){
                    if(string.valueof(t.WhoId).substring(0,3) == contactPrefix || string.valueof(oldMap.get(t.Id).WhoId).substring(0,3)==contactPrefix){
                    system.debug('contact prefix');
                        //works for update statement
                        if(isDelete == False){
                            //works when WHoId is changed
                            if(oldMap.get(t.Id).WhoId != Null){
                                if(t.WhoId != oldMap.get(t.id).WhoId)
                                {
                                    contactIdList.add(t.Whoid);
                                    contactIdList.add(oldMap.get(t.Id).WhoId);
                                    taskSet.add(t.Id);
                                }
                            } 
                            //works when OwnerId is changed
                            if(t.OwnerId!= oldmap.get(t.id).OwnerId){
                                contactIdList.add(t.WhoId);
                                taskSet.add(t.Id);
                            }
                        } 
                
                        else{
                            //works for delete statement
                            if(oldMap.get(t.Id).WhoId != Null){
                                contactIdList.add(oldMap.get(t.Id).WhoId);
                                taskSet.add(t.Id);
                            }
                        }
                    }
                }
                else{
                    //works for update statements where Who Id is made empty
                    if(oldMap.get(t.Id).WhoId != Null){
                        if(string.valueof(oldMap.get(t.Id).WhoId).substring(0,3) == contactPrefix){
                            contactIdList.add(oldMap.get(t.Id).WhoId);
                            taskSet.add(t.Id);
                        }
                    }
                }
            }                  
        }
        if(contactIdlist.size()>0){
            //system.debug('contactIdList' + contactIdList);
            contactMap = new Map <Id,Contact>([SELECT Id,Telia_Activities__c,Cygate_Activities__c,Marketing_Activities__c,Service_Management_Activities__c,Other_Activities__c,(SELECT Id,Ownerid,WhoId,Owner.UserRoleId,RecordTypeId FROM Tasks),(SELECT Id,OwnerId,WhoId,Owner.UserRoleId,RecordTypeId FROM Events) FROM Contact WHERE ID IN: contactIdList AND RecordType.DeveloperName =: SEUtility.CONTACT_RECTYPE_CUSTOMER]);
            if(contactMap.values() != NULL){    
                for(contact con:contactMap.values()){
                    if(con.Tasks.size() >0){
                       for(task t:con.Tasks){
                           taskSet.add(t.Id);                        
                       }
                   }
                   if(con.Events.size() > 0){
                       for(Event e :con.Events){
                           if(contactSegmentMap.containsKey(e.WhoId)){
                               Set<String> setVar = new Set<String>();
                               setVar =  contactSegmentMap.get(e.Whoid);
                               setVar.add(roleSegmentMap.get(e.Owner.UserRoleId));
                               contactSegmentMap.put(e.Whoid,setVar);
                           }
                           else{
                               Set<String> setVar = new Set<String>();
                               setVar.add(roleSegmentMap.get(e.Owner.UserRoleId));
                               contactSegmentMap.put(e.Whoid,setVar);
                           }
                        }
                    }
                } 
            }       
        }
        
        if(taskSet.size()>0){
            taskRelationMap = new Map<Id,Task>([Select Id, WhoId,Owner.UserRoleId,Ownerid,(Select Id,TaskId,RelationId,Type,Task.OwnerId,Task.Owner.UserRoleId FROM TaskWhoRelations) FROM TASK WHERE Id IN: taskSet]);
        }
        if(taskRelationMap.values() != Null){
            for(Task t:taskRelationMap.values())
            {
                if(t.taskWhoRelations != Null){    
                    for(TaskWhoRelation twr:t.taskWhoRelations){
                        TWRList.add(twr);
                    }
                }
            }
            if(TWRList.size()>0){
                for(TaskWhoRelation TWR: TWRList){
                    if(TWR.Type == 'Contact'){
                    //if(oldMap != NUll && !(oldMap.containsKey(t.Id)))
                        if(contactTaskSegmentMap.containsKey(TWR.RelationId)){
                            Set<String> setVar = new Set<String>();
                            setVar =  contactTaskSegmentMap.get(TWR.RelationId);
                            string inst = string.valueof(roleSegmentMap.get(TWR.task.Owner.UserRoleId));
                            setVar.add(inst); 
                            //setVar.add(roleSegmentMap.get(TWR.task.Owner.UserRoleId));
                            contactTaskSegmentMap.put(TWR.RelationId,setVar);
                        }
                        else{
                            Set<String> setVar = new Set<String>();
                            string inst = string.valueof(roleSegmentMap.get(TWR.task.Owner.UserRoleId));
                            setVar.add(inst);
                            //setVar.add(roleSegmentMap.get(TWR.Task.Owner.UserRoleId));
                            contactTaskSegmentMap.put(TWR.RelationId,setVar);
                        }
                    }           
                }
            }
        }
        if(contactMap.Values() != NULL){
            for(Contact con:contactMap.values()){
            //contact cont = new Contact(id = con.Id);
                If(con.Telia_Activities__c != False){
                    if(contactTaskSegmentMap.get(con.Id) == NUll || !(contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_TELIA))){
                        if(contactSegmentMap.get(con.Id) == NUll || !(contactSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_TELIA))){
                            con.Telia_Activities__c = False;
                            booleanUpdate = True;
                        }
                    }
                }
                else{
                    if((contacttaskSegmentMap.get(con.Id) != Null) && contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_TELIA)){
                        con.Telia_Activities__c = True;
                        booleanUpdate = True;
                    }
                }
                if(con.Cygate_Activities__c != False){
                    if(contactTaskSegmentMap.get(con.Id) == Null || !(contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_CYGATE))){
                        if(contactSegmentMap.get(con.Id) == Null || !(contactSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_CYGATE))){
                            con.Cygate_Activities__c = False;
                            booleanUpdate = True;
                        }
                    }
                }
                else{
                    if((contacttaskSegmentMap.get(con.Id) != Null) && contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_CYGATE)){
                        con.Cygate_Activities__c = True;
                        booleanUpdate = True;
                    }
                }
                if(con.Service_Management_Activities__c != False){
                    if(contactTaskSegmentMap.get(con.Id) == Null || !(contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_SERVICE_MANAGEMENT))){
                        if(contactSegmentMap.get(con.Id) == Null || !(contactSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_SERVICE_MANAGEMENT))){
                            con.Service_Management_Activities__c = False;
                            booleanUpdate = True;
                        }
                    }
                }
                else{
                    if((contacttaskSegmentMap.get(con.Id) != Null) && contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_SERVICE_MANAGEMENT)){
                        con.Service_Management_Activities__c = True;
                        booleanUpdate = True;
                    }
                }
                if(con.Marketing_Activities__c != False){
                    if(contactTaskSegmentMap.get(con.Id) == Null || !(contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_MARKETING))){
                        if(contactSegmentMap.get(con.Id) == Null || !(contactSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_MARKETING))){
                            con.Marketing_Activities__c = False;
                            booleanUpdate = True;
                        }
                    }
                }
                else{
                    if((contacttaskSegmentMap.get(con.Id) != Null) && contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_MARKETING)){
                        con.Marketing_Activities__c = True;
                        booleanUpdate = True;
                    }
                }
                if(con.Other_Activities__c != False){
                    if(contactTaskSegmentMap.get(con.Id) == Null || !(contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_OTHER))){
                        if(contactSegmentMap.get(con.Id) == Null || !(contactSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_OTHER))){
                            con.Other_Activities__c = False;
                            booleanUpdate = True;
                        }
                    }
                }
                else{
                    if((contacttaskSegmentMap.get(con.Id) != Null) && contactTaskSegmentMap.get(con.Id).contains(SEUtility.CONTACT_SEGMENT_OTHER)){
                        con.Other_Activities__c = True;
                        booleanUpdate = True;
                    }
                }
    
                if(booleanUpdate == TRUE){
                    contactUpdateList.add(con);
                }
            }  
        }  
        if(contactUpdateList.size() > 0){
        
            try{
                update ContactUpdateList;
            }
            catch(exception e){
                for(Task t:TaskList){
                    t.adderror(e.getmessage());
                }
            }            
        }
    }

}