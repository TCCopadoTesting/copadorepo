@isTest
public with sharing class Test_UserTriggerHandler {
    
    @testSetup
    static void initTestData() {
        Test_DataFactory.setUpOrg();
    }

    static testMethod void createNewSMEUserNoCygateContactCreated() {
        UserTriggerHandler.testCreateContacts = true;
        User smeUser;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            Test.startTest();
            smeUser = Test_DataFactory.createSMEUser();
            insert smeUser;
            Test.stopTest();
        }

        List<Contact> conList = [SELECT Id, Cygate_User__c FROM Contact WHERE Is_Cygate_User_Contact__c = true];
        List<Account> accList = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_N LIMIT 1];

        System.assertEquals(conList.size(), 0, 'No Cygate Contact should have been created for a SME User');
    }
    
    static testMethod void createNewCygateUserCygateContactCreated() {
        UserTriggerHandler.testCreateContacts = true;
        User cygateUser;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            Test.startTest();
            cygateUser = Test_DataFactory.createCygateUser();
            insert cygateUser;
            Test.stopTest();
        }

        List<Contact> conList = [SELECT Id, Cygate_User__c, Email, Telia_Id__c FROM Contact WHERE Is_Cygate_User_Contact__c = true];
        List<Account> accList = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_N LIMIT 1];
        
        //List<User_Performance__c > usrPerList = [SELECT Id, Name FROM User_Performance__c WHERE User.UserRoleId LIKE 'CG%'  AND User__r.Cygate_Performance_User__c = true];
        
        
        System.assertEquals(conList.size(), 1, 'A Cygate Contact should have been created for a Cygate User');
        System.assertEquals(conList[0].Cygate_User__c, cygateUser.Id, 'A Cygate Contact should have been created for the specific Cygate User');
        System.assertEquals(conList[0].Email, cygateUser.Email, 'The email address for the user should have been copied to the contact');
        System.assertEquals(conList[0].Telia_Id__c, cygateUser.Telia_Id__c, 'The telia id for the user should have been copied to the contact');
    }

    static testMethod void createNewTeliaUserTeliaContactCreated() {
        UserTriggerHandler.testCreateContacts = true;
        User teliaUser;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            Test.startTest();
            teliaUser = Test_DataFactory.createSMEUser();
            insert teliaUser;
            Test.stopTest();
        }

        List<Contact> conList = [SELECT Id, Telia_User__c, Email, Telia_Id__c, AccountId FROM Contact WHERE Is_Telia_User_Contact__c = true];
        Account acc = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_Nr_Telia LIMIT 1];

        System.assertEquals(conList.size(), 1, 'A Telia Contact should have been created for a Telia User');
        System.assertEquals(conList[0].Telia_User__c, teliaUser.Id, 'A Telia Contact should have been created for the specific Telia User');
        System.assertEquals(conList[0].Telia_Id__c, teliaUser.Telia_Id__c, 'The telia id for the user should have been copied to the contact');
        System.assertEquals(conList[0].AccountId, acc.Id, 'The Contacts account should be the Telia Account');
    }

    /* Because of recursion handling, it is not possible to get contacts created when the user is inserted and updated in the same setting.
    // This Test case is no longer valid and will therefore be deleted.
    static testMethod void changeLargeUserToCygateUserCygateContactCreated() {
        User largeUser;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            largeUser = Test_DataFactory.createLargeUser();
            insert largeUser;
        }

        List<Account> accList = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_N LIMIT 1];
        // Note: Account should always exist in production
        if(accList.size() == 0){
            Account acc = new Account();
            acc.Name = 'Cygate Internal AB';
            acc.Org_Nr__c = label.Account_Org_N;
            acc.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_LARGE);
            insert acc;
        }

        List<Contact> conListBefore = [SELECT Id, Cygate_User__c FROM Contact WHERE Is_Cygate_User_Contact__c = true];
        System.assertEquals(conListBefore.size(), 0, 'No Cygate Contact should have been created for a Large User');

        Test.startTest();
        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            User cygateUser = Test_DataFactory.createCygateUser();
            largeUser.UserRoleId = cygateUser.UserRoleId;
            update largeUser;
        }
        Test.stopTest();

        List<Contact> conList = [SELECT Id, Cygate_User__c FROM Contact WHERE Is_Cygate_User_Contact__c = true];
        List<Account> accListAfter = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_N LIMIT 1];

        System.assertEquals(conList.size(), 1, 'A Cygate Contact should have been created for a Cygate User');
        System.assertEquals(conList[0].Cygate_User__c, largeUser.Id, 'A Cygate Contact should have been created for the specific Cygate User');
        System.assertEquals(accList.size(), 1, 'An Account should have been created when inserting a Cygate User');
    }*/
    
    static testMethod void Test_cygatePerformanceUsers()
    {
         UserTriggerHandler.testCreateContacts = true;
        User teliaUser;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            Test.startTest();
            User cygateUser = Test_DataFactory.createCygateUser();
            insert cygateUser;
            List<User> u = new List<User>();
            u.add(cygateUser);
            List<Yearly_Performance_Score__c> yps = [SELECT Id, User_Performance__c FROM Yearly_Performance_Score__c WHERE User_Performance__r.user__c in :u AND User_Performance__r.User__r.Cygate_Performance_User__c = true];
            List<User_Performance__c > up = [SELECT Id, Name FROM User_Performance__c WHERE User__c in :u AND User__r.Cygate_Performance_User__c = true];
            
            String jsonStr ='[{"Name":"Test"}]';
            String transformedText = jsonStr.replaceAll('""','"\"');
            UserTriggerHandler.loadExistingCygateYearlyPerformanceScore(u);
            UserTriggerHandler.createUserPerformance(u);
            UserTriggerHandler.insertNewUserPerformance(transformedText);
            Test.stopTest();
        }
    }
    
}