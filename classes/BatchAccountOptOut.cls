/**
About
-----
Description: BatchAccountOptOut for  CLM Marketing Opt Out
Create date: 03.3-2017

Update History
--------------
Created Mars 2017 - Vahraz Mostajir - SAEN-1838. 

Issues / TODOs
--------------     
*/

public class BatchAccountOptOut implements Database.Batchable<Account>, Database.Stateful  {
    
    public Map<Id, Account> parentAccounts;
    
    public BatchAccountOptOut(){
        System.debug('AccountOptOut INIT');      
        parentAccounts = new Map<Id, Account>([SELECT Id, Marketing_Mails_Opt_Out__c, From_Date__c, To_Date__c
                                               FROM Account
                                               WHERE Process_Opt_Out__c = True 
                                               AND (RecordTypeId = :SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_KK) 
                                                    OR RecordTypeId = :SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_KK_SEC1)
                                                    OR RecordTypeId = :SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_KK_SEC2))]);
        
        
    }
    
    public List<Account> start(Database.BatchableContext BC) { 
        System.debug('AccountOptOut START');
        
        return new List<Account>([SELECT Id, Process_Opt_Out__c, Opt_Out_Manually_Set__c, Parent.Id, 
                                  From_Date__c, Man_From_Date__c,
                                  To_Date__c, Man_To_Date__c,
                                  Marketing_Mails_Opt_Out__c, Man_Mails_Opt_Out__c
                                  FROM Account
                                  WHERE ParentId IN :parentAccounts.keySet() 
                                  AND RecordTypeId != :SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_CYGATE_SUB)]);
    }
    
    public void execute(Database.BatchableContext BC, List<Account> scope){  
        System.debug('AccountOptOut EXECUTE');       

        for(Account acc : scope){
            if(String.isEmpty(parentAccounts.get(acc.ParentId).Marketing_Mails_Opt_Out__c) && acc.Opt_Out_Manually_Set__c){
                acc.From_Date__c = acc.Man_From_Date__c;
                acc.To_Date__c = acc.Man_To_Date__c;
                acc.Marketing_Mails_Opt_Out__c = acc.Man_Mails_Opt_Out__c;
            }else{
                acc.Process_Opt_Out__c = true;
                acc.From_Date__c = parentAccounts.get(acc.ParentId).From_Date__c;
                acc.To_Date__c = parentAccounts.get(acc.ParentId).To_Date__c;
                acc.Marketing_Mails_Opt_Out__c = parentAccounts.get(acc.ParentId).Marketing_Mails_Opt_Out__c;
            }
        }
        
        update scope;
    }
    
    public void finish(Database.BatchableContext BC){
        System.debug('AccountOptOut FINNISH');
        Database.executeBatch(new BatchContactOptOut(), 20);
    }   
}