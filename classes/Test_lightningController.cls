@isTest
public class Test_lightningController {
    public static testmethod void no01_testNavigateArticleController(){
        System.debug('@@Test no01_NavigateArticleController Starts');
        test.starttest();
        ProcessInstanceWorkitem workItem  = new ProcessInstanceWorkitem();
        ProcessInstance processrec = new ProcessInstance();
        Knowledge__kav kavobj = new Knowledge__kav();
        kavobj.Submitted_for_Approval__c = true;
        kavobj.Title = 'test Article1';
        kavobj.UrlName = 'testurl1';
        kavobj.Reference_Solution_Approver__c = '00524000006c1V6';
        insert kavobj;
        system.debug('knowledgeId'+kavobj.id);
        processrec =[SELECT Id,ProcessDefinitionId,Status,TargetObject.type FROM ProcessInstance WHERE TargetObjectId =:kavobj.id];
        workItem =[SELECT Id,Actor.name,OriginalActor.name,ProcessInstanceId FROM ProcessInstanceWorkitem where ProcessInstanceId =:processrec.Id];
        system.debug('AppId'+workItem);
        if(workItem !=null)
            NavigateArticleController.GettargetObjectId(workItem.Id);
        test.stopTest();
    }
    public static testmethod void no01_ProductDetailController(){
        System.debug('@@Test no01_ProductDetailController Starts');
        test.starttest();
        System.runAs(new User(Id = Userinfo.getUserId())){
            Case caseobj = new Case();            
            caseobj.Subject = 'case test subject';
            insert caseobj;
            List<OpportunityLineItem> olist = ProductDetailController.getRecord(caseobj.Id);
            Id oppty = ProductDetailController.getOpportunity (caseobj.Id);
        }
        test.stopTest(); 
    }
}