@isTest
private class Test_LatestClosedOpportunities  {    
    
    public static testmethod void testLatestClosedOpportunities(){ 
        User salesManager;         
        Id pricebookId;

       
        System.runAs(new User(Id = Userinfo.getUserId())){
        salesManager = Test_DataFactory.createSalesManagerUsers(1)[0];
        salesManager.Bypass_VR__c = true;
        insert salesManager;
        }
        System.runAs(salesManager){
        Test.startTest();
        Account acc = Test_DataFactory.createOneAccount();
        acc.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_LARGE);
        insert acc;
        Product2 prod = Test_DataFactory.createProducts(1)[0];
        insert prod;
        pricebookId = Test.getStandardPricebookId();
        Opportunity opp = Test_DataFactory.createOwnedOpportunities(1, salesManager)[0];
        opp.StageName = OpportunityValidationHandler.OPP_STAGE_QUALIFY;
        opp.Agreement_Signed__c = true;
        opp.Large_Continuation_Sales__c = 2;
        opp.CloseDate = System.today();
        insert opp;
        Contact c = Test_DataFactory.createOneContact();
        c.AccountId = acc.Id;
        insert c;
        OpportunityContactRole ocr = Test_DataFactory.createOpportunityContactRole();
        ocr.ContactId = c.Id;
        ocr.Role = OpportunityValidationHandler.OPP_CR_AVTALSTECKNARE;
        ocr.OpportunityId = opp.Id;
        insert ocr;
        
        PricebookEntry pbe = Test_DataFactory.createPricebookEntry(pricebookId, prod);
        insert pbe;
        
        OpportunityLineItem oppLi = Test_DataFactory.createOpportunityLineItem(opp.Id, pbe.Id);
        insert oppLi;
        
        opp.Main_Product_Area__c = 'Other';
        opp.StageName = OpportunityValidationHandler.OPP_STAGE_WON;          
        update opp;
        Test.stopTest();
        LatestClosedOpportunities.getlistoflatestclosedopportunity();
        }

    }
    
     public static testmethod void test_listoftopclosedopportunitycygate(){
        System.runAs(Test_DataFactory.getByPassUser()){
                
                Test.startTest();
                Account acc = Test_DataFactory.createOneAccount ();
                insert acc;
                
                Opportunity opp = Test_DataFactory.createCygateOpportunity();
                opp.accountId = acc.Id;
                opp.StageName = OpportunityValidationHandler.OPP_STAGE_WON;
                insert opp;
                
                Opportunity opp2 = Test_DataFactory.createCygateOpportunity();
                opp2.accountId = acc.Id;
                insert opp2;
                
                Test.stopTest();
                LatestClosedOpportunities.getlistoftopclosedopportunitycygate();
                LatestClosedOpportunities.getlistoflatestclosedopportunitycygate();
                LatestClosedOpportunities.getlistofpipelinedopportunitycygate();
                
        }
     }
}