/**
About
-----
Description: LeadConvertController

Update History
--------------
Created 2017-08-22 12:38 Vahraz Mostajir - Used for LeadConvertComponent

Issues / TODOs
--------------     
*/


public class LeadConvertController {
    public class ActionStatus{
        public String ErrorMessage {get; set;}
        public Database.LeadConvertResult ReturnData {get; set;}
        public Boolean IsSuccess {get; set;}
    }
    
    @AuraEnabled
    public static String convertLead(Id leadId, String accId, String oppName, String buyInterestId, String recordType, String contactId, Boolean overwrite){
        
        ActionStatus actionStatus = new ActionStatus();        
        Lead lead = [SELECT Id, FirstName, LastName, Email, Title, Phone, MobilePhone FROM Lead WHERE Id = :leadId];
        lead.Lead_Convert__c = True;
        lead.Lead_Conversion_Time__c  = DateTime.now();
        
        if(!String.isEmpty(buyInterestId)){
            lead.CA_Buy_Interest__c = buyInterestId;
        }
        try{ 
            update lead;
        }catch(Dmlexception dex){
            actionStatus.IsSuccess = false;
            actionStatus.ErrorMessage = dex.getMessage();
        }
        
        Database.LeadConvert lc = new Database.LeadConvert();
        
        lc.setConvertedStatus('Konverterat');
        lc.setLeadId(leadId);
        
        
        //If duplicate contact has been selected
        if(!String.isEmpty(contactId)){
            Contact con = [SELECT Id, AccountId, FirstName, LastName, Email, Title, Phone, MobilePhone FROM Contact WHERE Id = :contactId LIMIT 1];
            List<AccountContactRelation> acrList = [SELECT AccountId, ContactId FROM AccountContactRelation WHERE ContactId = :contactId AND AccountId =:accId LIMIT 1];
            if(acrList.size() == 0){
                AccountContactRelation acr = new AccountContactRelation();
                acr.AccountId = accId;
                acr.ContactId = con.Id; 
                acrList.add(acr);
                try{ 
                    insert acrList[0];
                    System.Debug('inserted acr succesfully');
                }catch(Dmlexception dex){
                    actionStatus.IsSuccess = false;
                    actionStatus.ErrorMessage = dex.getMessage();
                    return JSON.serialize(actionStatus);
                }
            }
            
            //If overwrite option was checked
            if(overwrite){
                con.FirstName = lead.FirstName;
                con.LastName = lead.LastName;
                con.Title = lead.Title;
                con.Phone = lead.Phone;
                con.MobilePhone = lead.MobilePhone; 
            }
            
            try{ 
                //Update the contact
                update con;
                System.Debug('updated con succesfully');
            }catch(Dmlexception dex){
                actionStatus.IsSuccess = false;
                actionStatus.ErrorMessage = dex.getMessage();
                return JSON.serialize(actionStatus);
            }
            
            lc.setContactId(con.Id);
            lc.setAccountId(con.AccountId);
        }else{
            //If there was no duplicate contact, we just set the accId and a new contact will be created
            lc.setAccountId(accId);
        } 
        
        if(!String.isEmpty(oppName)){
            lc.setOpportunityName(oppName);
        }else{
            lc.setDoNotCreateOpportunity(true);
        }
        lc.setOwnerId(UserInfo.getUserId()); 
        
        try{ 
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            System.Debug('updated con succesfully');
            actionStatus.IsSuccess = lcr.isSuccess();
            actionStatus.ReturnData = lcr;
        }catch(Dmlexception dex){
            actionStatus.IsSuccess = false;
            actionStatus.ErrorMessage = dex.getMessage();
            
            return JSON.serialize(actionStatus);
        }
        
        return JSON.serialize(actionStatus);
    } 
    
    @AuraEnabled
    public static Lead getLead(String leadId){
        Lead lead = [SELECT Id, Name, Account__c, Account__r.Name, Email FROM Lead WHERE Id = :leadId LIMIT 1];
        
        return lead;
    }
    
    @AuraEnabled
    public static List<Contact> getDuplicateContacts(String email){
        List<Contact> contacts = [SELECT Id, Name, Email, Account.Name FROM Contact WHERE Email = :email];
        
        return contacts;
    }
    
    @AuraEnabled
    public static List<Buy_Interest__c> getBuyInterests(String leadId){
        List<Buy_Interest__c> biList = [SELECT Id, Name, Lead_Source__c, CreatedDate, Buy_Interest_Submitter_Name__c  FROM Buy_Interest__c WHERE Lead__c = :leadId AND Lead_Source__c = :LeadTriggerHandler.LEAD_SOURCE_RESELLER ORDER BY CreatedDate ];
        
        return biList;
    }
    
    @AuraEnabled
    public static List<String> getOpportunityRecordTypes(){
        List<String> availableRecordTypes = 
            SEUtility.GetAvailableRecordTypeNamesForSObject(Opportunity.SObjectType);
        return availableRecordTypes;
    }
}