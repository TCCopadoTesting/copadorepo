/**
    About
    -----
    Description: TriggerHandler for the User object
    Create date: April 2016
            
    Update History
    --------------
    Created Apr 2016 - A.N
    Updated Sep 2016 - A.N Removed hardcoded references to Org Nr Ids. Refactored method for filtering Telia users, as it recursively did SOQL-queries.
                        Refactored createCygateContacts and createTeliaContacts to avoid doing unneccesary SOQL-queries
    Updated Sep 2016 - A.N Updated methods createCygateContacts/createTeliaContacts so that the Telia Id is populated on the contacts. SAEN-1932
    Updated Dec 2016 - A.N SAEN-2117 updated method filterTeliaUsers. Instead of hard coded values, define the role names in a custom setting. (Also SOHO:Chef was added to the list)
    Updated Jun 2018 - Y.K SALEF-1126 Add/Revmove users from Leaderboard (Resultat & Poäng)
    Updated Oct 2018 - D.C. SALEF- 1496 For All cygate Users Chatter notification option set to "every post"

*/

public class UserTriggerHandler {

    // To reduce SOQL limit issues when running tests, in most cases we do not need to create Telia/Cygate contacts.
    // Unless testCreateContacts is set to true in the test class, the createTeliaContacts/createCygateContacts wil not run.
    @TestVisible private static Boolean testCreateContacts = false;
    
    public static boolean avoidRecursion = false;
    public static boolean avoidRecursionCPerfor = false;
    public static boolean avoidRecursionchatter = false;
    private boolean m_isExecuting;
    private integer BatchSize = 0;

    public UserTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    //D.C.
    public void OnBeforeInsert(List<User> newList, Map<ID, User> newMap){
        System.debug('UserTriggerHandler.OnBeforeInsert starts. Number of SOQLs used so far ' + Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        setChatterFrequency(newList);
    }
    //D.C.  

    public void OnAfterInsert(List<User> newList, Map<ID, User> newMap){
        System.debug('UserTriggerHandler.OnAfterInsert starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        createUserPerformance(newList);
        createCygateContacts(newList);
        createTeliaContacts(newList);
        AddToGroup(newList);
    }

    public void OnAfterUpdate(List<User> newList, Map<ID, User> newMap, List<User> oldList, Map<ID, User> oldMap){
        System.debug('UserTriggerHandler.OnAfterUpdate starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + BatchSize);
        createCygateContacts(newList);
        createTeliaContacts(newList);
    }

    // Filter out the Cygate users, and call future method to create contact records
    private static void createCygateContacts(List<User> newList){
        // To avoid SOQL limit issues, do not create contacts when running in test mode unless specified.
        if(Test.isRunningTest() && testCreateContacts == false) return;
        
        List<User> cygateUsers = filterCygateUsers(newList);

        if(cygateUsers.size() > 0 && avoidRecursion == false){
            avoidRecursion = true;
            
            if(!System.isFuture()) { 
                List<Contact> newCygateContacts = new List<Contact>();
                Set<Id> userIdsWithCygateContacts = loadExistingCygateContacts(cygateUsers);

                for(User u : cygateUsers){
                    if(!userIdsWithCygateContacts.contains(u.Id)){
                        Contact c = new Contact();
                        c.Is_Cygate_User_Contact__c = true;
                        c.Cygate_User__c = u.Id;
                        c.AccountId = getCygateAccountId();
                        c.FirstName = u.FirstName;
                        c.LastName = u.LastName;
                        c.Email= u.Email;
                        c.Telia_Id__c = u.Telia_Id__c;
                        c.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.CONTACT_RECTYPE_CYGATE_USER);
                        newCygateContacts.add(c);
                    }
                }

                if(newCygateContacts.size() > 0) {
                    String insertNewContactList = JSON.serialize(newCygateContacts);
                    if(insertNewContactList != null){
                        insertNewCygateContacts(insertNewContactList);
                    }
                } 
            }
        }
    }

    // Author S.S
    // To avoid Mixed DML operation error when both a User and Contact object is changed    
    @future private static void insertNewCygateContacts(string insertNewContactList){
        List<Contact> newCygateContacts = (List<Contact>)JSON.deserialize(insertNewContactList,List<Contact>.class);
        if(newCygateContacts.size() > 0) {
            insert newCygateContacts; 
        }
    }
    
    // The criteria for identifying Cygate users must be revised when Cygate Chatter Plus users are added to the system.
    private static List<User> filterCygateUsers(List<User> newList){
        Set<Id> cygateRoleIds = new Map<Id,UserRole>([SELECT Id FROM UserRole WHERE Name LIKE 'CG%']).keySet();
        List<User> cygateUsers = new List<User>();
        for(User u : newList){
            if(cygateRoleIds.contains(u.UserRoleId) && !SEUtility.isSystemAdmin(u)){
                cygateUsers.add(u);
            }
        }
        return cygateUsers;
    }

    private static Set<Id> loadExistingCygateContacts(List<User> cygateUsers){
        Set<Id> userIds = new Set<Id>();
        for(Contact c : [SELECT Id, Cygate_User__c FROM Contact WHERE Cygate_User__c in :cygateUsers]){
            userIds.add(c.Cygate_User__c);
        }
        return userIds;
    }

    private static Id cygateAccountId;
    private static Id getCygateAccountId(){
        if(cygateAccountId == null){
            List<Account> accList = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_N LIMIT 1];            
            if(accList.size() != 0){ // Note: Account should always exist in production
                cygateAccountId = accList[0].Id;
            } else {
                Account acc = new Account();
                acc.Name = 'Cygate Internal AB';
                acc.Org_Nr__c = label.Account_Org_N;
                acc.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_LARGE);
                insert acc;
                cygateAccountId = acc.Id;
            } 
        }
        return cygateAccountId;
    }

    //Author: N.G
    // Filter out the Telia users, and call future method to create contact records
    private static void createTeliaContacts(List<User> newList){
        // To avoid SOQL limit issues, do not create contacts when running in test mode unless specified.
        if(Test.isRunningTest() && testCreateContacts == false) return;

        List<User> teliaUsers = filterTeliaUsers(newList);
        
        if(teliaUsers.size() > 0 && avoidRecursion == false){
            avoidRecursion = true;

            if(!System.isFuture()){
                List<Contact> newTeliaContacts = new List<Contact>();
                Set<Id> userIdsWithTeliaContacts = loadExistingTeliaContacts(teliaUsers);

                for(User u : teliaUsers){
                    if(!userIdsWithTeliaContacts.contains(u.Id)){
                        Contact c = new Contact();
                        c.Is_Telia_User_Contact__c = true;
                        c.Telia_User__c = u.Id;
                        c.AccountId = getTeliaAccountId();
                        c.FirstName = u.FirstName;
                        c.LastName = u.LastName;
                        c.Telia_Id__c = u.Telia_Id__c;
                        c.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.CONTACT_RECTYPE_TELIA_USER);
                        newTeliaContacts.add(c);
                    }
                }

                if(newTeliaContacts.size() > 0){
                    String insertNewContacts = JSON.serialize(newTeliaContacts);
                    if(insertNewContacts != null){
                        insertNewTeliaContacts(insertNewContacts);
                    }
                }
            }
        }
    }

    // Author S.S
    // To avoid Mixed DML operation error when both a User and Contact object is changed    
    @future private static void insertNewTeliaContacts(string insertNewContacts){
        List<Contact> newTeliaContacts = (List<Contact>)JSON.deserialize(insertNewContacts,List<Contact>.class);
        if(newTeliaContacts.size() > 0) {
            insert newTeliaContacts;
        }
    }

    private static List<User> filterTeliaUsers(List<User> newList){
        Set<Id> teliaRoleIds = new Set<Id>();

        Set<String> topLevelUserRoles = new Set<String>();
        for(Telia_SF_User_Contact_Roles__c role : Telia_SF_User_Contact_Roles__c.getall().values()){
            topLevelUserRoles.add(role.Name);
        }
        
        // Add top level Telia roles
        for(UserRole ur : getAllUserRoles()){
            if(topLevelUserRoles.contains(ur.Name))
                teliaRoleIds.add(ur.id);
        }   
        // Get all subordinate roles to the top level Telia roles
        teliaRoleIds = addSubRoleIds(teliaRoleIds);

        List<User> teliaUsers = new List<User>();
        for(User u : newList){
            if(teliaRoleIds.contains(u.UserRoleId) && !SEUtility.isSystemAdmin(u))
                teliaUsers.add(u);
        }
        return teliaUsers;
    }

    // Author A.N
    // returns a set with the original role ids plus all subordinate role ids. Will call itself recursively until it has reached the bottom of the role hierarchy
    private static Set<Id> addSubRoleIds(Set<Id> roleIds) {
        Set<Id> subRoleIds = new Set<Id>();

        for (Id parentId : roleIds){
            for (UserRole ur : getAllUserRoles()){
                if(ur.ParentRoleId == parentId) subRoleIds.add(ur.Id);
            }
        }
        // Recursive call
        if (!subRoleIds.isEmpty()) subRoleIds.addAll(addSubRoleIds(subRoleIds));
        
        roleIds.addAll(subRoleIds);
        return roleIds;
    }

    private static List<UserRole> userRoles;
    private static List<UserRole> getAllUserRoles(){
        if(userRoles == null) userRoles = [SELECT Id, Name, ParentRoleId FROM UserRole];
        return userRoles;
    }
 
    private static Set<Id> loadExistingTeliaContacts(List<User> teliaUsers){
        Set<Id> userIds = new Set<Id>();
        for(Contact c : [SELECT Id, Telia_User__c FROM Contact WHERE Telia_User__c in :teliaUsers]){
            userIds.add(c.Telia_User__c);
        }
        return userIds;
    }

    // TODO: This method is also used by LeadTriggerHandler and should be moved tp SEUtility-class
    private static Id teliaAccountId;
    public static Id getTeliaAccountId(){
        if(teliaAccountId == null){
            List<Account> accList = [SELECT Id FROM Account WHERE Org_Nr__c = :label.Account_Org_Nr_Telia LIMIT 1];
            if(accList.size() != 0){ // Note: Account should always exist in production
                teliaAccountId = accList[0].Id;
            } else {
                Account acc = new Account();
                acc.Name = 'Telia';
                acc.Org_Nr__c = label.Account_Org_Nr_Telia;
                acc.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_LARGE);
                insert acc;
                teliaAccountId = acc.Id;
            }
        }
        return teliaAccountId;
    }
    
    //Author YK
    // For Leader board automation
    //SALEF-1126 Add/Revmove users from Leaderboard (Resultat & Poäng)
    // The criteria for identifying Cygate users with Cygate_Performance_User__c as true 
    @TestVisible private static List<User> cygatePerformanceUsers(List<User> newList){
        Set<Id> cygateRoleIds = new Map<Id,UserRole>([SELECT Id FROM UserRole WHERE Name LIKE 'CG%']).keySet();
        List<User> cygatePerformanceUsersList = new List<User>();
        for(User u : newList){
            if(cygateRoleIds.contains(u.UserRoleId) && !SEUtility.isSystemAdmin(u) && u.Cygate_Performance_User__c == true){
                cygatePerformanceUsersList.add(u);
            }
        }
        return cygatePerformanceUsersList;
    }
     
    //Author YK
    // For Leader board automation
    //SALEF-1126 Add/Revmove users from Leaderboard (Resultat & Poäng)
    @TestVisible private static Set<Id> loadExistingCygateYearlyPerformanceScore(List<User> cygateUsers){
        Set<Id> YearlyPerformanceScoreIds = new Set<Id>();
        for(Yearly_Performance_Score__c ys : [SELECT Id, User_Performance__c FROM Yearly_Performance_Score__c WHERE User_Performance__r.user__c in :cygateUsers  AND  User_Performance__r.User__r.Cygate_Performance_User__c = true]){
            YearlyPerformanceScoreIds.add(ys.id);
        }
        return YearlyPerformanceScoreIds;
    }
    
    //Author YK
    // For Leader board automation
    //SALEF-1126 Add/Revmove users from Leaderboard (Resultat & Poäng)
    @TestVisible private static Set<Id> loadExistingCygateUserPerformance(List<User> cygateUsers){
        Set<Id> UserPerformanceIds = new Set<Id>();
        for(User_Performance__c up : [SELECT Id, Name FROM User_Performance__c WHERE User__c in :cygateUsers  AND User__r.Cygate_Performance_User__c = true]){
            UserPerformanceIds.add(up.id);
        }
        return UserPerformanceIds;
    }
    
    
   
    //Author YK
    // For Leader board automation
    //SALEF-1126 Add/Revmove users from Leaderboard (Resultat & Poäng)
    @TestVisible @future private static void insertNewUserPerformance(string insertNewUP){
        List<User_Performance__c> newUPList = (List<User_Performance__c>)JSON.deserialize(insertNewUP,List<User_Performance__c>.class);
        if(newUPList.size() > 0) {
            insert newUPList ;
            List<Yearly_Performance_Score__c> newYearlyPerformanceScore = new List<Yearly_Performance_Score__c>();
            for(User_Performance__c usrPer : newUPList ){
                    //if(!YearlyPerformanceScoreIdsWithCygateUsers.contains(u.Id)){
                        Yearly_Performance_Score__c ys = new Yearly_Performance_Score__c();
                        //ys.Name = (Date.Today().Year()).format();
                        ys.Name = String.ValueOf(Date.Today().Year());
                        ys.Sales_Revenue_Target__c = 0;
                        ys.Competence_Score__c = 0;
                        ys.Result__c = 0;
                        system.debug('####userPer.id'+ usrPer.id);
                        ys.User_Performance__c = usrPer.id;
                        
                        newYearlyPerformanceScore.add(ys);
                    //}
                }
            insert newYearlyPerformanceScore;
        }
    }

    //Author YK
    // For Leader board automation
    //SALEF-1126 Add/Revmove users from Leaderboard (Resultat & Poäng)
    @TestVisible private static void createUserPerformance(List<User> newList){
         // To avoid SOQL limit issues, do not create contacts when running in test mode unless specified.
        //if(Test.isRunningTest() && testCreateContacts == false) return;
        
        System.debug('$$$In YeralyPerformance Method');
        
        //List<User> cygateUsers = filterCygateUsers(newList);
        List<User> cygateUsersWithCygatePerformance = cygatePerformanceUsers(newList);
        
        if(cygateUsersWithCygatePerformance.size() > 0 && avoidRecursionCPerfor == false){
            avoidRecursionCPerfor = true;
            
            if(!System.isFuture()) { 
            
                List<User_Performance__c > newUserPerformance = new List<User_Performance__c>();
                Set<Id> UserPerformanceIdsWithCygateUsers = loadExistingCygateUserPerformance(cygateUsersWithCygatePerformance);
                //List<Yearly_Performance_Score__c> newYearlyPerformanceScore = new List<Yearly_Performance_Score__c>();
                //Set<Id> YearlyPerformanceScoreIdsWithCygateUsers = loadExistingCygateYearlyPerformanceScore(cygateUsers);
                
                 for(User u : cygateUsersWithCygatePerformance){
                    if(!UserPerformanceIdsWithCygateUsers.contains(u.Id)){
                        User_Performance__c up = new User_Performance__c ();
                        up.Name = u.FirstName +' '+u.LastName;
                        up.User__c = u.id;
                        up.OwnerId = u.id;
                        newUserPerformance.add(up);
                       
                    }
                }
                
                if(newUserPerformance.size() > 0) {
                    String insertNewUserPerformanceList = JSON.serialize(newUserPerformance);
                    if(insertNewUserPerformanceList != null){
                    insertNewUserPerformance(insertNewUserPerformanceList);
                   // createYearlyPerformanceScore(newUserPerformance);
                    }
                }
                  
            }
        }
    }
    //Author D.C.
    //SALEF- 1496    This method will update the DefaultGroupNotificationFrequency = Email every Post and DigestFrequency= Daily. Before a User record is inserted.
    // Thus all Cygate record will have Chatter notification for all every-post.
    @TestVisible private static void setChatterFrequency(List<User> newList){
        List<User> cygateUsers = filterCygateUsers(newList);
        for(User u: cygateUsers){
            u.DefaultGroupNotificationFrequency = 'P';
            u.DigestFrequency = 'D';
            System.debug('$$$In setChatterFrequency Method user - ' + cygateUsers);
        }
    
    }
    
    //Author D.C.
    //SALEF- 1496  For any new Cygate User the cyagte chatter group ''Cygate - Användarsupport''  will be tag.
    @TestVisible private static void AddToGroup(List<User> newList){
        List<User> cygateUsers = filterCygateUsers(newList);
                        
            if(cygateUsers.size() > 0 && avoidRecursionchatter == false){
            avoidRecursionchatter = true;
         
                if(!System.isFuture()) {
                    Set<Id> UserWithCygateUsers = loadExistingCygateUser(cygateUsers);
                    List<CollaborationGroupMember>listGroupMember =new List<CollaborationGroupMember>(); 
            
                    for(User u : cygateUsers){
                        if(!UserWithCygateUsers.contains(u.id) && (!Test.isRunningTest())){
                        CollaborationGroupMember gm= new CollaborationGroupMember(); 
                        CollaborationGroup  g=[select Id from CollaborationGroup Where Name='Cygate - Användarsupport'];
                       
                        gm.CollaborationGroupId=g.id;
                        gm.MemberId = u.id;
                        listGroupMember.add(gm);
                        
                       }
                    }
                    
                    if(!listGroupMember.isEmpty()) {
                        String updatecygateUser = JSON.serialize(listGroupMember);
                        if(updatecygateUser != null){
                        UpdateCyagteUserNow(updatecygateUser);
                        }
                    }
                }
            }
        
    
    }
    
    @future private static void UpdateCyagteUserNow(string Updateuser){

        List<CollaborationGroupMember> newIDList = (List<CollaborationGroupMember>)JSON.deserialize(Updateuser,List<CollaborationGroupMember>.class);
            if(newIDList.size() > 0) {      
                insert newIDList ;
            }
        
    
    }
    
    @TestVisible private static Set<Id> loadExistingCygateUser(List<User> cygateUsers){
        Set<Id> cygateuserIds = new Set<Id>();
        If(!Test.isRunningTest()){
        for(CollaborationGroupMember up : [SELECT MemberId FROM CollaborationGroupMember WHERE CollaborationGroupId IN (SELECT ID FROM CollaborationGroup WHERE Name = 'Cygate - Användarsupport')]){
            cygateuserIds.add(up.MemberId);
        }
        }
        return cygateuserIds;
        
    }
    
    //D.C. 
    
     
}