/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RHA_TL_RelatedRecordUpdate_RA_Batch implements Database.Batchable<SObject> {
    global String namefield;
    global Set<Id> relatedrecords;
    global Map<Id,Boolean> rr_excludecoloring;
    global Map<Id,Boolean> rr_excludeonresource;
    global Map<Id,String> rr_names;
    global Map<Id,Boolean> rr_readonly;
    global Map<Id,String> rr_reference;
    global RHA_TL_RelatedRecordUpdate_RA_Batch(String c_namefield, Map<Id,String> c_rr_names) {

    }
    global RHA_TL_RelatedRecordUpdate_RA_Batch(String c_namefield, Set<Id> c_relatedrecords, Map<Id,String> c_rr_names, Map<Id,Boolean> c_rr_excludeonresource, Map<Id,Boolean> c_rr_excludecoloring, Map<Id,Boolean> c_rr_readonly) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
