/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RHA_TriggerDispatcher {
    global RHA_TriggerDispatcher() {

    }
    global static void entry(ResourceHeroApp.RHA_TriggerDispatcher.TriggerParameters tp) {

    }
global class TriggerParameters {
    global TriggerParameters(Boolean ib, Boolean ia, Boolean ii, Boolean iu, Boolean id, Boolean iud, Boolean ie, List<SObject> ol, List<SObject> nl, Map<Id,SObject> om, Map<Id,SObject> nm) {

    }
}
}
