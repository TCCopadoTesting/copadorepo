/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScriveSignedDocumentsProcessor implements Database.AllowsCallouts, Database.Batchable<scrive.ScriveSignedDocumentsProcessor.DocumentItem>, Database.Stateful {
    global ScriveSignedDocumentsProcessor(scrive.ScriveSignedDocumentsProcessor.Job currentJob) {

    }
    global void execute(Database.BatchableContext context, List<scrive.ScriveSignedDocumentsProcessor.DocumentItem> items) {

    }
    global void finish(Database.BatchableContext context) {

    }
    global List<scrive.ScriveSignedDocumentsProcessor.DocumentItem> start(Database.BatchableContext context) {
        return null;
    }
global class DocumentItem {
    global String remoteId;
    global String remoteTitle;
    global scrive.ScriveDocument getLocal() {
        return null;
    }
}
global interface Job {
    String getName();
    void process(scrive.ScriveSignedDocumentsProcessor.DocumentItem param0);
}
global class JobException extends Exception {
}
}
