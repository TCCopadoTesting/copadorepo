/**
About
-----
Description: AutoDirectController for the Auto Direct Action for both Lightning and Classic.
Create date: 31.10.2016

Update History
--------------
Created Dev 2016 - V.M 

Issues / TODOs
--------------     
*/



global class AutoDirectController{
    
    private static Boolean HasError { get; set; }
    private static List<String> ErrorMsgs { get; set; }
    
    public class ActionStatus{
        public List<String> ErrorMessages {get; set;}
        public String DirectedTo {get; set;}
        public Boolean IsSuccess {get; set;}
    }
    
    //For Salesforce Classic
    webservice static String webProcess(String leadId){
        return processRecord(leadId);
    }
    
    //For Salesforce Lightning
    @AuraEnabled
    public static String getResult(String leadId){
        return processRecord(leadId); 
    }
    
    private static String processRecord(String leadId){
        Lead lead = [SELECT Id, LeadSource, Account__c, Account__r.Owner.Name, Account__r.OwnerId, Account__r.Business_Unit__c FROM Lead WHERE Id = :leadId  LIMIT 1];
        ErrorMsgs = new List<String>();
        General_Lead_Settings__c settings = General_Lead_Settings__c.getValues('General Settings');  
        //Validate the lead
        validateLead(lead, settings);
        
        //Assign the lead
        String directedTo = assignLead(lead, settings);    
        lead.Status = LeadTriggerHandler.LEAD_STATUS_QUALIFY;
        
        if(!HasError){
            try{
                update lead;
            }catch(System.DmlException e){
                HasError = True;
                ErrorMsgs.add(e.getDmlMessage(0));
            } 
        }
        
        ActionStatus actionStatus = new ActionStatus();
        
        If(HasError){
            actionStatus.IsSuccess = False;
            actionStatus.ErrorMessages = ErrorMsgs;
        }else{
            actionStatus.IsSuccess = True;
            actionStatus.DirectedTo = directedTo;
        }
        return JSON.serialize(actionStatus);
    }     
    
    private static Boolean validateLead(Lead lead, General_Lead_Settings__c settings){
        HasError = False;
        if(lead.Account__c == null ){ 
            HasError = true;
            ErrorMsgs.add(Label.AD_Account_Missing);
        } 
        
        if(lead.Account__r.OwnerId == settings.AD_Integration_User_ID__c && lead.LeadSource != LeadTriggerHandler.LEAD_SOURCE_RESELLER){ 
            HasError = true;
            ErrorMsgs.add(Label.AD_Account_Owner_Missing);
        }
        
        return HasError;
    }
    
    private static String assignLead(Lead lead, General_Lead_Settings__c settings){
        String directedTo;
        Set<String> volumeSalesBU =  new Set<String>(settings.AD_Volume_Sales_BU__c.trim().split(';'));
        Set<String> tamBU =  new Set<String>(settings.AD_TAM_BU__c.trim().split(';'));

        if(volumeSalesBU.contains(lead.Account__r.Business_Unit__c)){
            //Assign to Volume Sales Queue
            lead.OwnerId = settings.AD_Volume_Sales_ID__c;
            directedTo = Label.AD_Volume_Sales;
            
        }else if(lead.Account__r.OwnerId == settings.AD_Integration_User_ID__c && 
                 tamBU.contains(lead.Account__r.Business_Unit__c) && 
                 lead.LeadSource == LeadTriggerHandler.LEAD_SOURCE_RESELLER){
                     //Assign to TAM Queue
                     lead.OwnerId = settings.AD_TAM_ID__c;
                     directedTo = Label.AD_Volume_Sales;
                 }else if(lead.Account__r.OwnerId != settings.AD_Integration_User_ID__c){
                     //Assign to Account Owner
                     lead.OwnerId = lead.Account__r.OwnerId;
                     directedTo =  lead.Account__r.Owner.Name;
                 }else{
                     HasError = true;
            		 ErrorMsgs.add(Label.AD_Cant_Route);
                 }
        
        return directedTo;
    }
}