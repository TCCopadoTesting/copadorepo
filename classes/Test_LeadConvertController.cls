@isTest
public class Test_LeadConvertController {
    
    @isTest static void testwrapperbethod(){
        Database.LeadConvertResult dbrslt;
        LeadConvertController.ActionStatus lc = new LeadConvertController.ActionStatus();
        lc.ErrorMessage = 'Test Error Log';
        lc.IsSuccess = true;
        lc.ReturnData = dbrslt;
    }
    
    @isTest static void testConvertLead(){
        Test.startTest();
        Lead testLead = Test_DataFactory.createLead();
        Account AccObj = Test_DataFactory.createOneAccount();
        insert AccObj;
        testLead.Account__c = AccObj.Id;
        insert testLead;
        Buy_Interest__c buyObj = new Buy_Interest__c();
        buyObj.Name = 'TestBuyInterestName';
        buyObj.Lead__c = testLead.Id;
        buyObj.Lead_Source__c = 'ÅF';
        buyObj.Status__c = 'Nytt'; 
        insert buyObj;       
        Contact testcontact = Test_DataFactory.createOneContactWithAccount();
        insert testcontact;
        String result = LeadConvertController.convertLead(testLead.Id, AccObj.Id, 'testoppName', buyObj.Id,'recordType',testcontact.Id,true);
        LeadConvertController.getLead(testLead.Id);
        LeadConvertController.getDuplicateContacts('test@dummy.com');
        LeadConvertController.getBuyInterests (testLead.Id);
        LeadConvertController.getOpportunityRecordTypes();
        test.stopTest();
    }
    
    @isTest static void testConvertLeadNegativeTest(){
        Test.startTest();
        Lead testLead = Test_DataFactory.createLead();
        Account AccObj = Test_DataFactory.createOneAccount();
        insert AccObj;
        testLead.Account__c = AccObj.Id;
        insert testLead;
        Buy_Interest__c buyObj = new Buy_Interest__c();
        buyObj.Name = 'TestBuyInterestName';
        buyObj.Lead__c = testLead.Id;
        buyObj.Lead_Source__c = 'ÅF';
        buyObj.Status__c = 'Nytt'; 
        insert buyObj;       
        Contact testcontact = Test_DataFactory.createOneContactWithAccount();
        insert testcontact;
        String result = LeadConvertController.convertLead(testLead.Id, testcontact.Id, '', buyObj.Id,'recordType',testcontact.Id,true);
        test.stopTest();
    }
    
    @isTest static void testBlockLead(){
        Test.startTest();
        Lead testLead = Test_DataFactory.createLead();
        Account AccObj = Test_DataFactory.createOneAccount();
        insert AccObj;
        testLead.Account__c = AccObj.Id;
        insert testLead;
        Contact testcontact = Test_DataFactory.createOneContactWithAccount();
        testcontact.recordtypeId = SEUtility.getRecordTypeId(SEUtility.CONTACT_RECTYPE_TELIA_USER);
        insert testcontact;
        Buy_Interest__c buyObj = new Buy_Interest__c();
        buyObj.Name = 'TestBuyInterestName';
        buyObj.Lead__c = testLead.Id;
        buyObj.Lead_Source__c = 'ÅF';
        buyObj.Status__c = 'Nytt'; 
        buyObj.Buy_Interest_Submitter__c = testcontact.Id;
        insert buyObj;       
        String result = LeadConvertController.convertLead(testLead.Id, AccObj.Id, '', buyObj.Id,'recordType',testcontact.Id,true);
        test.stopTest();
    }
}