/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SelectObjectComponentController {
    @RemoteAction
    global static List<scrive.SelectObjectComponentController.ResultCategory> find(String queryString, String among) {
        return null;
    }
global class ResultCategory {
}
}
