/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScriveDocumentRefresher implements Database.AllowsCallouts, Database.Batchable<scrive__ScriveDocument__c>, Database.Stateful {
    global List<String> errorMessages;
    global Integer numberOfFailures;
    global Integer numberOfSuccesses;
    global ScriveDocumentRefresher() {

    }
    global void execute(Database.BatchableContext BC, List<scrive__ScriveDocument__c> documents) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global List<scrive__ScriveDocument__c> start(Database.BatchableContext info) {
        return null;
    }
}
