global class BatchToAddAgreSitnOnNewlyCreatedKKNR implements Database.Batchable<sObject>{
    String query;
    Date todaysdate = system.today();
    String securitycategoryklass1 = 'Klass 1';
    String Securitycategoryklass2 = 'Klass 2';
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Set<Id> kknrRecTypeIds = new Set<Id>{SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_KK)};
        List<Account_Business_Unit__c> excludeAccountBU = Account_Business_Unit__c.getall().values();
        Set<String> businessName = new Set<String>();
        for(Account_Business_Unit__c bu : excludeAccountBU)
        {
            businessName.add(bu.Name);
        }
        query = 'SELECT Id, OwnerId FROM Account WHERE RecordTypeId IN :kknrRecTypeIds and CreatedDate >= :todaysdate and Business_Unit__c IN :businessName and (Security_Category__c != :securitycategoryklass1 OR Security_Category__c != :Securitycategoryklass2)';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> Listofkknr) {
        
        List<Agreement_Categories__c> listofagreementcategories = Agreement_Categories__c.getall().values();
        List<Agreement_Situation__c> insertlistofagreementsituationOnkknr = new List<Agreement_Situation__c>();
        
        if(Listofkknr.size() > 0)
        {
            for(Account acc : Listofkknr)
            {
                for(Agreement_Categories__c agreecat : listofagreementcategories)
                {   
                    Agreement_Situation__c agrementsituation = new Agreement_Situation__c();
                    agrementsituation.Agreement_Category__c = agreecat.Name;
                    agrementsituation.KKnr__c = acc.id;
                    agrementsituation.OwnerId = acc.OwnerId;
                    /*agrementsituation.Start_Date__c = System.today();
                    agrementsituation.End_Date__c = System.today() + 1;
                    agrementsituation.Supplier__c = 'Telia';*/
                    insertlistofagreementsituationOnkknr.add(agrementsituation);                        
                }                       
            }
          }
            if(insertlistofagreementsituationOnkknr.size() > 0)
            {
                
                try
                {
                    insert insertlistofagreementsituationOnkknr;
                }
                catch(Exception e)
                { 
                    system.debug('Exception Caught:'+e.getmessage());
                }                                  
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
        BatchToChangeAgreementSituationOwner b = new BatchToChangeAgreementSituationOwner();
        database.executeBatch(b,200);
    }
}