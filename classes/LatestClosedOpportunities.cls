public without sharing class LatestClosedOpportunities {

@AuraEnabled
    public static List<Opportunity> getlistoflatestclosedopportunity(){ 
    return [select Id, Name, OwnerId,Owner.SmallPhotoUrl, Owner.Name, Account.Owner.Name,Account.Name, Large_New_Sales__c, Large_Continuation_Sales__c ,CloseDate, Vunnen_Time_Stamp__c from Opportunity where (Owner.Profile.Name = 'Telia Sales - Large' OR Owner.Profile.Name = 'Telia Sales - Service' OR Owner.Profile.Name = 'Telia Sales - Solution') and Vunnen_Time_Stamp__c != null Order By Vunnen_Time_Stamp__c Desc Limit 10 ];

    }
    
 @AuraEnabled
    public static List<Opportunity> getlistoftopclosedopportunitycygate(){ 
    return [select Id, Name,Account.Name, Owner.name,Owner.SmallPhotoUrl,closedate,Probability,StageName,Total_Amount_Cygate__c,Total_Amount_Produkt__c,Total_Amount_Tj_nst__c,Cygate_Sales_Team__c from Opportunity WHERE CloseDate = THIS_MONTH AND StageName = 'Vunnen' AND ((Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_WEBORDER) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_AFFAR) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_AFTERREG) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_STANDARD) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_ENLARGEMENT)) ORDER BY Total_Amount_Cygate__c DESC Limit 10];

    }
    
@AuraEnabled
    public static List<Opportunity> getlistofpipelinedopportunitycygate(){ 
    return [select Id, Name,Account.Name, Owner.name,Owner.SmallPhotoUrl,closedate,Probability,StageName,Total_Amount_Cygate__c,Total_Amount_Produkt__c,Total_Amount_Tj_nst__c,Cygate_Sales_Team__c from Opportunity WHERE (CloseDate = THIS_MONTH OR CloseDate = THIS_YEAR) AND (StageName != 'Vunnen' AND StageName != 'Förlorad' AND StageName != 'Avbruten' AND StageName != 'Avfärdat prospect' AND StageName != 'Closed MF' AND StageName != 'stängd') AND ((Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_WEBORDER) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_AFFAR) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_AFTERREG) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_STANDARD) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_ENLARGEMENT)) ORDER BY Total_Amount_Cygate__c DESC Limit 10];

    }
    
    @AuraEnabled
    public static List<Opportunity> getlistoflatestclosedopportunitycygate(){ 
    return [select Id, Name,Account.Name, Owner.name,Owner.SmallPhotoUrl,closedate,Probability,StageName,Total_Amount_Cygate__c,Total_Amount_Produkt__c,Total_Amount_Tj_nst__c,Cygate_Sales_Team__c,Vunnen_Time_Stamp__c from Opportunity WHERE CloseDate = THIS_MONTH AND StageName = 'Vunnen' AND ((Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_WEBORDER) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_AFFAR) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_AFTERREG) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_STANDARD) OR (Recordtype.DeveloperName =: SEUtility.OPP_RECTYPE_CYGATE_ENLARGEMENT)) AND Vunnen_Time_Stamp__c != null ORDER BY Vunnen_Time_Stamp__c DESC Limit 10];

    }

}