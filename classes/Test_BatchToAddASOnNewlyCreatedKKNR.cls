@isTest
public with sharing class Test_BatchToAddASOnNewlyCreatedKKNR{

@isTest static void BatchToAddAgreSitnOnNewlyCreatedKKNR(){
       
        Account_Business_Unit__c cs = new Account_Business_Unit__c();
        cs.Name = 'test1';
        insert cs;
        List<Account> listofaccount = new List<Account>();
        Account acc1 = Test_DataFactory.createOneKundkontoAccount();
        acc1.Business_Unit__c = 'test1';
        acc1.Is_Owner_Changed__c = system.today();
        listofaccount.add(acc1);
        
       Account acc2 = new Account(Name='TestAccount 123');
        acc2.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_KK);
        acc2.Business_Unit__c = 'test1';
        acc2.Kundkonto_Nr__c = 'abc' + 00;
        listofaccount.add(acc2);
        insert listofaccount; 
        List<Agreement_Situation__c> insertlistofagreementsituationOnkknr = new List<Agreement_Situation__c>(); 
        Agreement_Situation__c agre1 = new Agreement_Situation__c();
        agre1.Agreement_Category__c = 'UC/Mobil';
        agre1.Supplier__c = 'Telia';
        agre1.KKnr__c = acc1.id;
        agre1.End_Date__c = system.today() ;
        insertlistofagreementsituationOnkknr.add(agre1);
        insert insertlistofagreementsituationOnkknr;
        Test.startTest();
        BatchToAddAgreSitnOnNewlyCreatedKKNR obj = new BatchToAddAgreSitnOnNewlyCreatedKKNR();
        DataBase.executeBatch(obj);             
        Test.stopTest();  
    }
}