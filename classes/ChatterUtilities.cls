/**
About
-----
Description: Utility Class for Chatter functionality in Apex

The ChatterUtilities class is used by several Process Builders. The invokable method createChatterPosts will create new chatter posts when f.ex. new tasks/events are created.

The class also have general utility methods for creating new feed items

Create date: April 2016

Update History
--------------
Created Jun 2015 - A.N Created
Updated Sep 2016 - Refactored: Cleaned up in debug statements.

Issues / TODOs
--------------  

*/

public with sharing class ChatterUtilities {

    // When system tests are running, the ConnectAPI is only available if SeeAllData=true.
    // We therefore do not post the ChatterPosts when unit tests are running unless this is specifically stated (by setting testChatterPosts = true)
    @TestVisible private static Boolean testChatterPosts = false;

    @InvocableMethod
    public static void createChatterPosts(List<Id> sObjectIds){
        System.debug('createChatterPosts input sObjectIds: ' + sObjectIds);
        List<ConnectApi.BatchInput> chatterPosts = new List<ConnectApi.BatchInput>();

        // Create Chatter posts when new Contact is created on an Account
        for(Contact c : [SELECT Id, Name, AccountId FROM Contact WHERE Id in : sObjectIds]){
            if(c.AccountId != null){
                ConnectApi.FeedItemInput item = createFeedItem(c.AccountId, System.Label.CHATTER_New_Contact_created_on_Account, '/' + c.Id, c.Name);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            }
        }

        // Create Chatter posts when new opportunity is created on an Account
        for(Opportunity opp : [SELECT Id, Name, AccountId, RecordTypeId FROM Opportunity WHERE Id in : sObjectIds]){
            if(opp.AccountId != null){
                String text = (SEUtility.getRecordTypeDevName(opp.RecordTypeId) == SEUtility.OPP_RECTYPE_PROSPECT ? System.Label.CHATTER_New_Prospect_created_on_Account : System.Label.CHATTER_New_Opportunity_created_on_Account);
                ConnectApi.FeedItemInput item = createFeedItem(opp.AccountId, text, '/' + opp.Id, opp.Name);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            }
        }

        // Create Chatter posts on opportunity feed when:
        // a) A new event is created with a future start date. Also post to contact.
        // b) An event related to an opportunity is completed.
        for(Event ev : [SELECT Id, Subject, WhoId, WhatId, StartDateTime, Event_Complete__c FROM Event WHERE Id in : sObjectIds]){
            if(ev.Event_Complete__c && ev.WhatId != null && String.valueOf(ev.WhatId).substring(0,3) == Opportunity.sObjectType.getDescribe().getKeyPrefix()){
                ConnectApi.FeedItemInput item = createFeedItem(ev.WhatId, System.Label.CHATTER_Event_closed_on_Opportunity, '/' + ev.Id, ev.Subject);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            } else if(ev.StartDateTime > System.now() && ev.WhatId != null && String.valueOf(ev.WhatId).substring(0,3) == Opportunity.sObjectType.getDescribe().getKeyPrefix()){
                ConnectApi.FeedItemInput item = createFeedItem(ev.WhatId, System.Label.CHATTER_New_Event_created_on_Contact_Opportunity, '/' + ev.Id, ev.Subject);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            }
            if(ev.StartDateTime > System.now() && ev.WhoId != null && String.valueOf(ev.WhoId).substring(0,3) == Contact.sObjectType.getDescribe().getKeyPrefix()){
                ConnectApi.FeedItemInput item = createFeedItem(ev.WhoId, System.Label.CHATTER_New_Event_created_on_Contact_Opportunity, '/' + ev.Id, ev.Subject);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            }
        }

        // Create Chatter posts on opportunity feed when:
        // a) A new task is created with a future start date. Also post to contact.
        // b) An task related to an opportunity is closed.
        for(Task task : [SELECT Id, Subject, WhoId, WhatId, ActivityDate, IsClosed FROM Task WHERE Id in : sObjectIds]){
            if(task.IsClosed && task.WhatId != null && String.valueOf(task.WhatId).substring(0,3) == Opportunity.sObjectType.getDescribe().getKeyPrefix()){
                ConnectApi.FeedItemInput item = createFeedItem(task.WhatId, System.Label.CHATTER_Task_closed_on_Opportunity, '/' + task.Id, task.Subject);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            } else if(task.ActivityDate > System.today() &&task.WhatId != null && String.valueOf(task.WhatId).substring(0,3) == Opportunity.sObjectType.getDescribe().getKeyPrefix()){
                ConnectApi.FeedItemInput item = createFeedItem(task.WhatId, System.Label.CHATTER_New_Task_created_on_Contact_Opportunity, '/' + task.Id, task.Subject);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            }
            if(task.ActivityDate > System.today() && task.WhoId != null && String.valueOf(task.WhoId).substring(0,3) == Contact.sObjectType.getDescribe().getKeyPrefix()){
                ConnectApi.FeedItemInput item = createFeedItem(task.WhoId, System.Label.CHATTER_New_Task_created_on_Contact_Opportunity, '/' + task.Id, task.Subject);
                chatterPosts.add(new ConnectApi.BatchInput(item));
            }
        }

        if(!chatterPosts.isEmpty() && (!Test.isRunningTest() || testChatterPosts)){
            System.debug('ChatterUtilities.createChatterPosts number of new feed items: ' + chatterPosts.size());
            ConnectApi.ChatterFeeds.postFeedElementBatch(null, chatterPosts);
        }
    }

    public static ConnectApi.FeedItemInput createFeedItem(Id subjectId, String text){
        return createFeedItem(subjectId, text, null, null);
    }

    public static ConnectApi.FeedItemInput createFeedItem(Id subjectId, String text, String optUrl, String optUrlName){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        feedItemInput.body = new ConnectApi.MessageBodyInput();
        feedItemInput.subjectId = subjectId;

        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
        feedItemInput.body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        textSegment.text = text;
        feedItemInput.body.messageSegments.add(textSegment);

        if(optUrl != null){
            ConnectApi.FeedElementCapabilitiesInput capability = new ConnectApi.FeedElementCapabilitiesInput();
            ConnectApi.LinkCapabilityInput link = new ConnectApi.LinkCapabilityInput();
            link.url = optUrl;
            link.urlName = optUrlName;
            capability.link = link;
            feedItemInput.capabilities = capability;
        }

        return feedItemInput;
    }
}