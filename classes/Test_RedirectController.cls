@isTest
public class Test_RedirectController {
    
    @testSetup
    static void initTestData() {
        Test_DataFactory.setUpOrg();
    }

    @isTest
    static void testSMEUser(){
        User u;
        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            u = Test_DataFactory.createSMEUser();
            insert u;
        }

        System.runAs(u){
            RedirectController controller = new RedirectController();  
            PageReference pr = controller.redirect();
            System.assertEquals('/apex/CommitView', pr.getUrl());
        }
    }
    
    
    @isTest
    static void testLargeUser(){
        User u;
        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            u = Test_DataFactory.createLargeUser();
            insert u;
        }
        
        System.runAs(u){
            RedirectController controller = new RedirectController();  
            PageReference pr = controller.redirect();
            System.assertEquals('/apex/NewCommitViewLarge', pr.getUrl());
        }
    }
    
}