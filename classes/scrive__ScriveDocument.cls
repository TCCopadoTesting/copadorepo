/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScriveDocument {
    global List<scrive.ScriveDocumentParty> parties {
        get;
    }
    global scrive__ScriveDocument__c record {
        get;
    }
    global scrive.ScriveDocumentParty addParty(SObject partyRecord) {
        return null;
    }
    global static scrive.ScriveDocument createEmpty() {
        return null;
    }
    global static scrive.ScriveDocument createFromProcessTemplate(Id processTemplateId, Id sourceRecordId) {
        return null;
    }
    global static scrive.ScriveDocument createFromTemplate(String templateId) {
        return null;
    }
    global void fillIn() {

    }
    global void saveLocally() {

    }
}
