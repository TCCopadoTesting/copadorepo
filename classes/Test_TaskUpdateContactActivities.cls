/* Author P.P July 2017
* NOTE: This test class uses the @IsTest(SeeAllData=true), as this is required for querying TaskWhoRelations in TaskTriggerHandler by run as User who does not have 'View All Data' True.
* In general, the @IsTest(SeeAllData=true) should never by used, and required data should be created in the test method.
- July 2017 - P.P : SALEF-73 Test Class for TaskTriggerHandler, covers part of UpdateContactActivities
*/

@isTest(SeeAllData = True)
public with sharing class Test_TaskUpdateContactActivities {
@isTest static void testUpdateContactActivities(){
        User uR1 = new User();
        User uL1 = new user();
        user uC1 = new User();
        user uSM1 = new User();
        user uM1 = new User();
        string roleuR1 = '';
        string roleuL1 = '';
        string roleuC1 = '';
        string roleuSM1 = '';
        string roleuM1 = '';
        Account acc1 = new Account();
        Account acc2 = new Account();
        Account acc3 = new Account();
        Account acc4= new Account();
        List<Account> accList = new List<Account>();
        Campaign camp = new Campaign();
        Contact conL = new Contact();
        Contact conC = new Contact();
        Contact conR = new Contact();
        Contact conSM = new Contact();
        Contact conM = new Contact();
        List<Id> conIdList = new List<Id>();
        List<Contact> contactList = new List<Contact>();
       task tL = new Task();
        System.runAs(Test_DataFactory.getByPassUser()){//to avoid MIXED_DML operation
            List<User> renewalUserList = Test_DataFactory.createLargeRenewalUserTeam(1);
            uR1 = renewalUserList[0];
            roleuR1 = uR1.UserRoleId;
            List<user> largeUserList = Test_DataFactory.createLargeUserTeam(2);
            uL1 = largeUserList[0];
            roleuL1 = uL1.UserRoleId;
            uC1 = Test_DataFactory.createCygateUser();
            insert uC1;
            roleuC1 = uC1.userRoleId;
            List<User> uSMList = Test_DataFactory.createSMUserTeam(1);
            uSM1 = uSMList[0];
            roleuSM1 = uSM1.userRoleId;
            List<User> uMList = Test_DataFactory.createMarketingUserTeam(1);
            uM1 = uMList[0];
            roleuM1 = uM1.userRoleId;
        }
        Test.startTest(); //D.C.
        acc1 = Test_DataFactory.createOneLargeStandardAccount();
        acc1.OwnerId = uL1.Id;
        acc1.Cygate_Account_Manager__c = uC1.Id;
        accList.add(acc1);
        acc2 = Test_DataFactory.createOneLargeStandardAccount();
        acc2.OwnerId = uR1.Id;
        accList.add(acc2);
        acc3 = Test_DataFactory.createOneLargeStandardAccount();
        acc3.OwnerId = uSM1.Id;
        accList.add(acc3);
        acc4 = Test_DataFactory.createOneLargeStandardAccount();
        acc4.OwnerId = uM1.Id;
        accList.add(acc4);
        insert accList;
        acc1 = accList[0];
        acc2 = accList[1];
        acc3 = accList[2];
        acc4 = accList[3];

        camp = Test_DataFactory.createCygateCampaign();
        insert camp;
        conL = new Contact(LastName='TestLastName',AccountId = acc1.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testl@dummy.com',Role__c = 'Annan');
        contactList.add(conL);
        conC = new Contact(LastName='TestLastName',AccountId = acc1.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testC@dummy.com',Role__c = 'Annan');
        contactList.add(conC);
        conR = new Contact(LastName='TestLastName',AccountId = acc2.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testR@dummy.com',Role__c = 'Annan');
        contactList.add(conR);
        conM = new Contact(LastName='TestLastName',AccountId = acc4.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testM@dummy.com',Role__c = 'Annan');
        contactList.add(conM);
        conSM = new Contact(LastName='TestLastName',AccountId = acc3.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testSM@dummy.com',Role__c = 'Annan');
        contactList.add(conSM);
        insert contactList;
        conL = contactList[0];
        conIdList.add(conL.Id);
        conC = contactList[1];
        conIdList.add(conC.Id);
        conR = contactList[2];
        conIdList.add(conR.Id);
        conM = contactList[3];
        conIdList.add(conM.Id);
        conSM = contactList[4];
        conIdList.add(conSM.Id);
        List<Task> taskList = Test_DataFactory.createTasks(6);           
        List<Event> eventList = Test_DataFactory.createEvents(2);
        
        system.runAs(uL1){
            tL = taskList[0];
            tL.OwnerId = uL1.Id;
            tL.WhoId = conL.Id;
            insert tL;
            event eL = eventList[0];
            eL.WhoId = conL.Id;
            eL.OwnerId = uL1.Id;
            insert eL;
            }
            
            system.runAs(uC1){
            task tC = taskList[1];
            tC.OwnerId = uC1.Id;
            tC.WhoId = conC.Id;
            tC.Campaign__c = camp.Id;
            tC.Bredda_aff_ren__c=true;
                tC.ka_avtalad_aff_r__c=true;
                tC.V_xa_befintlig_aff_r__c=true;
                tC.Customer_status__c='Ny kund';
            insert tC;
            
            tC.WhoId = conL.Id;
            update tC;
            
            tC.WhoId = NULL;
            update tC;
            
        }
        
        system.runAs(uL1){
            tL.WhoId = conC.Id;
            update tL;
            
            tL.WhoId = NULL;
            update tL;
            tL.OwnerId = uC1.Id;
            update tL;
            system.debug('after update ownner -->'+tL.OwnerId);
            
        }   
        
        system.runAs(uR1){
            task tR = taskList[2];
            tR.OwnerId = uR1.Id;
            tR.WhoId = conR.Id;
            insert tR;
            
            Task tR1 = taskList[3];
            tR1.OwnerId = uR1.Id;
            insert tR1;
            
            tR1.WhoId = conR.Id;
            update tR1; 
            
            List<Task> tRList = new List<Task>();
            tR.WhoId = NULL;
            tR1.WhoId = NULL;
            tRList.add(tR);
            tRList.add(tR1);
            update tRList;
        }
        
        system.runAs(uM1){
            task tM = taskList[4];
            tM.OwnerId = uM1.Id;
            tM.WhoId = conM.Id;
            insert tM;
            
            delete tM;
        }
        
        system.runAs(uSM1){
            task tSM = taskList[5];
            tSM.OwnerId = uSM1.Id;
            tSM.WhoId = conSM.Id;
            insert tSM;   
                     
            tSM.OwnerId = uM1.Id;
            update tSM;
            
            tSM.WhoId = NULL;
            update tSM;
            Test.stopTest(); //D.C.
        }
        List<Contact> conSAList = [Select Id, Telia_Activities__c,Cygate_Activities__c,Other_Activities__c,Service_Management_Activities__c,Marketing_Activities__c FROM COntact WHERE Id IN: conIdList];
        for(contact con:conSAList){
            if(conL.Id == con.Id){
                system.assertequals(con.Telia_Activities__c,TRUE);
                system.assertequals(con.Cygate_Activities__c,FALSE);
            }
            if(conC.Id == con.Id){
                system.assertequals(con.Cygate_Activities__c,False);
            }
            if(conR.Id == con.Id){
                system.assertequals(con.Other_Activities__c,False);
            }
            if(conM.Id == con.Id){
                system.assertequals(con.Marketing_Activities__c,False);
            }
        }       
    }
}