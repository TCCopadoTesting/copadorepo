/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScriveDocumentParty {
    global Object getFieldValue(String fieldName) {
        return null;
    }
    global String getSigningPageUrl() {
        return null;
    }
    global void resetPartyRecord(SObject partyRecord) {

    }
    global void setFieldValue(String fieldName, Object fieldValue) {

    }
}
