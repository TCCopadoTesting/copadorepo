/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MatrixController {
    @RemoteAction
    global static List<Map<String,String>> checkForAssignmentChanges(List<Map<String,String>> assignments, String action, List<String> relatedrecords, Boolean showarchivedassignments, String listviewid) {
        return null;
    }
    @RemoteAction
    global static Map<String,Map<String,Map<String,String>>> getAllObjectDetailsAndListviews() {
        return null;
    }
    @RemoteAction
    global static List<ResourceHeroApp__Resource_Assignment__c> getAssignmentsForResourceListView(List<String> relatedrecords, Boolean showarchivedassignments) {
        return null;
    }
    @RemoteAction
    global static List<Map<String,String>> getAssignmentsForResourceListView_so(List<String> relatedrecords, Boolean showarchivedassignments) {
        return null;
    }
    @RemoteAction
    global static List<SObject> getAssignmentsFromListview(String selectedobject, String listviewid) {
        return null;
    }
    @RemoteAction
    global static List<Map<String,String>> getAssignmentsFromListview_so(String selectedobject, String listviewid) {
        return null;
    }
    @RemoteAction
    global static List<ResourceHeroApp__Resource_Assignment__c> getAssignments(List<String> relatedrecords, Boolean showarchivedassignments) {
        return null;
    }
    @RemoteAction
    global static List<Map<String,String>> getAssignments_so(List<String> relatedrecords, Boolean showarchivedassignments) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getForecastCount(String assignmentstrings, String FromDateAsString, String ToDateAsString, String viewby, Integer queryrecordlimit) {
        return null;
    }
    @RemoteAction
    global static List<ResourceHeroApp__Resource_Forecast__c> getForecasts(List<String> assignments, String startdate, String enddate, String viewby) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static List<List<String>> getInitialHighlights(Id MainRecordId, List<String> data, List<String> headers, String FromDateAsString, String ToDateAsString, String AorF) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static List<List<String>> getInitialHighlights_v2(Id MainRecordId, List<String> data, List<String> headers, String FromDateAsString, String ToDateAsString, String AorF, String ViewBy) {
        return null;
    }
    @RemoteAction
    global static List<Id> getLiewViewRecordIds(String selectedobject, String listviewid) {
        return null;
    }
    @RemoteAction
    global static Map<String,Map<String,String>> getMatrixEnabledFieldsMap() {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static Map<String,ResourceHeroApp__RHA_Matrix_Enabled_Fields__c> getMatrixEnabledFields() {
        return null;
    }
    @RemoteAction
    global static List<Map<String,String>> getMatrixFields() {
        return null;
    }
    @RemoteAction
    global static List<ResourceHeroApp__Resource_Forecast__c> getOtherForecasts(List<String> assignments, List<String> resources, String startdate, String enddate, String viewby) {
        return null;
    }
    @RemoteAction
    global static Map<String,Map<String,String>> getRelatedRecordDetails(List<Id> relatedids) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getSaveHOT_Notes_v1(String MainRecordString, String FromDateAsString, String ToDateAsString, String AorF, String viewby, List<String> data) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getSaveHOT_WeekView(String MainRecordString, String FromDateAsString, String ToDateAsString, String AorF, String viewby, List<String> data) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getSaveHOT(String MainRecordString, String FromDate, String ToDate, List<String> data) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getSaveHOT_v2(String MainRecordString, String FromDateAsString, String ToDateAsString, String AorF, List<String> data) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getSaveHOT_v3(String MainRecordString, String FromDateAsString, String ToDateAsString, String AorF, String viewby, List<String> data) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> getSettings() {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String newSavePeriod(List<String> assignments, String startdate, String enddate, String viewby, String AorF, Map<String,String> data) {
        return null;
    }
    @RemoteAction
    global static String newSave_Notes(List<String> assignments, String startdate, String enddate, String viewby, String AorF, Map<String,String> data) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String newSave(List<String> assignments, String startdate, String enddate, String viewby, String AorF, Map<String,String> data) {
        return null;
    }
    @RemoteAction
    global static Map<String,Map<String,String>> quickAssignSave(String assignmentid, String resourceid) {
        return null;
    }
    @RemoteAction
    global static String saveRH(List<String> assignments, String startdate, String enddate, String viewby, String AorF, Map<String,String> data) {
        return null;
    }
    @RemoteAction
    global static List<SObject> searchRecords(String queryString, String objectName, List<String> fieldNames, String fieldsToSearch, String filterClause, String orderBy, Integer recordLimit) {
        return null;
    }
    @RemoteAction
    global static String shiftForecasts(String mainrecordid, String cutoffdate, Integer numweeks) {
        return null;
    }
}
