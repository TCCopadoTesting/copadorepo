@isTest
public class Test_SEUtility {
    
    @isTest static void getFirstDayOfYear(){
        for(Integer i=3500; i<5000; i++){
           // System.debug('First day of the year '+i+' is '+SEUtility.getFirstDayOfTeliaYear(i).format());
           // System.debug('Last day of the year '+i+' is '+SEUtility.getLastDayOfTeliaYear(i).format());
        }
    }
    
    @isTest static void testLastAndFirstDayOfYearForNext15years(){
        Map<Integer, Date> startDayMap = new Map<Integer,Date>();
        Map<Integer, Date> endDayMap = new Map<Integer, Date>();
        
        startDayMap.put(2015, Date.newInstance(2014, 12, 29));
        endDayMap.put(2015, Date.newInstance(2016, 1, 3));
        
        startDayMap.put(2016, Date.newInstance(2016, 1, 4));
        endDayMap.put(2016, Date.newInstance(2017, 1, 1));
        
        startDayMap.put(2017, Date.newInstance(2017, 1, 2));
        endDayMap.put(2017, Date.newInstance(2017, 12, 31));
        
        startDayMap.put(2018, Date.newInstance(2018, 1, 1));
        endDayMap.put(2018, Date.newInstance(2018, 12, 30));
        
        startDayMap.put(2019, Date.newInstance(2018, 12, 31));
        endDayMap.put(2019, Date.newInstance(2019, 12, 29));
        
        startDayMap.put(2020, Date.newInstance(2019, 12, 30));
        endDayMap.put(2020, Date.newInstance(2021, 1, 3));
        
        startDayMap.put(2021, Date.newInstance(2021, 1, 4));
        endDayMap.put(2021, Date.newInstance(2022, 1, 2));
        
        startDayMap.put(2022, Date.newInstance(2022, 1, 3));
        endDayMap.put(2022, Date.newInstance(2023, 1, 1));
        
        startDayMap.put(2023, Date.newInstance(2023, 1, 2));
        endDayMap.put(2023, Date.newInstance(2023, 12, 31));
        
        startDayMap.put(2024, Date.newInstance(2024, 1, 1));
        endDayMap.put(2024, Date.newInstance(2024, 12, 29));
        
        startDayMap.put(2025, Date.newInstance(2024, 12, 30));
        endDayMap.put(2025, Date.newInstance(2025, 12, 28));
        
        startDayMap.put(2026, Date.newInstance(2025, 12, 29));
        endDayMap.put(2026, Date.newInstance(2027, 1, 3));
        
        startDayMap.put(2027, Date.newInstance(2027, 1, 4));
        endDayMap.put(2027, Date.newInstance(2028, 1, 2));
        
        startDayMap.put(2028, Date.newInstance(2028, 1, 3));
        endDayMap.put(2028, Date.newInstance(2028, 12, 31));
        
        startDayMap.put(2029, Date.newInstance(2029, 1, 1));
        endDayMap.put(2029, Date.newInstance(2029, 12, 30));
        
        startDayMap.put(2030, Date.newInstance(2029, 12, 31));
        endDayMap.put(2030, Date.newInstance(2030, 12, 29));
        
        startDayMap.put(2031, Date.newInstance(2030, 12, 30));
        endDayMap.put(2031, Date.newInstance(2031, 12, 28));
        
        startDayMap.put(2032, Date.newInstance(2031, 12, 29));
        endDayMap.put(2032, Date.newInstance(2033, 01, 02));
        
        startDayMap.put(2033, Date.newInstance(2033, 01, 03));
        endDayMap.put(2033, Date.newInstance(2034, 01, 01));
        
        startDayMap.put(2034, Date.newInstance(2034, 01, 02));
        endDayMap.put(2034, Date.newInstance(2034, 12, 31));
        
        startDayMap.put(2035, Date.newInstance(2035, 01, 01));
        endDayMap.put(2035, Date.newInstance(2035, 12, 30));
        
        startDayMap.put(2036, Date.newInstance(2035, 12, 31));
        endDayMap.put(2036, Date.newInstance(2036, 12, 28));
        
        for(Integer i=2016; i<=2036; i++){
            System.debug(SEUtility.getFirstDayOfTeliaYear(i)+' is the first day of the year '+i);
            System.debug(SEUtility.getLastDayOfTeliaYear(i)+' is the last day of the year '+i);
            System.assertEquals(startDayMap.get(i), SEUtility.getFirstDayOfTeliaYear(i), 'The first day of year '+i+' should be '+startDayMap.get(i)+' but is '+((DateTime)SEUtility.getFirstDayOfTeliaYear(i)).format('E')+' '+SEUtility.getFirstDayOfTeliaYear(i).format());
        	System.assertEquals(endDayMap.get(i), SEUtility.getLastDayOfTeliaYear(i), 'The last day of year '+i+' should be '+endDayMap.get(i)+' but is '+SEUtility.getLastDayOfTeliaYear(i).format());
            if(i==2015||i==2020 || i==2026 || i==2032){
                System.assertEquals(371, SEUtility.getFirstDayOfTeliaYear(i).daysBetween(SEUtility.getLastDayOfTeliaYear(i))+1, 'Number of days for year '+i+' doesn\'t match');
            }
            else{
            	System.assertEquals(364, SEUtility.getFirstDayOfTeliaYear(i).daysBetween(SEUtility.getLastDayOfTeliaYear(i))+1, 'Number of days for year '+i+' doesn\'t match');    
            }
        }
    }
    
    @isTest static void testTeliaQuarterCreationForNext15years(){
        Map<Integer,Map<Date,Date>> tempReturnMap = new Map<Integer,Map<Date,Date>>();
        for(Integer i=2016; i<=2036; i++){
        	tempReturnMap = SEUtility.getTeliaQuarterStartAndEndDates(i);
            //For every quarter
            for(Integer j : tempReturnMap.KeySet()){
                //For every enddate in every quarter
                for(Date d : tempReturnMap.get(j).keySet()){
                    //If the year has 53 weeks
                    if(j==4 && (i==2015||i==2020 || i==2026 || i==2032)){
                    	System.assertEquals(98, d.daysBetween(tempReturnMap.get(j).get(d))+1, 'Number of days for year '+i+' Q'+j+' doesn\'t match up');
                    }
                    else{
                        System.assertEquals(91, d.daysBetween(tempReturnMap.get(j).get(d))+1, 'Number of days between '+d.format()+' and '+tempReturnMap.get(j).get(d).format()+' for year '+i+' Q'+j+' doesn\'t match up');
                    }
                }
            }
        }   
    }

    @isTest static void testRecordTypes(){
        System.runAs(Test_DataFactory.getByPassUser()){  // V.A  Bypassing User
        Account acc = Test_DataFactory.createOneAccount();
        insert acc;

        Account accAfterInsert = [Select Id, RecordTypeId, RecordType.DeveloperName From Account Where id = :acc.Id LIMIT 1];
        String recTypeName = SEUtility.getRecordTypeDevName(accAfterInsert.RecordTypeId);
        System.assertEquals(recTypeName, accAfterInsert.RecordType.DeveloperName);
        Id recTypeId = SEUtility.getRecordTypeId(accAfterInsert.RecordType.DeveloperName);
        System.assertEquals(recTypeId, accAfterInsert.RecordTypeId);
    }
    }
    @isTest static void testSolutionAreaName(){
        System.assertEquals('Unified and Mobile Communications', SEUtility.getSolutionAreaName('Unified Communications'));
        System.assertEquals('Services', SEUtility.getSolutionAreaName('Service'));
        System.assertEquals('Cloud Networking', SEUtility.getSolutionAreaName('Cloud Networking'));
        System.assertEquals('Mobility Management and Business Apps', SEUtility.getSolutionAreaName('Mobility Management'));
		System.assertEquals('IBS', SEUtility.getSolutionAreaName('IBS'));
        System.assertEquals('Other', SEUtility.getSolutionAreaName('Other'));
        System.assertEquals('Cygate Nätverk', SEUtility.getSolutionAreaName('Cygate Nätverk'));
        System.assertEquals('Cygate Säkerhet', SEUtility.getSolutionAreaName('Cygate Säkerhet'));
        System.assertEquals('Cygate Server/Lagring/Applikation', SEUtility.getSolutionAreaName('Cygate Server/Lagring/Applikation'));
        System.assertEquals('Cygate UC', SEUtility.getSolutionAreaName('Cygate UC'));
        System.assertEquals('Cygate DMD', SEUtility.getSolutionAreaName('Cygate DMD'));
        System.assertEquals('Cygate Infrastruktur', SEUtility.getSolutionAreaName('Cygate Infrastruktur'));
        System.assertEquals('Cygate Övrigt', SEUtility.getSolutionAreaName('Cygate Övrigt'));
    }
    
    @isTest static void testSolutionAreaCode(){
        System.assertEquals('UC ', SEUtility.getSolutionAreaCode('Unified and Mobile Communications'));
        System.assertEquals('Services ', SEUtility.getSolutionAreaCode('Services'));
        System.assertEquals('CN ', SEUtility.getSolutionAreaCode('Cloud Networking'));
        System.assertEquals('MM ', SEUtility.getSolutionAreaCode('Mobility Management and Business Apps'));
        System.assertEquals('IBS ', SEUtility.getSolutionAreaCode('IBS'));
        System.assertEquals('', SEUtility.getSolutionAreaCode('Other'));
    }
    
    @isTest static void testWriteMultiPicklistString(){
        System.assertEquals('Test1;Test2;Test3', SEUtility.writeMultiSelectPicklist(new Set<String>{'Test1','Test2','Test3'}));
    }

    @isTest static void testValidProfileNameDefinitions(){
        Set<String> profileIdNameSet = new Set<String>();
        for(Profile p:[SELECT Id, Name FROM Profile]) {
            profileIdNameSet.add(p.Name);
        }
        System.assert(profileIdNameSet.contains(SEUtility.PROFILE_SYS_ADMIN), 'Could not find a profile labelled ' + SEUtility.PROFILE_SYS_ADMIN);
        System.assert(profileIdNameSet.contains(SEUtility.PROFILE_TELIA_ADMIN), 'Could not find a profile labelled ' + SEUtility.PROFILE_TELIA_ADMIN);
        System.assert(profileIdNameSet.contains(SEUtility.PROFILE_LARGE), 'Could not find a profile labelled ' + SEUtility.PROFILE_LARGE);
        System.assert(profileIdNameSet.contains(SEUtility.PROFILE_SME), 'Could not find a profile labelled ' + SEUtility.PROFILE_SME);
        System.assert(profileIdNameSet.contains(SEUtility.PROFILE_CYGATE_SALES), 'Could not find a profile labelled ' + SEUtility.PROFILE_CYGATE_SALES);
        System.assert(profileIdNameSet.contains(SEUtility.PROFILE_GENERAL), 'Could not find a profile labelled ' + SEUtility.PROFILE_GENERAL);
    }
    ////
   @isTest static void testIsSystemAdmin(){
        Id SysAdminProfileId = [select id from profile where Name=:SEUtility.PROFILE_SYS_ADMIN limit 1].id;
        SEUtility.isSystemAdmin(SysAdminProfileId);
        User SysAdminUser = [Select id,ProfileId from user where ProfileId=:SysAdminProfileId limit 1];
        SEUtility.isSystemAdmin(SysAdminUser);
		SEUtility.isEnterpriseBusinessAdminUser(SysAdminUser);
        Id LeadAgentProfileId = [select id from profile where Name=:SEUtility.PROFILE_LEADS_AGENT limit 1].id;
        SEUtility.isLeadsAgent(LeadAgentProfileId);
        User LeadAgentUser = [Select id,ProfileId from user where ProfileId=:LeadAgentProfileId limit 1];
        SEUtility.isLeadsAgent(LeadAgentUser);
        Id CygateProfileId = [select id from profile where Name=:SEUtility.PROFILE_CYGATE_SALES limit 1].id;
        User CygateUserUser = [Select id,ProfileId from user where ProfileId=:CygateProfileId limit 1];
        SEUtility.isCygateUser(CygateUserUser);
        Id TeliaProfileId = [select id from profile where Name=:SEUtility.PROFILE_TELIA_ADMIN limit 1].id;
        User TeliaProfileIdUser = [Select id,ProfileId,UserRoleId from user where ProfileId=:TeliaProfileId limit 1];
        SEUtility.isCygateUser(TeliaProfileIdUser);
        SEUtility.isTAMUserRole(TeliaProfileIdUser.UserRoleId);
        SEUtility.updateUserRoleFuture(TeliaProfileIdUser.id,TeliaProfileIdUser.UserRoleId);
        SEUtility.deactivateUserFuture(TeliaProfileIdUser.id);
        SEUtility.getLargeOppRectypeIds();
        SEUtility.getSolutionAreaFromRole('Services');
		//SEUtility.GetAvailableRecordTypeNamesForSObject opp;
        String subject='test mail'; Exception e; List<Id> ids=new list<Id>{CygateUserUser.id};
        SEUtility.sendExceptionMail(subject,e,ids);
        Map<Id,DateTime> accIdtoDateMap = new Map<Id,DateTime>();
        System.runAs(Test_DataFactory.getByPassUser()){  // V.A  Bypassing User
        Account acc = Test_DataFactory.createOneAccount();
        acc.Contacted__c = system.today()-1;
        insert acc;
		accIdtoDateMap.put(acc.id,system.today());
        SEUtility.setAccountContactedDate(accIdtoDateMap);
        }
    }
}