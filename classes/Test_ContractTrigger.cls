@IsTest
public class Test_ContractTrigger {
    
    // This test verifies the basic functionality of the Contract Trigger
    public static testmethod void no01_createandUpdateContract(){
        Test_DataFactory.setUpOrg();
        Account acc;
        System.runAs(new User(Id = Userinfo.getUserId())){
            acc = Test_DataFactory.createOneLargeStandardAccount();
            insert acc;
            system.debug('@@@'+acc);
            
            Contract conObj = new Contract();
            conObj.StartDate = system.today();
            conObj.status = 'Draft';
            conObj.accountId = acc.Id;
            insert conObj;
            update conObj;
        }
    }
}