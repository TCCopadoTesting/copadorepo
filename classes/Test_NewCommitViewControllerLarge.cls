//***********************Updated Version****************************************************************************************************************
// Updated by S.S - Added test method 'TemporarySalesManagerCommitView' to cover the code coverage for temporary sales manager commit view (Case 1364)
//******************************************************************************************************************************************************

@isTest
public class Test_NewCommitViewControllerLarge {
    
    @testSetup
    static void initTestData() {
        Test_DataFactory.setUpOrg();
    }
    
    @isTest
    static void testCommit(){
        List<User> largeUserTeam;
        List<Seller__c> largeSellerTeam;
        List<Yearly_Target__c> largeSellerteamYT;
            
        System.runAs(new User(Id = Userinfo.getUserId())){
            // returns list of large users 
            largeUserTeam = Test_DataFactory.createLargeUserTeam(5);
            // returns list of sellers 
            largeSellerTeam = Test_DataFactory.createLargeSellerTeam(largeUserTeam,Date.parse('2015-01-01'));
            // returns list of yearly targets 
            largeSellerteamYT = Test_DataFactory.createYearlyTargetsSellerTeam(largeSellerTeam);
        }

        User salesRep = largeUserTeam[4];
        Id sellerId;
        for(Seller__c s : largeSellerTeam){
            if(s.User__c == salesRep.Id){
                sellerId = s.Id;
            }
        }
        System.runAs(salesRep){  
            NewCommitViewControllerLarge controller = new NewCommitViewControllerLarge();           
            controller.saveJudge();
            System.assertEquals('Commit Saved', controller.status, 'Commit was not saved');
       }
    }
    
    @isTest
    static void testCommitAsManager(){
        List<User> largeUserTeam;
        List<Seller__c> largeSellerTeam;
        List<Yearly_Target__c> largeSellerteamYT;
        List<SelectOption> pageLoadOptions;
        List<SelectOption> secMangerPageLoadOptions;

        System.runAs(new User(Id = Userinfo.getUserId())){
            // returns list of large users 
            largeUserTeam = Test_DataFactory.createLargeUserTeam(5);
            // returns list of sellers 
            largeSellerTeam = Test_DataFactory.createLargeSellerTeam(largeUserTeam,Date.parse('2015-01-01'));
            // returns list of yearly targets 
            largeSellerteamYT = Test_DataFactory.createYearlyTargetsSellerTeam(largeSellerTeam);
            

        }

        User manager = largeUserTeam[3];
        Id sellerId;
        for(Seller__c s : largeSellerTeam){
            if(s.User__c == manager.Id){
                sellerId = s.Id;
            }
        }
        System.runAs(manager){  
            NewCommitViewControllerLarge controller = new NewCommitViewControllerLarge();
            controller.saveJudge();
            System.assertEquals('Commit Saved', controller.status, 'Commit was not saved');
       }
    }
    
    @isTest
    static void testTemporarySalesManagerCommitView(){
    
        Integer thisYear = System.today().year();
        Date startDate = SEUtility.getFirstDayOfTeliaYear(thisYear);
        
        List<User> largeuserTeam;
        List<Seller__c> largesellerteam;
        Id DirectorId,ManagerId,SellerId,largeSellerId;
        Id manageruserid;
        List<Id> listofseller = new List<Id>();

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            // returns list of users {SalesUnitManager, SalesTeamManager, Seller}
            largeuserTeam = Test_DataFactory.createLargeUserTeam();
            // returns list of sellers {SalesUnitManager, SalesTeamManager, Seller}
            largesellerteam = Test_DataFactory.createLargeSellerTeam(largeuserTeam,startdate);
        }
        
        for(Seller__c s: largesellerteam )
        {
            if(s.Type__c == 'Sales Unit Director')
            {
                DirectorId = s.Id;
            }
            else if(s.Type__c == 'Sales Team Manager')
            {
                ManagerId = s.Id;
                manageruserid = s.User__c;
            }
            else if(s.Type__c == 'Seller')
            {
                SellerId = s.Id;
            }
        }
        
        Seller__c updateManagerSalesTeam = new Seller__c();
        updateManagerSalesTeam.Manage_Other_Team__c = 'Large Enterprise North';
        updateManagerSalesTeam.id = DirectorId;
        Update updateManagerSalesTeam ;
        
        Seller__c temporaryManager = new Seller__c();
        temporaryManager.Manager__c = DirectorId;
        temporaryManager.Type__c = 'Sale Team Manager';
        temporaryManager.Start_Date__c = startDate;
        temporaryManager.User__c = manageruserid;
        insert temporaryManager;
        
        Seller__c sellerastemp = new Seller__c();
        sellerastemp.Manager__c = temporaryManager.id;
        sellerastemp.Type__c = 'Seller';
        sellerastemp.User__c = Userinfo.getUserId();
        sellerastemp.Start_Date__c = startDate;
        insert sellerastemp;
        
        Seller__c temporaryManager1 = new Seller__c();
        temporaryManager1.Temporary_Manager__c = sellerastemp.Id;
        temporaryManager1.Has_Been_Replaced__c = true;
        temporaryManager1.id = temporaryManager.id;
        update temporaryManager1;
        
       
        System.runAs(new User(Id = Userinfo.getUserId())){  
            NewCommitViewControllerLarge controller = new NewCommitViewControllerLarge();
            PageReference commitSaved = controller.saveJudge();
            System.assertEquals('Commit Saved', controller.status, 'Commit was not saved');
        }
    }
}