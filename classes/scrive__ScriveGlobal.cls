/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScriveGlobal {
    global ScriveGlobal() {

    }
    global static scrive.ScriveGlobal.ScriveDocumentResult forward(Id scriveDocumentId, String emailAddress) {
        return null;
    }
    global static scrive.ScriveGlobal.ScriveDocumentResult remove(Id scriveDocumentId) {
        return null;
    }
    global static scrive.ScriveGlobal.ScriveDocumentResult restart(Id scriveDocumentId) {
        return null;
    }
    global static scrive.ScriveGlobal.ScriveDocumentResult sendReminder(Id scriveDocumentId) {
        return null;
    }
    global static scrive.ScriveGlobal.ScriveDocumentResult withdraw(Id scriveDocumentId) {
        return null;
    }
global class ScriveDocumentResult {
    global String errorMessage {
        get;
    }
    global Boolean isSuccess {
        get;
    }
}
}
