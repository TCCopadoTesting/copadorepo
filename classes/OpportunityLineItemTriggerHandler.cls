/**
    About
    -----
    Description: OpportunityLineItemTriggerHandler on OpportunityLineItem
    Create date: 22.09-2016

    Update History
    --------------
    Created Sep 2016 - A.N Merged functionality from OpportunityProductAfter into trigger handler. (OpportunityProductAfter.trigger has been renamed to OpportunityLineItem.trigger)

    Issues / TODOs
    --------------  
    
*/



public class OpportunityLineItemTriggerHandler {
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /* * OpportunityLineItemTriggerHandler Trigger Calls * */
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * */

    private Boolean m_isExecuting = false;
    private Integer batchSize = 0;

    public OpportunityLineItemTriggerHandler(Boolean isExecuting, Integer size){
        m_isExecuting = isExecuting;
        batchSize = size;
    }
    
    public void OnBeforeInsert(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> newMap){
       System.debug('OpportunityLineItemTriggerHandler.OnBeforeInsert starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + batchSize);
       setBuyInterest(newList);
    }

    public void OnAfterInsert(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> newMap){
        System.debug('OpportunityLineItemTriggerHandler.OnAfterInsert starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + batchSize);
        updateSalesOutcome(newList, null);
        updateSolutionSalesOutcomeRecords(newList, null);
    }
    
   public void OnBeforeUpdate(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> newMap){
       System.debug('OpportunityLineItemTriggerHandler.OnBeforeUpdate starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + batchSize);
       setBuyInterest(newList);
    }

    public void OnAfterUpdate(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> newMap, List<OpportunityLineItem> oldList, Map<Id, OpportunityLineItem> oldMap){
        System.debug('OpportunityLineItemTriggerHandler.OnAfterUpdate starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + batchSize);
        updateSalesOutcome(newList, oldMap);
        updateSolutionSalesOutcomeRecords(newList, oldMap);
    }

    public void OnAfterDelete(List<OpportunityLineItem> oldList, Map<Id, OpportunityLineItem> oldMap){
        System.debug('OpportunityLineItemTriggerHandler.OnAfterDelete starts. Number of SOQLs used so far ' +Limits.getQueries() + ' queries. Number of records = ' + batchSize);
        updateSalesOutcome(oldList, null);
        updateSolutionSalesOutcomeRecords(oldList, null);
        CygateProductDelete(oldList);
    }  

    // Author A.N merged filters from old OpportunityProductAfter.
    // - Added to filters so that opps are only updated if the Sales_Outcome__c value has changed.
    // - Removed check Sales_Outcome__c != null. As this is a formula field the value will never be null.
    private static void updateSalesOutcome(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> oldMap){
        Set<Id> oppIds = new Set<Id>();

        // If insert, oldMap is null and all opp Ids will be added to set. If delete/undelete input for oldMap is set to null as this should also add all opp Ids to set. (Note: on delete the "oldList" is sent as input "newList" to the method as the newList doesn't exist on delete)
        // If update, the opp is only added to set if Sales_Outcome__c or Sales_Outcome_Large__c is changed
        for(OpportunityLineItem oli : newList){
            if(oldMap == null || oli.Sales_Outcome__c != oldMap.get(oli.Id).Sales_Outcome__c || oli.Sales_Outcome_Large__c != oldMap.get(oli.Id).Sales_Outcome_Large__c){
                oppIds.add(oli.OpportunityId);
            }
        }

        if(oppIds.isEmpty()) return;

        List<Opportunity> oppList = [SELECT Id, Sales_Outcome__c, Sales_Outcome_Large__c, 
                                    (SELECT Sales_Outcome__c, Sales_Outcome_Large__c FROM OpportunityLineItems)
                                    FROM Opportunity WHERE Id in : oppIds];
                
        for(Opportunity opp : oppList){
            opp.Sales_Outcome__c = 0;
            opp.Sales_Outcome_Large__c = 0;

            if(!opp.OpportunityLineItems.isEmpty()){
                for(OpportunityLineItem oli : opp.OpportunityLineItems){
                    opp.Sales_Outcome__c += oli.Sales_Outcome__c;
                    opp.Sales_Outcome_Large__c += oli.Sales_Outcome_Large__c;
                }
            } else {
                OpportunityTriggerHandler.addOppWithLastProductDeleted(opp.Id);
            }
        }
        try
        {
            update oppList;
        }
        catch(Exception e)
        {
            system.debug(''+e.getmessage());
        }
        
    }
    // Author A.N merged filters from old OpportunityProductAfter.
    // - Added to filters so that opps are only updated if the Sales_Outcome_Solution__c value has changed.
    private static void updateSolutionSalesOutcomeRecords(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> oldMap){
        Set<Id> oppIds = new Set<Id>();

        // If insert, oldMap is null and all opp Ids will be added to set. If delete/undelete input for oldMap is set to null as this should also add all opp Ids to set. (Note: on delete the "oldList" is sent as input "newList" to the method as the newList doesn't exist on delete)
        for(OpportunityLineItem oli : newList){
            System.debug('oli.Sales_Outcome_Solution__c ' + oli.Sales_Outcome_Solution__c);
            System.debug('oldMap.get(oli.Id).Sales_Outcome_Solution__c' + (oldMap == null ? 0.0 : oldMap.get(oli.Id).Sales_Outcome_Solution__c));
            if(oldMap == null || oli.Sales_Outcome_Solution__c != oldMap.get(oli.Id).Sales_Outcome_Solution__c){
                oppIds.add(oli.OpportunityId);
            }
        }

        if(oppIds.isEmpty()) return;

        // Only Won opportunities needs to have the Sales Outcome Solution-records updated (recreated)
        List<Opportunity> closedWonOpps = [SELECT Id, CloseDate, OwnerId, AccountId FROM Opportunity WHERE Id in : oppIds AND IsWon = true];
        if(!closedWonOpps.isEmpty()){
            SolutionsSalesOutcomeServiceClass.deleteExistingRecords(closedWonOpps);
            SolutionsSalesOutcomeServiceClass.createNewRecords(closedWonOpps);
        }
    }
    
    // Author Vahraz
    // Sets Buy Interest if there is only one AF Buy Interest on the 
    private static void setBuyInterest(List<OpportunityLineItem> newList){
        
        //OpportunityId, OpportunityLineItem
        Map<Id, List<OpportunityLineItem>> checkedOlis = new Map<Id, List<OpportunityLineItem>>();
        
        for(OpportunityLineItem oli : newList){
            if(oli.From_reseller_lead__c == True){
                if(checkedOlis.get(oli.OpportunityId) == null){
                    List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
                    oliList.add(oli);
                    checkedOlis.put(oli.OpportunityId, oliList);
                }else{
                    checkedOlis.get(oli.OpportunityId).add(oli);
                }  
            }
        }
        
        if(!checkedOlis.isEmpty()){
             Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([SELECT Id FROM Opportunity WHERE Id IN :checkedOlis.keySet()]);

            //OpportunityId, Buy_Interest__c
            Map<Id, Buy_Interest__c> buyInterests = new Map<Id, Buy_Interest__c>();
            
            for(Buy_Interest__c bi : [SELECT Id, Opportunity__c FROM Buy_Interest__c WHERE Lead_Source__c = :LeadTriggerHandler.LEAD_SOURCE_RESELLER AND Opportunity__c IN :opportunities.keySet()]){
                if(buyInterests.get(bi.Opportunity__c) == null){
                    buyInterests.put(bi.Opportunity__c, bi);
                    for(OpportunityLineItem oli :checkedOlis.get(bi.Opportunity__c)){
                        oli.Buy_Interest__c = bi.Id;
                    }

                }else {
                    for(OpportunityLineItem oli :checkedOlis.get(bi.Opportunity__c)){
                        oli.addError(Label.LP_Opp_Many_BI_Error);
                    }
                }
            }         
        }
    }
    
    
private static void CygateProductDelete(List<OpportunityLineItem> oldlist){
    Set<Id> oppIds = new Set<Id>();
    Integer currentSubProductCount=0;
    Integer currentmainProductCount=0;
    Integer oldmainProductCount=0;
        for(OpportunityLineItem oli : oldList){
            if(oli.Product_Family__c.contains('Cygate')){   
                  
                       if(oli.By_Pass_Cygate_Sub_Product__c==false){
                           oppIds.add(oli.OpportunityId);
                           //oldmainProductCount+=1;
                       }
              }
         }
         List<Opportunity> oppList = new List<Opportunity>();
         if(!oppIds.isEmpty()){
            oppList = [SELECT Id,
                                    (SELECT By_Pass_Cygate_Sub_Product__c,Product_Family__c FROM OpportunityLineItems where By_Pass_Cygate_Sub_Product__c=false)
                                    FROM Opportunity WHERE Id in : oppIds];
             }                                   

 for(Opportunity opp : oppList){ 
    for (OpportunityLineItem oli : opp.OpportunityLineItems){           
            /*if(oli.By_Pass_Cygate_Sub_Product__c==true && oli.Product_Family__c.contains('Cygate'))
                    {
                    currentSubProductCount+=1;                     
                    }*/
            if(oli.By_Pass_Cygate_Sub_Product__c==false && oli.Product_Family__c.contains('Cygate'))
                    {
                    currentmainProductCount+=1;                    
                    }
                    
            }
            if((currentmainProductCount==0) || opp.OpportunityLineItems.isEmpty())
            {
                        OpportunityTriggerHandler.addOppWithLastCygateProductDeleted(opp.Id);
            }         
            }      
   
}
}