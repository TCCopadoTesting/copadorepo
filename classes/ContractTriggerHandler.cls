public class ContractTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public ContractTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void OnbeforeInsert(List<Contract> newList,Map<Id,Contract>newMap){
        For (Contract conObj : newList){
            if(String.isBlank(conObj.Contract_Record_Type__c) && String.isNotBlank(conObj.recordtypeId)){
                system.debug('Inside:-'+conObj.recordtypeId);
                system.debug('###'+Schema.getGlobalDescribe().get('Contract').getDescribe().getRecordTypeInfosById().get(conObj.recordtypeId).getName());
                conObj.Contract_Record_Type__c = Schema.getGlobalDescribe().get('Contract').getDescribe().getRecordTypeInfosById().get(conObj.recordtypeId).getName();
            }
        }
    }
    
    public void OnbeforeUpdate(List<Contract> newList){
        updatecontractrecType(newList);
    }
    
    
    public void updatecontractrecType (List<Contract>newList){
        for(Contract con: newList){
            if(con.Contract_Record_Type__c == 'Cygate Konkurrentavtal'){
                con.RecordTypeId = SEUtility.getRecordTypeId('Cygate_Competitor_Agreements');
            }else if(con.Contract_Record_Type__c == 'Cygateavtal'){
                con.RecordTypeId = SEUtility.getRecordTypeId('Cygate_Agreement');
            }else if(con.Contract_Record_Type__c == 'Telia Konkurrentavtal'){
                con.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.Contract_TELIA);
            }else if(con.Contract_Record_Type__c == 'Teliaavtal'){
                con.RecordTypeId = SEUtility.getRecordTypeId(SEUtility.Contract_TELIA_Avtal);
            }
        }
    }
}