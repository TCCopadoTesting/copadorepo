/* Author A.N. March 2016
* NOTE: This test class uses the @IsTest(SeeAllData=true), as this is required for the Connect API used by the EventTriggerHandler.
* In general, the @IsTest(SeeAllData=true) should never by used, and required data should be created in the test method.
*/
@IsTest(SeeAllData=true)
public with sharing class Test_EventTriggerHandlerChatterPosts {
    
    @isTest
    static void testPostChatterUpdateEventNoInvitees(){
        
        User cygateUser = Test_DataFactory.createCygateUser();
        Account acc;
        Event e;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            insert cygateUser;
            acc = Test_DataFactory.createOneCygateAccount(cygateUser.Id);
            insert acc;
        }

        System.runAs(cygateUser){           
            test.starttest();
            Contact c = Test_DataFactory.createOneContact();
            c.AccountId = acc.Id;
            insert c;

            e = Test_DataFactory.createCygateKundbesokEvent();
            e.WhoId = c.Id;
            e.WhatId = acc.Id;
            e.Bredda_aff_ren__c=true;
            e.ka_avtalad_aff_r__c=true;
            e.V_xa_befintlig_aff_r__c=true;
            e.Customer_status__c='Ny kund';
            e.ActivityDateTime = System.now() + 1; // start time is one day from now -> event is open.          
            insert e;
            test.stoptest();
        }

        //Assert
        Integer noOfChatterPosts = [SELECT COUNT() FROM FeedItem WHERE ParentId =: cygateUser.Id];       
        //System.assertEquals(noOfChatterPosts, 0, 'No chatter posts should have be created as the event is still open');

        System.runAs(cygateUser) {
            e.ActivityDateTime = System.now() - 1; // start time is one day before now -> event is closed.
            update e;
        }

        //Assert
        Integer noOfChatterPostsAfter = [SELECT COUNT() FROM FeedItem WHERE ParentId =: cygateUser.Id];
        //System.assertEquals(noOfChatterPostsAfter, 1, 'A chatter posts should have be created as the event is closed');
    }

    @isTest
    static void testPostChatterUpdateEventWithInvitees(){
        /*
        List<User> userList = new List<User>();
        User cygateUser;
        Account acc;
        Event e;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            userList.add(Test_DataFactory.createCygateUser());
            userList.add(Test_DataFactory.createCygateUser());
            userList.add(Test_DataFactory.createCygateUser());
            insert userList;
            cygateUser = userList[0];
            acc = Test_DataFactory.createOneCygateAccount(cygateUser.Id);
            insert acc;
        }

        System.runAs(cygateUser) {        
            Contact c = Test_DataFactory.createOneContact();
            c.AccountId = acc.Id;
            insert c;

            e = Test_DataFactory.createCygateKundbesokEvent();
            e.WhoId = c.Id;
            e.WhatId = acc.Id;
            e.ActivityDateTime = System.now() + 1; // start time is one day from now -> event is open.
            insert e;
            List<EventRelation> erList = new List<EventRelation>();
            erList.add(Test_DataFactory.createEventRelation(e.Id, userList[1].Id));
            erList.add(Test_DataFactory.createEventRelation(e.Id, userList[2].Id));
            insert erList;
        }

        //Assert
        Integer noOfChatterPosts = [SELECT COUNT() FROM FeedItem WHERE ParentId =: cygateUser.Id];
        System.assertEquals(noOfChatterPosts, 0, 'No chatter posts should have be created as the event is still open');

        System.runAs(cygateUser) {
            e.ActivityDateTime = System.now() - 1; // start time is one day before now -> event is closed.
            update e;
        }

        //Assert
        FeedItem chatterPost = [SELECT Id, Body FROM FeedItem WHERE ParentId =: cygateUser.Id LIMIT 1];
        String chatterText = chatterPost.body;
        System.assert(chatterText.contains(userList[0].LastName), 'The event assigned to should be referenced in the chatter post');
        System.assert(chatterText.contains(userList[1].LastName), 'The event invitee no 1 to should be referenced in the chatter post');
        System.assert(chatterText.contains(userList[2].LastName), 'The event invitee no 2 to should be referenced in the chatter post');
        */
    }

    @isTest
    static void testPostChatterUpdateEventRelatedToOpp(){
        
        User cygateUser = Test_DataFactory.createCygateUser();
        Account acc;
        Event e;

        System.runAs(new User(Id = Userinfo.getUserId())){ // Avoids MIXED_DML_OPERATION error (when test executes in the Salesforce UI).
            insert cygateUser;
            acc = Test_DataFactory.createOneCygateAccount(cygateUser.Id);
            insert acc;
        }

        System.runAs(cygateUser) {
            test.starttest();
            Contact c = Test_DataFactory.createOneContact();
            c.AccountId = acc.Id;
            insert c;

            e = Test_DataFactory.createCygateKundbesokEvent();
            e.WhoId = c.Id;
            e.WhatId = acc.Id;
            e.Bredda_aff_ren__c=true;
            e.ka_avtalad_aff_r__c=true;
            e.V_xa_befintlig_aff_r__c=true;
            e.Customer_status__c='Ny kund';
            e.ActivityDateTime = System.now() + 1; // start time is one day from now -> event is open.
            insert e;
            test.stoptest();
        }

        //Assert
        Integer noOfChatterPosts = [SELECT COUNT() FROM FeedItem WHERE ParentId =: cygateUser.Id];
        //System.assertEquals(noOfChatterPosts, 0, 'No chatter posts should have be created as the event is still open');

        System.runAs(cygateUser) {
            e.ActivityDateTime = System.now() - 1; // start time is one day before now -> event is closed.
            update e;
        }

        //Assert
        Integer noOfChatterPostsAfter = [SELECT COUNT() FROM FeedItem WHERE ParentId =: cygateUser.Id];
        //System.assertEquals(noOfChatterPostsAfter, 1, 'A chatter posts should have be created as the event is closed');
    }
     @isTest static void testUpdateContactActivitiesForEvents(){ //Author - P.P
        User uR1 = new User();
        User uL1 = new user();
        user uC1 = new User();
        user uSM1 = new User();
        user uM1 = new User();
        string roleuR1 = '';
        string roleuL1 = '';
        string roleuC1 = '';
        string roleuSM1 = '';
        string roleuM1 = '';
        Account acc1 = new Account();
        Account acc2 = new Account();
        Account acc3 = new Account();
        Account acc4= new Account();
        List<Account> accList = new List<Account>();
        Campaign camp = new Campaign();
        Contact conL = new Contact();
        Contact conC = new Contact();
        Contact conR = new Contact();
        Contact conSM = new Contact();
        Contact conM = new Contact();
        List<Id> conIdList = new List<Id>();
        List<Contact> contactList = new List<Contact>();
        event eL = new event();
        
        System.runAs(Test_DataFactory.getByPassUser()){// to avoid MIXED_DML operation
            List<User> renewalUserList = Test_DataFactory.createLargeRenewalUserTeam(1);
            uR1 = renewalUserList[0];
            roleuR1 = uR1.UserRoleId;
            List<user> largeUserList = Test_DataFactory.createLargeUserTeam(2);
            uL1 = largeUserList[0];
            roleuL1 = uL1.UserRoleId;
            uC1 = Test_DataFactory.createCygateUser();
            insert uC1;
            roleuC1 = uC1.userRoleId;
            List<User> uSMList = Test_DataFactory.createSMUserTeam(1);
            uSM1 = uSMList[0];
            roleuSM1 = uSM1.userRoleId;
            List<User> uMList = Test_DataFactory.createMarketingUserTeam(1);
            uM1 = uMList[0];
            roleuM1 = uM1.userRoleId;
        }
        
        acc1 = Test_DataFactory.createOneLargeStandardAccount();
        acc1.OwnerId = uL1.Id;
        acc1.Cygate_Account_Manager__c = uC1.Id;
        accList.add(acc1);
        acc2 = Test_DataFactory.createOneLargeStandardAccount();
        acc2.OwnerId = uR1.Id;
        accList.add(acc2);
        acc3 = Test_DataFactory.createOneLargeStandardAccount();
        acc3.OwnerId = uSM1.Id;
        accList.add(acc3);
        acc4 = Test_DataFactory.createOneLargeStandardAccount();
        acc4.OwnerId = uM1.Id;
        accList.add(acc4);
        insert accList;
        acc1 = accList[0];
        acc2 = accList[1];
        acc3 = accList[2];
        acc4 = accList[3];

        camp = Test_DataFactory.createCygateCampaign();
        insert camp;
        
        conL = new Contact(LastName='TestLastName',AccountId = acc1.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testl@dummy.com',Role__c = 'Annan');
        contactList.add(conL);
        conC = new Contact(LastName='TestLastName',AccountId = acc1.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testC@dummy.com',Role__c = 'Annan');
        
        contactList.add(conC);
        conR = new Contact(LastName='TestLastName',AccountId = acc2.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testR@dummy.com',Role__c = 'Annan');
        contactList.add(conR);
        conM = new Contact(LastName='TestLastName',AccountId = acc4.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testM@dummy.com',Role__c = 'Annan');
        contactList.add(conM);
        conSM = new Contact(LastName='TestLastName',AccountId = acc3.Id,FirstName = 'TestFirstName',Phone = '+460345678',Email = 'testSM@dummy.com',Role__c = 'Annan');
        contactList.add(conSM);
        insert contactList;
        conL = contactList[0];
        conIdList.add(conL.Id);
        conC = contactList[1];
        conIdList.add(conC.Id);
        conR = contactList[2];
        conIdList.add(conR.Id);
        conM = contactList[3];
        conIdList.add(conM.Id);
        conSM = contactList[4];
        conIdList.add(conSM.Id);
        List<Event> eventList = Test_DataFactory.createEvents(6);           
        List<Task> TaskList = Test_DataFactory.createTasks(2);
        
        system.runAs(uL1){
            eL = eventList[0];
            eL.OwnerId = uL1.Id;
            eL.WhoId = conL.Id;
            insert eL;
            Task tL = taskList[0];
            tL.WhoId = conL.Id;
            tL.OwnerId = uL1.Id;
            insert tL;
        }
                  
        system.runAs(uC1){
            event eC = eventList[1];
            eC.OwnerId = uC1.Id;
            eC.WhoId = conC.Id;
            eC.Campaign__c = camp.Id;
            eC.Bredda_aff_ren__c=true;
            eC.ka_avtalad_aff_r__c=true;
            eC.V_xa_befintlig_aff_r__c=true;
            eC.Customer_status__c='Ny kund';
            insert eC;
            
            eC.WhoId = conL.Id;
            update eC;
            
            eC.WhoId = NULL;
            update eC;
        }
        test.starttest();
        system.runAs(uL1){
            eL.WhoId = conC.Id;
            update eL;
            
            eL.WhoId = NULL;
            update eL;
            eL.OwnerId = uC1.Id;
            update eL;
        }   
        
        system.runAs(uR1){
            event eR = eventList[2];
            eR.OwnerId = uR1.Id;
            eR.WhoId = conR.Id;
            insert eR;
            Event eR1 = eventList[3];
            eR1.OwnerId = uR1.Id;
            insert eR1;
            
            eR1.WhoId = conR.Id;
            update eR1; 
            
            List<Event> eRList = new List<Event>();
            eR.WhoId = NULL;
            eR1.WhoId = NULL;
            eRList.add(eR);
            eRList.add(eR1);
            update eRList;
        }
        
        system.runAs(uM1){
            event eM = eventList[4];
            eM.OwnerId = uM1.Id;
            eM.WhoId = conM.Id;
            insert eM;
            
            delete eM;
        }
        
        system.runAs(uSM1){
            event eSM = eventList[5];
            eSM.OwnerId = uSM1.Id;
            eSM.WhoId = conSM.Id;
            eSM.Meetingforum__c ='Strategisk';
            eSM.Meeting_Type__c ='Ordinarie';
            insert eSM;   
                     
            eSM.OwnerId = uM1.Id;
            update eSM;
            
            eSM.WhoId = NULL;
            update eSM;
        }
        List<Contact> conSAList = [Select Id, Telia_Activities__c,Cygate_Activities__c,Other_Activities__c,Service_Management_Activities__c,Marketing_Activities__c FROM COntact WHERE Id IN: conIdList];
        for(contact con:conSAList){
            if(conL.Id == con.Id){
                system.assertequals(con.Telia_Activities__c,TRUE);
                system.assertequals(con.Cygate_Activities__c,FALSE);
            }
            if(conC.Id == con.Id){
                system.assertequals(con.Cygate_Activities__c,False);
            }
            if(conR.Id == con.Id){
                system.assertequals(con.Other_Activities__c,False);
            }
            if(conM.Id == con.Id){
                system.assertequals(con.Marketing_Activities__c,False);
            }
        }
        test.stoptest();       
    }
}