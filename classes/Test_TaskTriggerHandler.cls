@isTest
public with sharing class Test_TaskTriggerHandler {
    
    @testSetup
    static void initTestData() {
        Test_DataFactory.setUpOrg();
    }
    
    @isTest static void Test_PR_createFollowUpTask() {
        System.debug('@@Test test_PR_createFollowUpTask Starts');
        User prUser;
        Account a;
        
        System.runAs(Test_DataFactory.getByPassUser()){  // V.A  Bypassing User
            prUser = Test_DataFactory.createPRUser();
            insert prUser;
            a = Test_DataFactory.createOneAccount();
            a.OwnerId = prUser.Id;
            insert a;
        }
        
        System.runAs(prUser){
            List<Contact> conList = Test_DataFactory.createContacts(1);
            conList[0].AccountId = a.Id;
            
            insert conList;
            
            Campaign camp = Test_DataFactory.createTeliaCampaign();
            insert camp;
            
            Call_Activity__c ca = Test_DataFactory.createCallActivity(camp.Id, a.Id);
            insert ca; 
            
            List<Task> tasks = Test_DataFactory.createTasks(2);
            
            Task t1 = tasks[0];
            t1.WhoId = conList[0].id;
            t1.OwnerId = prUser.Id;
            t1.Call_Activity__c = ca.Id;
            t1.Call_Status__c = TaskTriggerHandler.STATUS_CALL_BACK;
            t1.Call_Back_Date__c = Date.today();
            t1.Type = TaskTriggerHandler.TYPE_PHONE;
            t1.Status = TaskTriggerHandler.STATUS_CLOSED;            
            
            Database.SaveResult sr1 = Database.insert(t1);
            system.assertEquals(sr1.isSuccess(), true);
            
            List<Task> followUpTask = new List<Task>([SELECT ActivityDate, Status, Call_Status__c FROM Task WHERE Status = :TaskTriggerHandler.STATUS_OPEN]);
            system.assertEquals(followUpTask.size(), 1);
            system.assert(followUpTask[0] != null);
            system.assertEquals(followUpTask[0].ActivityDate, Date.today());
            system.assertEquals(followUpTask[0].Call_Status__c, TaskTriggerHandler.STATUS_CONTACTED);
        }
    }
    
    @isTest static void Test_PR_setPrNotInterested() {
        System.debug('@@Test Test_PR_setPrNotInterested Starts');
        User prUser;
        Account a;
        
        System.runAs(Test_DataFactory.getByPassUser()){  // V.A  Bypassing User
            prUser = Test_DataFactory.createPRUser();
            insert prUser;
            a = Test_DataFactory.createOneAccount();
            a.OwnerId = prUser.Id;
            insert a;
        }
        
        System.runAs(prUser){
            List<Contact> conList = Test_DataFactory.createContacts(1);
            conList[0].AccountId = a.Id;
            insert conList;
            
            Campaign camp = Test_DataFactory.createTeliaCampaign();
            insert camp;
            
            Call_Activity__c ca = Test_DataFactory.createCallActivity(camp.Id, a.Id);
            insert ca; 
            
            List<Task> tasks = Test_DataFactory.createTasks(2);
            
            Task t1 = tasks[0];
            t1.WhoId = conList[0].id;
            t1.OwnerId = prUser.Id;
            t1.Call_Activity__c = ca.Id;
            t1.PR_Not_Interested__c = true;
            t1.Type = TaskTriggerHandler.TYPE_PHONE;
            t1.Status = TaskTriggerHandler.STATUS_CLOSED;            
            
            Database.SaveResult sr1 = Database.insert(t1);
            system.assertEquals(sr1.isSuccess(), true);
            
            List<Call_Activity__c> callActivity = new List<Call_Activity__c>([SELECT Id, Is_PR_Not_Interested__c FROM Call_Activity__c]);
            system.assert(callActivity[0] != null);
            system.assertEquals(callActivity.size(), 1);
            system.assertEquals(callActivity[0].Is_PR_Not_Interested__c, True);
        }
    }
    
    @isTest static void Test_PR_addContactToCampaign() {
        System.debug('@@Test Test_PR_addContactToCampaign Starts');
        User prUser;
        Account a;
        System.runAs(Test_DataFactory.getByPassUser()){  // V.A  Bypassing User
            prUser = Test_DataFactory.createPRUser();
            insert prUser;
            a = Test_DataFactory.createOneAccount();
            a.OwnerId = prUser.Id;
            insert a;
        }
        
        System.runAs(prUser){
            
            List<Contact> conList = Test_DataFactory.createContacts(1);
            conList[0].AccountId = a.Id;
            insert conList;
            
            Campaign camp = Test_DataFactory.createTeliaCampaign();
            insert camp;
            
            Call_Activity__c ca = Test_DataFactory.createCallActivity(camp.Id, a.Id);
            insert ca; 
            
            List<Task> tasks = Test_DataFactory.createTasks(2);
            
            Task t1 = tasks[0];
            t1.WhoId = conList[0].id;
            t1.OwnerId = prUser.Id;
            t1.Call_Activity__c = ca.Id;
            t1.PR_Not_Interested__c = true;
            t1.Type = TaskTriggerHandler.TYPE_PHONE;
            t1.Status = TaskTriggerHandler.STATUS_CLOSED;   
            t1.PR_Not_Interested__c = false;
            t1.Call_Status__c = TaskTriggerHandler.STATUS_CONTACTED;
            
            
            Database.SaveResult sr1 = Database.insert(t1);
            system.assertEquals(sr1.isSuccess(), true);
            
            List<CampaignMember> campaignMember = new List<CampaignMember>([SELECT Id, ContactId, CampaignId FROM CampaignMember]);
            system.assert(campaignMember[0] != null);
            system.assertEquals(campaignMember.size(), 1);
            system.assertEquals(campaignMember[0].ContactId, conList[0].Id);
            system.assertEquals(campaignMember[0].CampaignId, camp.Id);
        }
    }
}