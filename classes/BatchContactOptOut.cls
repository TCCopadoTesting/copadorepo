/**
About
-----
Description: BatchAccountOptOut for SAEN-1838 - CLM Marketing Opt Out
Create date: 03.3-2017

Update History
--------------
Created Mars 2017 - Vahraz Mostajir - SAEN-1838. 

Issues / TODOs
--------------     
*/

public class BatchContactOptOut implements Database.Batchable<Contact>, Database.Stateful  {
    
    public Map<Id, Account> parentAccounts;
    
    public BatchContactOptOut(){
        System.debug('ContactOptOut INIT');
        parentAccounts = new Map<Id, Account>([SELECT Id, Marketing_Mails_Opt_Out__c, From_Date__c, To_Date__c
                                               FROM Account
                                               WHERE (RecordTypeId = :SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_LARGE) 
                                                      OR RecordTypeId = :SEUtility.getRecordTypeId(SEUtility.ACC_RECTYPE_SME)) 
                                               AND Process_Opt_Out__c = True
                                              ]);
        
        
    }
    
    public List<Contact> start(Database.BatchableContext BC) {   
        return new List<Contact>([SELECT Id, Opt_Out_Manually_Set__c, AccountId,
                                  From_Date__c, Man_From_Date__c,
                                  To_Date__c, Man_To_Date__c,
                                  Marketing_Mails_Opt_Out_Contact__c, Man_Mails_Opt_Out__c
                                  FROM Contact
                                  WHERE AccountId IN :parentAccounts.keySet() 
                                  AND RecordTypeId = :SEUtility.getRecordTypeId(SEUTILITY.CONTACT_RECTYPE_CUSTOMER)
                                 ]);
    }
    
    public void execute(Database.BatchableContext BC, List<Contact> scope){  
        System.debug('ContactOptOut EXECUTE');     
        
        for(Contact con : scope){
            if(String.isEmpty(parentAccounts.get(con.AccountId).Marketing_Mails_Opt_Out__c) && con.Opt_Out_Manually_Set__c){
                con.From_Date__c = con.Man_From_Date__c;
                con.To_Date__c = con.Man_To_Date__c;
                con.Marketing_Mails_Opt_Out_Contact__c = con.Man_Mails_Opt_Out__c;
            }else{
                con.From_Date__c = parentAccounts.get(con.AccountId).From_Date__c;
                con.To_Date__c = parentAccounts.get(con.AccountId).To_Date__c;
                con.Marketing_Mails_Opt_Out_Contact__c = parentAccounts.get(con.AccountId).Marketing_Mails_Opt_Out__c;
            }
        }
        
        update scope;
    }
    
    public void finish(Database.BatchableContext BC){
        System.debug('ContactOptOut FINNISH');
        
        Database.executeBatch(new BatchOptOutCompleted(), 20);
    }   
}