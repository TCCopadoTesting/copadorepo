/* Author: Varma Alluri on 04.04.2017    
Description: Jira Ticket: SAEN-2252; ContentDocumentLinkTrigger calls this Handler populating a custom checkbox 
field (HasAttachment__c) on the subcase to TRUE or FALSE depending on if a content document exists on the subcase or not. */

public class ContentDocumentLinkTriggerHandler {
    public static void OnafterInsert(List<ContentDocumentLink> newList){
        CasetoCheck(newList);
    }
    public static void OnbeforeDelete(List<ContentDocumentlink> cds){
        CasetoUncheck(cds);
    }
    Public static void CasetoCheck(List<ContentDocumentLink> newList){
        Id recTypeId = SEUTility.getRecordTypeId('Enterprise_Subcase');
        Set<Id> CaseIds = new Set<Id>();
        List<Case> listCase = new List<Case>();
        string csprfx= Case.SObjectType.getDescribe().getKeyPrefix();
        
        for (ContentDocumentLink cdl : newList) {
            String id = cdl.LinkedEntityId;
            if(cdl.LinkedEntity.Type == 'Case' || id.substring(0,3) == csprfx){
                CaseIds.add(cdl.LinkedEntityId);
            }
        }
        List<Case> Caselist = [Select Id,HasAttachment__c,RecordTypeId from Case where RecordTypeId=:recTypeId and Id IN:CaseIds];
        if(Caselist!=null && Caselist.size()>0){
            for(Case cs : Caselist){
                cs.HasAttachment__c = true;
                listCase.add(cs);
            }
        }
        if(!listCase.isEmpty()){
            try{
                update listCase;
            }catch(exception e){
                system.debug('exception=='+e);
            }
        }
    }
    
    Public static void CasetoUncheck(List<ContentDocumentlink> cdls){
        
        Set<Id> CaseIds = new Set<Id>();
        List<Case> listCase = new List<Case>();
        string csprfx= Case.SObjectType.getDescribe().getKeyPrefix();
        for (ContentDocumentLink cdl : cdls) {
            String id = cdl.LinkedEntityId;
            if(id.substring(0,3) == csprfx){
                CaseIds.add(cdl.LinkedEntityId);
            }
        }
        Id recTypeId = SEUTility.getRecordTypeId('Enterprise_Subcase');
        List<Case> csToUpdate=new List<Case>();
        for(case cs:[Select Id,HasAttachment__c,RecordTypeId,(SELECT Id FROM ContentDocumentLinks),(SELECT Id FROM attachments) FROM Case where RecordTypeId=:recTypeId and ID In:CaseIds]){
            if(cs.ContentDocumentLinks.Size()==1 && cs.HasAttachment__c==True && cs.attachments.Size()==0 ) {
                cs.HasAttachment__c=false; 
                csToUpdate.add(cs);
            } 
        }
        if(!csToUpdate.isEmpty()){
            try{ 
                update csToUpdate;
            }catch(exception e){
                system.debug('exception=='+e);
            }
        }
    }
}