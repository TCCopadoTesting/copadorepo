trigger ContractTrigger on Contract (before update,before insert){
    system.debug('^^^^^'+trigger.new);
    ContractTriggerHandler handler = new ContractTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isBefore){        
        if(Trigger.isUpdate){
            handler.OnbeforeUpdate(Trigger.new);
        }else if (Trigger.isInsert){   
            handler.OnbeforeInsert(Trigger.new, Trigger.newMap);
        }
    }
}