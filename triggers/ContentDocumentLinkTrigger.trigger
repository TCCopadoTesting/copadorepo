/* Author: Varma Alluri on 04.04.2017                                                                                    
   Description: Jira Ticket: SAEN-2252; Trigger populating a custom checkbox field (HasAttachment__c)
   on the subcase to TRUE or FALSE depending on content document exists on the subcase or not. */

trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert,before delete) {
     if(trigger.isInsert && trigger.isAfter){
        ContentDocumentLinkTriggerHandler.OnafterInsert(trigger.New);
    }
    if (trigger.isBefore && trigger.isDelete) {
        ContentDocumentLinkTriggerHandler.OnbeforeDelete(trigger.old);
    }
}