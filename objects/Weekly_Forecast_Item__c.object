<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object links the FOLs (Forecast Opportnity Links) into the Kvartalsprognos and Årsmål reocrds. An opportunity is allocated to a Veckoprognos based on its Close Date.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Accumulated_Continuation_Sales_Outcome_X__c</fullName>
        <description>A rollup of the Continuation Sales Amount for all WON opportunities.
prev label: Utfall Continuation Sales</description>
        <externalId>false</externalId>
        <label>Outcome Continuation Sales</label>
        <summarizedField>Forecast_Opportunity_Link__c.Continuation_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Won__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Accumulated_New_Sales_Outcome_X__c</fullName>
        <description>A rollup of the New Sales Amount for all WON opportunities.
prev label: Utfall New Sales</description>
        <externalId>false</externalId>
        <label>Outcome New Sales</label>
        <summarizedField>Forecast_Opportunity_Link__c.New_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Won__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Accumulated_Sales_Value_Outcome_X__c</fullName>
        <description>A rollup of the Sales Value Amount for all WON opportunities.</description>
        <externalId>false</externalId>
        <label>Outcome Sales Value</label>
        <summarizedField>Forecast_Opportunity_Link__c.Sales_Value_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Won__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Accumulated_Sales_Value__c</fullName>
        <description>Configured for Sales Value Target Change</description>
        <externalId>false</externalId>
        <label>Accumulated Sales Value</label>
        <summarizedField>Forecast_Opportunity_Link__c.Sales_Value_Amount__c</summarizedField>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Achievement_Sales_Value__c</fullName>
        <description>Configured for Sales Value Target Change</description>
        <externalId>false</externalId>
        <formula>IF(isWeekInPast__c, Accumulated_Sales_Value__c , Sales_Value_Target__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Achievement Sales Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Achievement_Value_Continuation_Sales_X__c</fullName>
        <description>If the week is in the past this field shows the outcome, otherwise the commit value.
prev label: Måluppfyllelse Värde Continuation Sales</description>
        <externalId>false</externalId>
        <formula>if( isWeekInPast__c, Accumulated_Continuation_Sales_Outcome_X__c,  Continuation_Sales_Commit__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Achievement Value Continuation Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Achievement_Value_New_Sales_X__c</fullName>
        <description>If the week is in the past this field shows the outcome, otherwise the commit value.
prev label: Måluppfyllelse Värde New Sales</description>
        <externalId>false</externalId>
        <formula>if( isWeekInPast__c,  Accumulated_New_Sales_Outcome_X__c, New_Sales_Commit__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Achievement Value New Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Active_Week__c</fullName>
        <description>Is true when the seller start date is before the end date of the week and the seller end date is after the start date of the week
prev label: Aktiv vecka</description>
        <externalId>false</externalId>
        <formula>IF((Seller__r.Start_Date__c &lt;= End_Date__c &amp;&amp; (Seller__r.End_Date__c &gt;= Start_Date__c || ISBLANK(Seller__r.End_Date__c))) || ISBLANK(Seller__c) , true, false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Active Week</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Belongs_To_User__c</fullName>
        <description>Used for reporting toshow only the logged in users records</description>
        <externalId>false</externalId>
        <formula>User__c = $User.Id</formula>
        <label>Tillhör $User</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Belongs_To_Year_Id__c</fullName>
        <description>Used to reduce number of SOQLs. Do not remove</description>
        <externalId>false</externalId>
        <formula>Quarterly_Forecast_Item__r.Yearly_Target__r.Id</formula>
        <label>Tillhör År Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Belongs_To_Year__c</fullName>
        <description>Used to reduce SOQLs. Do not remove.</description>
        <externalId>false</externalId>
        <formula>VALUE(Quarterly_Forecast_Item__r.Yearly_Target__r.Name)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tillhör År</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comitted_on__c</fullName>
        <description>Time/ Date commit was done. This is updated when the user clicks the Commit button or submits a Judge into the Large Commit View</description>
        <externalId>false</externalId>
        <label>Commited on</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>CommitCS_plus_Judges__c</fullName>
        <description>Used for Large: Commit (weighted CS value of the open opportunities) plus the subordinates Judges.</description>
        <externalId>false</externalId>
        <formula>Commit_Continuation_Sales_Large__c +   Roll_up_Judge_CS__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Commit - Continuation Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CommitNS_plus_Judges__c</fullName>
        <description>Used for Large: Commit (weighted NS value of the open opportunities) plus the subordinates Judges.</description>
        <externalId>false</externalId>
        <formula>Commit_New_Sales_Large__c +  Roll_up_Judge_NS__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Commit - New Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Commit_Continuation_Sales_Large__c</fullName>
        <description>A rollup of the Weighted Continuation Sales Amount for all open opportunities. This field is predominantely for Large but can also be used by SME as the Opportunity.Large_Continuation_Sales__c field is updated from the roll-up fields when products are added to the Opportunity.</description>
        <externalId>false</externalId>
        <label>Commit Continuation Sales Large</label>
        <summarizedField>Forecast_Opportunity_Link__c.Weighted_Continuation_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Commit_New_Sales_Large__c</fullName>
        <description>A rollup of the Weighted New Sales Amount for all open opportunities. This field is predominantely for Large but can also be used by SME as the Opportnity.Large_New_Sales__c field is updated from the roll-up fields when products are added to the Opportunity.</description>
        <externalId>false</externalId>
        <label>Commit New Sales Large</label>
        <summarizedField>Forecast_Opportunity_Link__c.Weighted_New_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Continuation_Sales_Amount_Open_Opps__c</fullName>
        <description>This is the total unweighted Continuation Sales amount for all open opps. Used for Large: Commit (replaces the old CommitCS_plus_Judges__c field).</description>
        <externalId>false</externalId>
        <formula>Commit_Continuation_Sales_Large__c + Upside_Continuation_Sales__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Continuation Sales Amount Open Opps</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Continuation_Sales_Commit__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Used by SME. Gets populated when the users enters a value in the SME commit view. Is set on firsrt create of the record by this workflow rule https://telia.my.salesforce.com/01Q24000000Csam</description>
        <externalId>false</externalId>
        <label>Commit Continuation Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Continuation_Sales_Target__c</fullName>
        <defaultValue>0</defaultValue>
        <description>When the Årsmål record is created all Veckoprognos records are created and the Mål CS is split between the weeks based on the quarterly forecast CS target. Quarterly forecast target is split into 13 or 14, based on the number of weeks in that quarter. This code to update the Veckoprognos target works on insert or update of a new Årsmål
prev lab: Mål Continuation Sales</description>
        <externalId>false</externalId>
        <label>Continuation Sales Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Direct_Sales_Outcome__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Not used anymore? Field for SME that was suppose to show the outcome from the &quot;quick process&quot; opportunities. AN: This field has one reference in the commitViewController and two references in the commitView page. The field is not updated by any code/triggers or refernced in any reports. Anders Håkansson asked that this field remain in the system as it is displayed in the SME Commit
prev: Direktsälj Prognos</description>
        <externalId>false</externalId>
        <label>Direct Sales Outcome</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>The End Date of the week
prev label:Slutdatum</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Judge_CS__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Not in use (Populated from the Large Commit View). Note: In the new Large Commit View the users are not doing any judge, they only click commit which updates the &quot;commit on&quot;-datetime field on the Weekly Forecast/FOL records.</description>
        <externalId>false</externalId>
        <label>Judge CS</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Judge_NS__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Not in use (Populated from the Large Commit View). Note: In the new Large Commit View the users are not doing any judge, they only click commit which updates the &quot;commit on&quot;-datetime field on the Weekly Forecast/FOL records.</description>
        <externalId>false</externalId>
        <label>Judge NS</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>My_Teams_Weekly_Forecast__c</fullName>
        <description>Used for reporting to identify which Week records are within the logged in users team.</description>
        <externalId>false</externalId>
        <formula>Seller__r.Manager__r.User__r.Id = $User.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>My Teams Weekly Forecast</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>New_Sales_Amount_Open_Opps__c</fullName>
        <description>This is the total unweighted New Sales amount for all open opps. Used for Large: Commit (replaces the old CommitNS_plus_Judges__c field).</description>
        <externalId>false</externalId>
        <formula>Commit_New_Sales_Large__c + Upside_New_Sales__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>New Sales Amount Open Opps</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>New_Sales_Commit__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Used by SME. Gets populated when the users enters a value in the SME commit view. Is set on firsrt create of the record by this workflow rule https://telia.my.salesforce.com/01Q24000000Csam</description>
        <externalId>false</externalId>
        <label>Commit New Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>New_Sales_Target__c</fullName>
        <defaultValue>0</defaultValue>
        <description>When the Årsmål record is created all Veckoprognos records are created and the Mål NS  is split between the weeks based on the quarterly forecast CS target. Quarterly forecast target is split into 13 or 14, based on the number of weeks in that quarter. This code to update the Veckoprognos target only works on insert or update  of a new Årsmål
prev label: Mål New Sales</description>
        <externalId>false</externalId>
        <label>New Sales Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Outcome_Commit_CS__c</fullName>
        <description>Shows the outcome of the Opportunties linked to this week if they were included in the Commit for this week. This is used to display in the Large Commit View</description>
        <externalId>false</externalId>
        <label>Outcome Commit CS</label>
        <summarizedField>Forecast_Opportunity_Link__c.Continuation_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Include_in_Outcome_Commit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Won__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Outcome_Commit_NS__c</fullName>
        <description>Shows the outcome of the Opportunties linked to this week if they were included in the Commit for this week. This is used to display in the Large Commit View</description>
        <externalId>false</externalId>
        <label>Outcome Commit NS</label>
        <summarizedField>Forecast_Opportunity_Link__c.New_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Include_in_Outcome_Commit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Won__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Quarterly_Forecast_Item__c</fullName>
        <description>Links Veckoprognos records to the correct Kvartal record and allows values to be rolled up</description>
        <externalId>false</externalId>
        <label>Kvartalsprognos</label>
        <referenceTo>Quarterly_Forecast_Item__c</referenceTo>
        <relationshipLabel>Veckoprognoser</relationshipLabel>
        <relationshipName>Weekly_Targets</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Revenue_Impact_This_Year__c</fullName>
        <description>A rollup of the Revenue Impact This Year for all opportunities.</description>
        <externalId>false</externalId>
        <label>Revenue Impact This Year</label>
        <summarizedField>Forecast_Opportunity_Link__c.Revenue_Impact_This_Year__c</summarizedField>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Risk_Continuation_Sales__c</fullName>
        <description>A rollup of the Continuation Sales Amount for all open risk opportunities.</description>
        <externalId>false</externalId>
        <label>Risk Continuation Sales</label>
        <summarizedField>Forecast_Opportunity_Link__c.Weighted_Continuation_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Risk__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Risk_New_Sales__c</fullName>
        <description>A rollup of the New Sales Amount for all open risk opportunities.</description>
        <externalId>false</externalId>
        <label>Risk New Sales</label>
        <summarizedField>Forecast_Opportunity_Link__c.Weighted_New_Sales_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Risk__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Roll_up_Judge_CS__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Summary of the subordinates judges. Populated by trigger on the weekly forecast..</description>
        <externalId>false</externalId>
        <label>Roll-up Judge CS</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Roll_up_Judge_NS__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Summary of the subordinates judges. Populated by trigger on the weekly forecast..</description>
        <externalId>false</externalId>
        <label>Roll-up Judge NS</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Sales_Team__c</fullName>
        <description>Formula to display the Sales Team Name on the Veckoprognos record</description>
        <externalId>false</externalId>
        <formula>Seller__r.Sales_Team_Name__c</formula>
        <label>Sales Team Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Unit__c</fullName>
        <description>Formula to display the Sales Unit Name on the Veckoprognos record</description>
        <externalId>false</externalId>
        <formula>Seller__r.Sales_Unit_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Sales Unit Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Value_Target__c</fullName>
        <description>Sales Value Target , divided equally amongst all Weeks</description>
        <externalId>false</externalId>
        <label>Sales Value Target</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Seller__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Seller that this Veckoprognos relates to
prev label: Säljare</description>
        <externalId>false</externalId>
        <label>Seller</label>
        <referenceTo>Seller__c</referenceTo>
        <relationshipLabel>Veckoprognoser</relationshipLabel>
        <relationshipName>Veckoprognoser</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>The Start Date of the week
prev label: Startdatum</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Subordinates_Commit_Continuation_Sales__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Summary of the subordinates commit (SME)
prev label: Underordnade - Commit Continuation Sales</description>
        <externalId>false</externalId>
        <label>Subordinates - Commit Continuation Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Subordinates_Commit_New_Sales__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Summary of the subordinates commit (SME)
prev label: Underordnade - Commit New Sales</description>
        <externalId>false</externalId>
        <label>Subordinates - Commit New Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Superior_User__c</fullName>
        <description>Returns the User ID of the Seller&apos;s Manager
prev label: Överordnad Användare</description>
        <externalId>false</externalId>
        <formula>Seller__r.Manager__r.User__c</formula>
        <label>Superior User</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Upside_Continuation_Sales__c</fullName>
        <description>A rollup of the Upside Continuation Sales amount for all open opportunities.
prev label: Uppsida Continuation Sales</description>
        <externalId>false</externalId>
        <label>Upside Continuation Sales</label>
        <summarizedField>Forecast_Opportunity_Link__c.Upside_Continuation_Sales__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Upside_New_Sales__c</fullName>
        <description>A rollup of the Upside New Sales amount for all open opportunities.
prev label: Uppsida New Sales</description>
        <externalId>false</externalId>
        <label>Upside New Sales</label>
        <summarizedField>Forecast_Opportunity_Link__c.Upside_New_Sales__c</summarizedField>
        <summaryFilterItems>
            <field>Forecast_Opportunity_Link__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>Forecast_Opportunity_Link__c.Weekly_Forecast_Item__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Links the Veckoprognos to the User (this will always be the same user that is linked via the Seller)</description>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Weekly_Targets</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>isWeekInPast__c</fullName>
        <defaultValue>false</defaultValue>
        <description>True if the week is in the past and not equal to the current week. The value is updated by a batch job which runs every Sunday, and sets the check box to true for all weeks in the past.</description>
        <externalId>false</externalId>
        <label>isWeekInPast</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Veckoprognos</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>User__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Vecka</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Veckoprognoser</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Quarterly_Forecast_Item__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>User__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Quarterly_Forecast_Item__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>User__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
