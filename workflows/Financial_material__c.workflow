<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PPC_Update_FM_ID_Unique</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Unique_Financial_Material_ID__c</field>
        <formula>Name</formula>
        <name>PPC Update FM ID Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PPC Set FM ID Unique</fullName>
        <actions>
            <name>PPC_Update_FM_ID_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Financial_material__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>PPC Workflow Rule to Update Financial Material ID.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
