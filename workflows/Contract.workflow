<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_Notice_to_Cygate_Account_Manager</fullName>
        <description>Send Email Notice to Cygate Account Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@cygate.se</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Contract_Expiration_Notice</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Notice_to_Cygate_Account_Manager1</fullName>
        <description>Send Email Notice to Cygate Account Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@cygate.se</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Contract_Expiration_Notice</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Notice_to_Owner</fullName>
        <description>Send Email Notice to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Telia_Contracts_Notification_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Action_needed_checkbox_will_checked</fullName>
        <description>Action needed checkbox will be checked when agreement termination reminder is sent out.</description>
        <field>Actionneeded__c</field>
        <literalValue>1</literalValue>
        <name>Action needed checkbox -&apos;checked&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_needed_checkbox_will_unchecked</fullName>
        <description>Action needed checkbox will be unchecked when contract end date changes.</description>
        <field>Actionneeded__c</field>
        <literalValue>0</literalValue>
        <name>Action needed checkbox -&apos;unchecked&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_role</fullName>
        <description>This filed update will set the owner role to contract owner role field.
REF-CASE:2953</description>
        <field>Contract_Owner_Role__c</field>
        <formula>Owner.UserRole.Name</formula>
        <name>Set Owner role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Competitor- Send Notice Email</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract.Reminder_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>Konkurrentavtal,Teliaavtal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Integration User</value>
        </criteriaItems>
        <description>When a contract is created/edited and the Reminder Date is set, the system will schedule an email reminder to be sent to the account owner when the reminder date is met. Also the action needed flag will be set on the contract,</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_Notice_to_Owner</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Action_needed_checkbox_will_checked</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.Reminder_Date__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Owner Role</fullName>
        <actions>
            <name>Set_Owner_role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygateavtal,Cygate Konkurrentavtal</value>
        </criteriaItems>
        <description>This Workflow will set the Contract owner role to Contract Owner Role field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cygate Contract - Send Notice Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Reminder_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygateavtal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Definitive_End_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sending a reminder to the Cygate Account manager when a Cygate contract is about to expire</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_Notice_to_Cygate_Account_Manager</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Action_needed_checkbox_will_checked</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.Reminder_Date__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_Notice_to_Cygate_Account_Manager1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Telia or Competitor Contract - Send Notice Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Reminder_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>Teliaavtal,Telia Konkurrentavtal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Integration User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Reminder_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Old lablel: &quot;Competitor- Send Notice Email&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_Notice_to_Owner</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Action_needed_checkbox_will_checked</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.Reminder_Date__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Unchecking Action Needed Checkbox</fullName>
        <actions>
            <name>Action_needed_checkbox_will_unchecked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Action needed checkbox will be unchecked when the contract end date changes.</description>
        <formula>Actionneeded__c  = true  &amp;&amp; (ISCHANGED( EndDate ) || ISCHANGED( ContractTerm ) ||  ISCHANGED( Extension_Period__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
