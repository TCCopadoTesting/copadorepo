<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_2nd_Reminder_to_KKnr_Owner</fullName>
        <description>Send 2nd Reminder to KKnr Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Agreement_Situation_Expiration_Notice</template>
    </alerts>
    <alerts>
        <fullName>Send_Reminder_to_KKnr_Owner</fullName>
        <description>Send Reminder to KKnr Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Agreement_Situation_Expiration_Notice</template>
    </alerts>
    <rules>
        <fullName>Agreement Situation Expiration Notice</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Agreement_Situation__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement_Situation__c.Reminder_days_before__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an email reminder to the KKnr Owner X days before the Agreement End Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_to_KKnr_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Agreement_Situation__c.Reminder_Date__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Agreement Situation Expiration Notice Option 1</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Agreement_Situation__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement_Situation__c.Option_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an email reminder to the KKnr Owner X days before the Agreement End Date + Option 1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_to_KKnr_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Agreement_Situation__c.Reminder_Date_Option_1__c</offsetFromField>
            <timeLength>-6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Agreement Situation Expiration Notice Option 2</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Agreement_Situation__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement_Situation__c.Option_2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an email reminder to the KKnr Owner X days before the Agreement End Date + Option 2</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_to_KKnr_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Agreement_Situation__c.Reminder_Date_Option_2__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Agreement Situation Expiration Notice%09 - 2nd Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Agreement_Situation__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement_Situation__c.Reminder_2_days_before__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an email reminder to the KKnr Owner Reminder 2 days before the Agreement End Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_2nd_Reminder_to_KKnr_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Agreement_Situation__c.Reminder_2_Date__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
