<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Business_Area_Name</fullName>
        <field>Business_Area_Name__c</field>
        <formula>IF( ISPICKVAL(Manager__r.Type__c ,&quot;Business Area Manager&quot;), Manager__r.Business_Area_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Type__c ,&quot;Business Area Manager&quot;),Manager__r.Manager__r.Business_Area_Name__c, 
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Type__c ,&quot;Business Area Manager&quot;),Manager__r.Manager__r.Manager__r.Business_Area_Name__c, 
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Manager__r.Type__c ,&quot;Business Area Manager&quot;),Manager__r.Manager__r.Manager__r.Manager__r.Business_Area_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Manager__r.Manager__r.Type__c ,&quot;Business Area Manager&quot;),Manager__r.Manager__r.Manager__r.Manager__r.Manager__r.Business_Area_Name__c,&quot;&quot;)))))</formula>
        <name>Business Area Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Unit_Name</fullName>
        <field>Business_Unit_Name__c</field>
        <formula>IF( ISPICKVAL(Manager__r.Type__c ,&quot;Business Unit Manager&quot;), Manager__r.Business_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Type__c ,&quot;Business Unit Manager&quot;),Manager__r.Manager__r.Business_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Type__c ,&quot;Business Unit Manager&quot;),Manager__r.Manager__r.Manager__r.Business_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Manager__r.Type__c ,&quot;Business Unit Manager&quot;),Manager__r.Manager__r.Manager__r.Manager__r.Business_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Manager__r.Manager__r.Type__c ,&quot;Business Unit Manager&quot;),Manager__r.Manager__r.Manager__r.Manager__r.Manager__r.Business_Unit_Name__c,&quot;&quot;)))))</formula>
        <name>Business Unit Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Team_Name</fullName>
        <field>Sales_Team_Name__c</field>
        <formula>Manager__r.Sales_Team_Name__c</formula>
        <name>Sales Team Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Team_Number</fullName>
        <description>Copy the Sales Team Number from the Sales Team Manager record to the Seller.</description>
        <field>Sales_Team_Number__c</field>
        <formula>Manager__r.Sales_Team_Number__c</formula>
        <name>Sales Team Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Unit_Name</fullName>
        <field>Sales_Unit_Name__c</field>
        <formula>IF( ISPICKVAL(Manager__r.Type__c ,&quot;Sales Unit Director&quot;), Manager__r.Sales_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Type__c ,&quot;Sales Unit Director&quot;),Manager__r.Manager__r.Sales_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Type__c ,&quot;Sales Unit Director&quot;),Manager__r.Manager__r.Manager__r.Sales_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Manager__r.Type__c ,&quot;Sales Unit Director&quot;),Manager__r.Manager__r.Manager__r.Manager__r.Sales_Unit_Name__c,
IF( ISPICKVAL(Manager__r.Manager__r.Manager__r.Manager__r.Manager__r.Type__c ,&quot;Sales Unit Director&quot;),Manager__r.Manager__r.Manager__r.Manager__r.Manager__r.Sales_Unit_Name__c,&quot;&quot;)))))</formula>
        <name>Sales Unit Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSellerName</fullName>
        <description>This field update will set the user firstname + lastname to the field Seller_Name__c on the Seller__c object. This allows the users to search on the name in the Lookup Dialog for Sellers records.</description>
        <field>Seller_Name__c</field>
        <formula>User__r.FirstName+&apos; &apos;+ User__r.LastName</formula>
        <name>SetSellerName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSellerTeliaId</fullName>
        <description>This field update will set the user TeliaId to the field TeliaID__c on the Seller__c object. This allows the users to search on the telia ID in the Lookup Dialog for Sellers records.</description>
        <field>TeliaID__c</field>
        <formula>User__r.Telia_Id__c</formula>
        <name>SetSellerTeliaId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Business Unit Manager Updates</fullName>
        <actions>
            <name>Business_Area_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Business Area Names when the Manager is updated on a Seller where Type = Business Unit Manager</description>
        <formula>ISPICKVAL( Type__c ,&quot;Business Unit Manager&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Team Manager Updates</fullName>
        <actions>
            <name>Business_Area_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Business_Unit_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Unit_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Sales Unit Names, Business Unit Names and Business Area Names when the Manager is updated on a Seller where Type = Sales Team Manager</description>
        <formula>ISPICKVAL( Type__c ,&quot;Sales Team Manager&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Unit Director Updates</fullName>
        <actions>
            <name>Business_Area_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Business_Unit_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Business Unit Names and Business Area Names when the Manager is updated on a Seller where Type = Sales Unit Director</description>
        <formula>ISPICKVAL( Type__c ,&quot;Sales Unit Director&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Seller Manager Updates</fullName>
        <actions>
            <name>Business_Area_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Business_Unit_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Team_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Team_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Unit_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Sales Team Names, Sales Unit Names, Business Unit Names and Business Area Names when the Manager is updated on a Seller where Type = Seller</description>
        <formula>AND(ISPICKVAL( Type__c ,&quot;Seller&quot;), Is_Temporary_Manager__c  = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Seller Text Fields</fullName>
        <actions>
            <name>SetSellerName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetSellerTeliaId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Seller__c.User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
