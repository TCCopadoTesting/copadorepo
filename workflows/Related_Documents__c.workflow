<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Conga_Acc_Owner_Mobile</fullName>
        <description>fetched from user record, for conga purposes.</description>
        <field>Conga_Cygate_Account_Manager_Mobile__c</field>
        <formula>Account__r.Cygate_Account_Manager__r.MobilePhone</formula>
        <name>Set Conga Acc Owner Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Cygate_Account_Manager_Email</fullName>
        <description>Fetched from user record, for Conga purposes.</description>
        <field>Conga_Cygate_Account_Manager_Email__c</field>
        <formula>Account__r.Cygate_Account_Manager__r.Email</formula>
        <name>Set Conga Cygate Account Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Cygate_Account_Manager_Name</fullName>
        <field>Conga_Cygate_Account_Manager_Name__c</field>
        <formula>Opportunity__r.Account.Cygate_Account_Manager__r.FirstName + &quot; &quot; + Opportunity__r.Account.Cygate_Account_Manager__r.LastName</formula>
        <name>Set Conga Cygate Account Manager Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Cygate_Account_Manager_Phone</fullName>
        <description>fetched from user record, for conga purposes.</description>
        <field>Conga_Cygate_Account_Manager_Phone__c</field>
        <formula>Account__r.Cygate_Account_Manager__r.Phone</formula>
        <name>Set Conga Cygate Account Manager Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Account_Address</fullName>
        <field>Conga_Opportunity_Account_Billingaddress__c</field>
        <formula>Opportunity__r.Account.BillingStreet</formula>
        <name>Set Conga Opp Account Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Account_City</fullName>
        <field>Conga_Opportunity_Account_Billingcity__c</field>
        <formula>Opportunity__r.Account.BillingCity</formula>
        <name>Set Conga Opp Account City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Account_Id</fullName>
        <description>Sets the opp account id (orgnr used by cygate)</description>
        <field>Conga_Opp_Account__c</field>
        <formula>Opportunity__r.Account.Cygate_Account_ID__c</formula>
        <name>Set Conga Opp Account Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Account_Name</fullName>
        <field>Conga_Opportunity_Account_Name__c</field>
        <formula>Opportunity__r.Account.Name</formula>
        <name>Set Conga Opp Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Account_Postalcode</fullName>
        <field>Conga_Opportunity_Account_Postalcode__c</field>
        <formula>Opportunity__r.Account.BillingPostalCode</formula>
        <name>Set Conga Opp Account Postalcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Email</fullName>
        <description>Fetched from opp owners user record. Used for Conga purposes-</description>
        <field>Conga_Opportunity_Owner_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>Set Conga Opp Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Mobile</fullName>
        <description>Used for Conga purposes.</description>
        <field>Conga_Opportunity_Owner_Mobile__c</field>
        <formula>Opportunity__r.Owner.MobilePhone</formula>
        <name>Set Conga Opp Owner Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Phone</fullName>
        <description>Fetches the users phone nr from user record</description>
        <field>Conga_Opportunity_Owner_Phone__c</field>
        <formula>Opportunity__r.Owner.Phone</formula>
        <name>Set Conga Opp Owner Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Title</fullName>
        <description>Used for Conga documents merging.</description>
        <field>Conga_Opportunity_Owner_Title__c</field>
        <formula>Opportunity__r.Owner.Title</formula>
        <name>Set Conga Opp Owner Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cygate_AM_Email</fullName>
        <field>Conga_Cygate_Account_Manager_Email__c</field>
        <formula>Opportunity__r.Account.Cygate_Account_Manager__r.Email</formula>
        <name>Set Cygate AM Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cygate_AM_Mobile</fullName>
        <field>Conga_Cygate_Account_Manager_Mobile__c</field>
        <formula>Opportunity__r.Account.Cygate_Account_Manager__r.MobilePhone</formula>
        <name>Set Cygate AM Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cygate_AM_Phone</fullName>
        <description>Fetches Opp Accounts Cygate A.Ms phone from user record.</description>
        <field>Conga_Cygate_Account_Manager_Phone__c</field>
        <formula>Opportunity__r.Account.Cygate_Account_Manager__r.Phone</formula>
        <name>Set Cygate AM Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Account_Order_Id</fullName>
        <description>Fetch Visma id for Conga purposes</description>
        <field>Conga_Opp_Account_Order_Id__c</field>
        <formula>Opportunity__r.OrganisationalUnit__r.Order_Account_Id__c</formula>
        <name>Set Opp Account Order Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Name</fullName>
        <description>Fetches opp Name from the related opportunity. As Chatter plus users can&apos;t access opportunities, this field is populated so as ti be visible for these users</description>
        <field>Opportunity_Name__c</field>
        <formula>Opportunity__r.Name</formula>
        <name>Set Opportunity Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Number</fullName>
        <description>Fetches opp no from the related opportunity. The number is suppose to be used in the naming of the actual document merged with Conga.</description>
        <field>Opportunity_no__c</field>
        <formula>Opportunity__r.Opportunity_id__c</formula>
        <name>Set Opportunity No.</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Owner_Name</fullName>
        <field>Conga_Opportunity_Owner_Name__c</field>
        <formula>Opportunity__r.Owner.FirstName + &quot; &quot; + Opportunity__r.Owner.LastName</formula>
        <name>Set Opportunity Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Get Opp Account Cygate AM Info</fullName>
        <actions>
            <name>Set_Conga_Cygate_Account_Manager_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Cygate_AM_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Cygate_AM_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Cygate_AM_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opportunity_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets info about the Opportunity accounts&apos; Cygate Account Manager.</description>
        <formula>NOT(ISBLANK(Opportunity__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp Account Info if Visma Account</fullName>
        <actions>
            <name>Set_Opp_Account_Order_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Opportunity__c)) &amp;&amp; NOT(ISBLANK(Opportunity__r.OrganisationalUnit__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity No%2E</fullName>
        <actions>
            <name>Set_Opportunity_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opportunity_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If lookup field Opportunity is populated, the field Opportunity no should be populated.</description>
        <formula>NOT(ISBLANK(Opportunity__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
