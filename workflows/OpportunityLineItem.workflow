<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculate_Contribution_Margin</fullName>
        <field>Contribution_Margin__c</field>
        <formula>Total_Amount__c *
 Cygate_Margin__c</formula>
        <name>Calculate Contribution Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calculate_Cygate_Margin</fullName>
        <field>Cygate_Margin__c</field>
        <formula>IF((Product2.Name =&quot;Cygate DMD - 1 - Traditionell leverans - Produkt&quot; ||Product2.Name =&quot;Cygate DMD - 2 - Privat tjänst, skräddarsydd - Produkt&quot; || 
Product2.Name =&quot;Cygate Infrastruktur - 2 - Privat tjänst, skräddarsydd - Produkt&quot; || Product2.Name =&quot;Cygate Nätverk - 1 - Traditionell leverans - Produkt&quot; || 
Product2.Name =&quot;Cygate Nätverk - 2 - Privat tjänst, skräddarsydd - Produkt&quot; || Product2.Name = &quot;Cygate Server/Lagring/Applikation - 1 - Traditionell leverans - Produkt&quot; || 
Product2.Name = &quot;Cygate Server/Lagring/Applikation - 2 - Privat tjänst, skräddarsydd - Produkt&quot; || Product2.Name = &quot;Cygate Säkerhet - 1 - Traditionell leverans - Produkt&quot; || 
Product2.Name = &quot;Cygate Säkerhet - 2 - Privat tjänst, skräddarsydd - Produkt&quot; || Product2.Name = &quot;Cygate UC - 1 - Traditionell leverans - Produkt&quot; || 
Product2.Name = &quot;Cygate UC - 2 - Privat tjänst, skräddarsydd - Produkt&quot; || Product2.Name = &quot;Cygate Övrigt - 1 - Traditionell leverans - Produkt&quot;
|| Product2.Name = &quot;Cygate Övrigt - 2 - Privat tjänst, skräddarsydd - Produkt&quot;), 0.17, 
IF(Product2.Name = &quot;Cygate Infrastruktur - 1 - Traditionell leverans - Produkt&quot;, 0.25, 0.38 ))</formula>
        <name>Calculate Cygate Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Family</fullName>
        <field>Product_Family__c</field>
        <formula>TEXT(Product2.Family)</formula>
        <name>Product Family</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Name</fullName>
        <field>Product_Name__c</field>
        <formula>Product2.Name</formula>
        <name>Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bypass_Cygate_Sub_Products</fullName>
        <field>By_Pass_Cygate_Sub_Product__c</field>
        <literalValue>1</literalValue>
        <name>Update Bypass Cygate Sub Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>265 - Set Product Family</fullName>
        <actions>
            <name>Product_Family</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Copy the product family to the opportunity product from the related product</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>265 - Set Product Name</fullName>
        <actions>
            <name>Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Copy the product name to the opportunity product from the related product</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SALEF 188 - Set By Pass Cygate Sub Product</fullName>
        <actions>
            <name>Update_Bypass_Cygate_Sub_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.By_Pass_Cygate_Sub_Product__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Contribution Margin</fullName>
        <actions>
            <name>Calculate_Contribution_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygate efterregistrering,Cygate affärsmöjlighet,Cygate förenklad affärsmöjlighet,Cygate affär</value>
        </criteriaItems>
        <description>Used by Cygate to set the Contribution margin on the opportunity product. This value is then rolled up to the Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Product Margin</fullName>
        <actions>
            <name>Calculate_Cygate_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Cygate Margin field based on the Product and Business Type selected</description>
        <formula>( Opportunity.RecordType.DeveloperName  = &apos;Cygate_Efterregistrering&apos;   ||   Opportunity.RecordType.DeveloperName  = &apos;Cygate_Standard_Opportunity&apos;   ||  Opportunity.RecordType.DeveloperName  =  &apos;Cygate_Utokning&apos;  || Opportunity.RecordType.DeveloperName = &apos;Cygate_Aff_r&apos;) &amp;&amp; ISNULL(Cygate_Margin__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
