<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Client_Meetings_from_Rollup</fullName>
        <description>Set the Client Meetings Target to the same value as Client Meetings Target Rollup</description>
        <field>Client_Meetings_Target__c</field>
        <formula>Client_Meetings_Target_Rollup__c</formula>
        <name>Set Client Meetings from Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Sales_Revenue_from_Rollup</fullName>
        <field>Product_Sales_Revenue_Target__c</field>
        <formula>Product_Sales_Revenue_Target_Rollup__c</formula>
        <name>Set Product Sales Revenue from Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sales_Revenue_from_Rollup</fullName>
        <field>Sales_Revenue_Target__c</field>
        <formula>Sales_Revenue_Target_Rollup__c</formula>
        <name>Set Sales Revenue from Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Service_Sales_Revenue_from_Rollup</fullName>
        <field>Services_Sales_Revenue_Target__c</field>
        <formula>Services_Sales_Revenue_Target_Rollup__c</formula>
        <name>Set Service Sales Revenue from Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateResultatMonthAndYear</fullName>
        <description>Display Month and Year on which Resultat (Result__c)field is updated</description>
        <field>Resultat_Update_Date__c</field>
        <formula>CASE( 
MONTH(TODAY()), 
1, &quot;January&quot;, 
2, &quot;February&quot;, 
3, &quot;March&quot;, 
4, &quot;April&quot;, 
5, &quot;May&quot;, 
6, &quot;June&quot;, 
7, &quot;July&quot;, 
8, &quot;August&quot;, 
9, &quot;September&quot;, 
10, &quot;October&quot;, 
11, &quot;November&quot;, 
12, &quot;December&quot;, 
&quot;None&quot;)+&apos; &apos;+ TEXT(YEAR(TODAY()))</formula>
        <name>UpdateResultatMonthAndYear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Client Meetings Target from Rollup</fullName>
        <actions>
            <name>Set_Client_Meetings_from_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Yearly_Performance_Score__c.Update_Client_Meetings_From_Rollup__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Update Client Meetings From Rollup is set to true, the value in Client Meetings Target Rollup should be copied to the editable field Client Meetings Target.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Sales Revenue Target from Rollup</fullName>
        <actions>
            <name>Set_Sales_Revenue_from_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Yearly_Performance_Score__c.Update_Sales_Revenue_From_Rollup__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Update Sales Revenue From Rollup is set to true, the value in Sales Revenue Target Rollup should be copied to the editable field Sales Revenue Target.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Product Sales Revenue Target from Rollup</fullName>
        <actions>
            <name>Set_Product_Sales_Revenue_from_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Yearly_Performance_Score__c.Update_Product_Sales_Revenue_From_Rollup__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Update Product Sales Revenue From Rollup is set to true, the value in Product Sales Revenue Target Rollup should be copied to the editable field Product Sales Revenue Target.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Resultat date update</fullName>
        <actions>
            <name>UpdateResultatMonthAndYear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To show the month and year on which resultat( Result__c) is updated</description>
        <formula>OR(AND((NOT(ISBLANK(Result__c))),ISNEW()),ISCHANGED(Result__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Service Sales Revenue Target from Rollup</fullName>
        <actions>
            <name>Set_Service_Sales_Revenue_from_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Yearly_Performance_Score__c.Update_ServiceSales_Revenue_From_Rollup__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Update Service Sales Revenue From Rollup is set to true, the value in Service Sales Revenue Target Rollup should be copied to the editable field Service Sales Revenue Target.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
