<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Active_to_Fal</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Set Active to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Active_to_true</fullName>
        <description>This Filed update will set the active box to true.</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Set Active to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_SMS_campaign_details</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Sends campaign id and SMS content to GESB</description>
        <endpointUrl>https://google.com</endpointUrl>
        <fields>Id</fields>
        <fields>SMS_Content__c</fields>
        <fields>SMS_Failed_Status__c</fields>
        <fields>SMS_Ready_to_send_Status__c</fields>
        <fields>SMS_Sent_Status__c</fields>
        <fields>SMS_Testing__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>panteleimon.z.panidis@teliacompany.com</integrationUser>
        <name>Send SMS campaign details</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Set Campaign to Inactive on EndDate</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygate Campaign,Telia Campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.EndDate</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Completed,Aborted</value>
        </criteriaItems>
        <description>When a Campaign will reach EndDate, Campaign will no longer active.SALEF-1483</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Active_to_Fal</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Cygate Campaign to Inactive</fullName>
        <actions>
            <name>Set_Active_to_Fal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygate Campaign,Telia Campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notContain</operation>
            <value>Planned,In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.EndDate</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>When a Cygate Campaign is moved to stage Avbruten &amp; Slutförd  the this workflow will set the status to inactive.SALEF-11
SALEF-1483- If Campaign end date is less than today, Campaign will be inactive.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Cygate Campaign to active</fullName>
        <actions>
            <name>Set_Active_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygate Campaign,Telia Campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notContain</operation>
            <value>Completed,Aborted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.EndDate</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>This workflow rule automatically sets the Campaign to active when a new Campaign is created and edited if it has valid end date and status (In Progress,Planned)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Trigger SMS Campaign</fullName>
        <actions>
            <name>Send_SMS_campaign_details</name>
            <type>OutboundMessage</type>
        </actions>
        <actions>
            <name>SMS_workflow_triggered</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>equals</operation>
            <value>SMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.SMS_Testing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Send_SMS_Day__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Send_SMS_Time__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Campaign_Members_Ready_to_Send__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>This rules checks the following parameters and sends an outbound message to GESB in order to for the SMS sending to start
- Current week day within allows send days
- Start/End Time within current time
- Campaign Members with Ready-to-send status
- T</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>SMS_workflow_triggered</fullName>
        <assignedTo>dnyanesh.chavan@teliacompany.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Öppen</status>
        <subject>SMS workflow triggered</subject>
    </tasks>
</Workflow>
