<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_status_to_In_Progress</fullName>
        <description>SAEN-2204 When a requirement with status Done/Klar is being edited this field update will change the status to In Progress/Pågående.</description>
        <field>Status__c</field>
        <literalValue>Pågående</literalValue>
        <name>Change status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Related_Solution_History</fullName>
        <description>SAEN-2144 This sets the value from the related solution, logged in user and the NOW() stamp into a history field. This is used as a substitute for object history not working in lightning experience. Note: &quot;tomt värde&quot;=&quot;blank value&quot; in english.</description>
        <field>Related_Solution_History__c</field>
        <formula>IF(ISBLANK(Related_Solution__r.Name), &quot;(tomt värde)&quot;, Related_Solution__r.Name) + &quot; - &quot; + 
$User.FirstName + &quot; &quot; + $User.LastName + &quot; - &quot; + 
LEFT(TEXT(NOW()), 16) + BR() + 
PRIORVALUE(Related_Solution_History__c)</formula>
        <name>Update Related Solution History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>%22Done%22 requirement is being edited</fullName>
        <actions>
            <name>Change_status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2204 When a Requirement that has been approved is being edited, we need to update some things (status from &quot;done&quot; to &quot;in progress&quot; for instance).</description>
        <formula>NOT(ISNEW()) &amp;&amp;  AND(ISPICKVAL(PRIORVALUE(Status__c),&quot;Klar&quot;), ISPICKVAL(Status__c,&quot;Klar&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Related Solution Changed</fullName>
        <actions>
            <name>Update_Related_Solution_History</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2144 When Related Solution is changed we should update a Related Solution History field with details about the change. Works as a substitute for field history tracking which isnt possible to show in a related list in Lightning Experience.</description>
        <formula>ISCHANGED(Related_Solution__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
