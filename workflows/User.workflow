<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Activation_Date</fullName>
        <field>Activated_On__c</field>
        <formula>TODAY()</formula>
        <name>Activation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivation_Date</fullName>
        <description>date when user gets deactiavted, used for reporting purpose</description>
        <field>Deactivated_on__c</field>
        <formula>TODAY()</formula>
        <name>Deactivation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Activation Date</fullName>
        <actions>
            <name>Activation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Activation Date on the user record when the user is activated.</description>
        <formula>IsActive = True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deactivation date</fullName>
        <actions>
            <name>Deactivation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to capture date when an user gets deactivated.</description>
        <formula>IsActive = False</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
