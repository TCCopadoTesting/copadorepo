<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Clear_related_Org_Unit_id</fullName>
        <field>Org_Unit_ID__c</field>
        <name>Clear related Org Unit id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>Sales_Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Org_Unit_ID</fullName>
        <field>Org_Unit_ID__c</field>
        <formula>Id</formula>
        <name>Set Org Unit ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Sales_Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Clear Account Org Unit id when additional Org Unit</fullName>
        <actions>
            <name>Clear_related_Org_Unit_id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an organisation unit is created: If the account linked to the new org. unit already has another org. unit linked to it, clear the value of Org Unit Id field on the account (this field should only be set on account if exactly one related org. unit)</description>
        <formula>Sales_Account__r.Org_Unit_Count__c &gt;0</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update account with Related Org ID</fullName>
        <actions>
            <name>Set_Org_Unit_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an org unit is created it will push its id onto the account into a text field so that opportunities can be related to it as well. Used for Cygate</description>
        <formula>Sales_Account__r.Org_Unit_Count__c =0</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update account with Related Org ID 2</fullName>
        <actions>
            <name>Set_Org_Unit_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Org_Unit_Count__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Org_Unit_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
