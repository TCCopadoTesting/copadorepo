<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approved_Date_Time_Blank</fullName>
        <field>Approved_Date_Time__c</field>
        <name>Approved Date/Time = Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Date_Time_NOW</fullName>
        <description>2140</description>
        <field>Approved_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Approved Date/Time = NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reference_Solution_Status_Blank</fullName>
        <field>Reference_Solution_Status__c</field>
        <name>Reference Solution Status = Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reference_Solution_to_true</fullName>
        <description>SAEN-2139 sets Reference Solutoion field to true if approved in Approval process</description>
        <field>Reference_Solution__c</field>
        <literalValue>1</literalValue>
        <name>Set Reference Solution to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_for_Approval_to_false</fullName>
        <description>Set Submitted for Approval to false</description>
        <field>Submitted_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Set &apos;Submitted for Approval&apos; to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_Date_Time_Blank</fullName>
        <field>Submitted_Date_Time__c</field>
        <name>Submitted Date/Time = Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_Date_Time_NOW</fullName>
        <field>Submitted_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Submitted Date/Time = NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Reference_Solution_on_new_versio</fullName>
        <field>Reference_Solution__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Reference Solution on new versio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Summary_field</fullName>
        <description>2137 - pushes the first 1000 xters from Description field into Summary</description>
        <field>Summary</field>
        <formula>LEFT( Description__c ,1000)</formula>
        <name>Update Summary field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_as_New</fullName>
        <action>PublishAsNew</action>
        <label>Publish as New</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>New Article version uncheck Reference Solution</fullName>
        <actions>
            <name>Approved_Date_Time_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reference_Solution_Status_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Submitted_Date_Time_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Reference_Solution_on_new_versio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2140. on a new version of an article the Reference Solution checkbox should be set to flase</description>
        <formula>NOT( ISPICKVAL(PublishStatus , &quot;Publicerad&quot;)) &amp;&amp; $UserRole.DeveloperName &lt;&gt; &quot;CPM&quot; &amp;&amp;  ((Reference_Solution__c = TRUE &amp;&amp;  Submitted_for_Approval__c = FALSE &amp;&amp;  $User.Id &lt;&gt;  Reference_Solution_Approver__r.Id ) ||  (Submitted_for_Approval__c = TRUE &amp;&amp; Reference_Solution__c = FALSE &amp;&amp;  $User.Id &lt;&gt;  Reference_Solution_Approver__r.Id ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update article summary</fullName>
        <actions>
            <name>Update_Summary_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2137 - this workflow pushed the first 1000 xters into the Summary field in the Article to aid with searching</description>
        <formula>ISBLANK(  Summary  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
