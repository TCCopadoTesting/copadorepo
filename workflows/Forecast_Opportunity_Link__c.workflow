<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Include_in_Commit_Outcome</fullName>
        <field>Include_in_Outcome_Commit__c</field>
        <literalValue>1</literalValue>
        <name>Include in Commit Outcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Include_in_Commit_Outcome</fullName>
        <field>Include_in_Outcome_Commit__c</field>
        <literalValue>0</literalValue>
        <name>Reset Include in Commit Outcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Reset Include in Outcome Commit</fullName>
        <actions>
            <name>Reset_Include_in_Commit_Outcome</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resets the checkbox when the committed week is not equal to the related week</description>
        <formula>Committed_Week__c   &lt;&gt; Weekly_Forecast_Item__r.Name</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Include in Outcome Commit</fullName>
        <actions>
            <name>Include_in_Commit_Outcome</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Committed_Week__c =  Weekly_Forecast_Item__r.Name</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
