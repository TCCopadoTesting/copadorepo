<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>System_User_as_Contact_Owner</fullName>
        <description>Case 3094 - This field update will update System User as Contact Owner</description>
        <field>OwnerId</field>
        <lookupValue>system.adminuser@teliacompany.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>System User as Contact Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Role_on_Contact_to_Annan</fullName>
        <description>SALEF - 1005</description>
        <field>Role__c</field>
        <literalValue>Annan</literalValue>
        <name>Update Role on Contact to Annan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contact Owner System User</fullName>
        <actions>
            <name>System_User_as_Contact_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 3094 : Contacts do not belong to any user or user group now, System User will be Contact Owner</description>
        <formula>AND( RecordType.DeveloperName = &apos;Customer_Contact&apos;,OwnerId &lt;&gt; &apos;00524000007CzT2&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update%3ARole on Contact</fullName>
        <actions>
            <name>Update_Role_on_Contact_to_Annan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF - 1005, update role on contact to Annan when the role is not put in, in case of system generated contacts</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Customer_Contact&apos;,  ISPICKVAL(Role__c,&apos;&apos;), Contact_from_Lead__c = True )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
