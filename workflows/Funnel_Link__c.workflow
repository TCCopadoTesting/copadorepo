<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Funnel_Remove_FUL_from_Funnel_aft_ClDt</fullName>
        <description>SALEF 335</description>
        <field>Included_in_Funnel__c</field>
        <literalValue>0</literalValue>
        <name>Funnel:Remove FUL from Funnel aft ClDt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Funnel%3AUpdate FUL for Close Date</fullName>
        <active>false</active>
        <description>SALEF - 335</description>
        <formula>NOT(ISBLANK(Opportunity_Close_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Funnel_Remove_FUL_from_Funnel_aft_ClDt</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Funnel_Link__c.Opportunity_Close_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
