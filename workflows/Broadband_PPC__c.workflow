<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PPC_Update_BB_Record_Type</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>RecordTypeId</field>
        <lookupValue>Default</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PPC Update BB Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Add_on</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Add_on__c</field>
        <formula>Offering_service_name__r.Add_on__c</formula>
        <name>Update Add-on</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Add_on_SLA</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Add_on__c</field>
        <formula>Offering_service_addon__r.Add_on__c</formula>
        <name>Update Add-on</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Add_on_options</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Add_on_options__c</field>
        <formula>Offering_service_name__r.Add_on_options__c</formula>
        <name>Update Add-on options</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Add_on_options_SLA</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Add_on_options__c</field>
        <formula>Offering_service_addon__r.Add_on_options__c</formula>
        <name>Update Add-on options</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Availability</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Availability__c</field>
        <formula>Offering_service_name__r.Availability__c</formula>
        <name>Update Availability</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Availability_Add_On</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Availability__c</field>
        <formula>Offering_service_addon__r.Availability__c</formula>
        <name>Update Availability</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Main_Offering</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Main_offering__c</field>
        <name>Update Main Offering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Main_Offering_SLA</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Main_offering__c</field>
        <name>Update Main Offering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering</fullName>
        <field>Offering__c</field>
        <name>Update Offering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering_SLA</fullName>
        <field>Offering__c</field>
        <name>Update Offering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering_Service_name_Add_On</fullName>
        <field>Specific_Offering_Service__c</field>
        <formula>Offering_service_name__r.Specific_Offering_Service__c</formula>
        <name>Update Offering/Service name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering_Service_name_SLA</fullName>
        <field>Specific_Offering_Service__c</field>
        <formula>Offering_service_name__r.Specific_Offering_Service__c</formula>
        <name>Update Offering/Service name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering_service_detailed_def_SLA</fullName>
        <field>Offering_service_volume__c</field>
        <formula>Offering_service_name__r.Offering_service_volume__c</formula>
        <name>Update Offering/service detailed definit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering_service_detailed_definit</fullName>
        <field>Offering_service_volume__c</field>
        <formula>Offering_service_name__r.Offering_service_volume__c</formula>
        <name>Update Offering/service detailed definit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Portfolio</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Main_Portfolio__c</field>
        <name>Update Portfolio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Portfolio_SLA</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Main_Portfolio__c</field>
        <name>Update Portfolio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Resolution_time</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Resolution_time__c</field>
        <name>Update Resolution time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Resolution_time_Add_On</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Resolution_time__c</field>
        <name>Update Resolution time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SLA</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>SLA__c</field>
        <name>Update SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SLA_Add_On</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>SLA__c</field>
        <name>Update SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_time</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Service_time__c</field>
        <name>Update Service time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_time_Add_On</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Service_time__c</field>
        <name>Update Service time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PPC Set BB Record Type</fullName>
        <actions>
            <name>PPC_Update_BB_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>PPC Workflow Rule to Update Record Type.</description>
        <formula>True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PPC Set Info Add-On Layout</fullName>
        <actions>
            <name>Update_Availability_Add_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Offering_Service_name_Add_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Offering_service_detailed_definit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Resolution_time_Add_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SLA_Add_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_time_Add_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>PPC Workflow Rule to Update Record Type.</description>
        <formula>RecordType.DeveloperName = &apos;Add_on&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PPC Set Info Product Layout</fullName>
        <actions>
            <name>Update_Add_on</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Add_on_options</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Availability</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Resolution_time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>PPC Workflow Rule to Update Record Type.</description>
        <formula>RecordType.DeveloperName = &apos;Product&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PPC Set Info SLA Layout</fullName>
        <actions>
            <name>Update_Add_on_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Add_on_options_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Offering_Service_name_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Offering_service_detailed_def_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>PPC Workflow Rule to Update Record Type.</description>
        <formula>RecordType.DeveloperName = &apos;SLA&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
