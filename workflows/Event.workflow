<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Event_Completed</fullName>
        <field>Event_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Event Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Type_Update</fullName>
        <field>Event_Type__c</field>
        <formula>TEXT(Type)</formula>
        <name>Event Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Event_complete</fullName>
        <field>Event_Complete__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Event complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Execute_trigger_c</fullName>
        <field>Execute_trigger__c</field>
        <literalValue>1</literalValue>
        <name>Update Execute_trigger__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>48 - Event Completed</fullName>
        <active>true</active>
        <description>When an event&apos;s due date has passed this workflow checks the checkbox Event_Complete__c that executes a trigger.</description>
        <formula>(ISPICKVAL(Type,&quot;Fysiskt möte&quot;) || ISPICKVAL(Type,&quot;Web/video&quot;) || ISPICKVAL(Type,&quot;Telefonmöte&quot;)) &amp;&amp; Event_Complete__c= False &amp;&amp; ActivityDateTime &gt; NOW()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Event_Completed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Event.EndDateTime</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Check %27Execute  trigger%27 on Event</fullName>
        <active>true</active>
        <description>This workflow is used to check the checkbox &apos;Execute_trigger__c&apos; on Event if there is any Contact or Lead in Related To- Name field. So as to execte the EventTrigger to check the sambesök.</description>
        <formula>WhoId  &lt;&gt; NULL</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Execute_trigger_c</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Event.LastModifiedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cygate Kundbesök Past</fullName>
        <actions>
            <name>Event_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>when a planned event is moved to past or created in past , this workflow check event complete.</description>
        <formula>(ISPICKVAL(Type,&quot;Fysiskt möte&quot;)|| ISPICKVAL(Type,&quot;Web/video&quot;)) &amp;&amp;  Event_Complete__c= False &amp;&amp;  ActivityDateTime  &lt;=  NOW()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fill Event Type</fullName>
        <actions>
            <name>Event_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>populating custom field &quot;Event Type&quot; on Activities with value from standard event type field as its not possible to reference standard vent type field in formulas .</description>
        <formula>ISNEW()  ||  ISCHANGED(Type)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Kundbesök Past</fullName>
        <actions>
            <name>Event_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>when a planned event is moved to past or created in past , this workflow check event complete.</description>
        <formula>(ISPICKVAL(Type,&quot;Fysiskt möte&quot;)|| ISPICKVAL(Type,&quot;Web/video&quot;)  ||ISPICKVAL(Type,&quot;Telefonmöte&quot;) ) &amp;&amp;  Event_Complete__c= False &amp;&amp;  ActivityDateTime  &lt;=  NOW()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Event complete</fullName>
        <actions>
            <name>Uncheck_Event_complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>unchecks event complete if completed Cygate event kundbesök  is moved to future</description>
        <formula>(ISPICKVAL(Type,&apos;Fysiskt möte&apos;) || ISPICKVAL(Type,&apos;Web/video&apos;))&amp;&amp;  Event_Complete__c= True &amp;&amp;  ActivityDateTime  &gt;  NOW()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Event_Completed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Event.EndDateTime</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
