<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Execute_trigger_c</fullName>
        <field>Execute_trigger__c</field>
        <literalValue>1</literalValue>
        <name>Check Execute_trigger__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Type_to_Mail</fullName>
        <description>Automatically imported mails from outlook gets the type &apos;Email&apos;, but Telia uses &apos;Mail&apos; as the correct picklist type value</description>
        <field>Type</field>
        <literalValue>Mail</literalValue>
        <name>Set Type to Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_comment_in_custom_comment_field</fullName>
        <description>Sets a value in custom field Comment on Task from the standard field Comment. 
SAEN-1959</description>
        <field>Comment_on_Task__c</field>
        <formula>IF(LEN(Description)&gt;255,LEFT(Description,252)&amp;&quot;...&quot;,Description)</formula>
        <name>Set comment in custom comment field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Status</fullName>
        <description>Set Task Status = &apos;Stängd&apos;</description>
        <field>Status</field>
        <literalValue>Stängd</literalValue>
        <name>Task Status = &apos;Stängd&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Status_Open</fullName>
        <description>Set Task Status = Öppen</description>
        <field>Status</field>
        <literalValue>Öppen</literalValue>
        <name>Task Status = Öppen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Type</fullName>
        <description>To update custom field Task Type so that it can be used in other workflow.</description>
        <field>Task_Type__c</field>
        <formula>TEXT(Type)</formula>
        <name>Update Task Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check %27Execute  trigger%27 on Task</fullName>
        <active>true</active>
        <description>This workflow is used to check the checkbox &apos;Execute_trigger__c&apos; on Task if there is any Contact or Lead in Related To- Name field. So as to execte the TaskTrigger to check the sambesök.</description>
        <formula>WhoId  &lt;&gt; NULL</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Execute_trigger_c</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.LastModifiedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Comment populated on new task</fullName>
        <actions>
            <name>Set_comment_in_custom_comment_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This WF rule will push the value in standard Comment field to a new field called Comment on Task. This is because standard Comment field cant be used as a column in a actiity related list. SAEN-1959</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fill Task Type</fullName>
        <actions>
            <name>Update_Task_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates custom field &quot;Cygate Task Type&quot; on Activities with value from standard Task type field as its not possible to reference standard Task type field in formulas .</description>
        <formula>ISNEW()  ||  ISCHANGED( Type )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Email Update Type</fullName>
        <active>true</active>
        <description>This workflow rule will update the Type from Email (set as default by Salesforce) to Mail (which is the valid picklist value when selecting manually). It uses a time based trigger set to run 0h after created date, to ensure that related flows are run.</description>
        <formula>ISPICKVAL(Type, &apos;Email&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Type_to_Mail</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.CreatedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Past Cygate Task</fullName>
        <active>true</active>
        <description>When a task is created in past or when future task is moved to past, this workflow sets the value of Task Status as &apos;Stängd&apos;.</description>
        <formula>(RecordType.DeveloperName  = &quot;Cygate_Tasks&quot; &amp;&amp;   (ISPICKVAL( Type , &quot;Fysiskt&quot;) || ISPICKVAL( Type , &quot;Telefon&quot;) || ISPICKVAL( Type , &quot;Web&quot;) || ISPICKVAL( Type , &quot;Mail&quot;)|| ISPICKVAL( Type , &quot;Annat&quot;)) &amp;&amp; ISPICKVAL( Status , &quot;Öppen&quot;)  &amp;&amp; ActivityDate &lt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Task_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.LastModifiedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Task Status to %27Öppen%27</fullName>
        <active>true</active>
        <description>When completed/closed Task&apos;s due date is changed to future date this workflow change the Status to &apos;Öppen&apos;.</description>
        <formula>(RecordType.DeveloperName  = &quot;Cygate_Tasks&quot; &amp;&amp;   (ISPICKVAL( Type , &quot;Fysiskt&quot;) || ISPICKVAL( Type , &quot;Web&quot;) || ISPICKVAL( Type , &quot;Telefon&quot;) ||  ISPICKVAL( Type ,&quot;Mail&quot; )|| ISPICKVAL( Type , &quot;Annat&quot;))  &amp;&amp;   ISPICKVAL( Status , &quot;Stängd&quot;) &amp;&amp; ActivityDate &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Task_Status_Open</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.LastModifiedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Task Status</fullName>
        <active>true</active>
        <description>Sets the Task Status as &apos;Stängd&apos; when the Task&apos;s Due Date has passed which then executes a trigger.</description>
        <formula>(RecordType.DeveloperName  = &quot;Cygate_Tasks&quot; &amp;&amp; (ISPICKVAL( Type , &quot;Fysiskt&quot;) || ISPICKVAL( Type , &quot;Web&quot;) ||  ISPICKVAL(Type , &quot;Telefon&quot;)  ||  ISPICKVAL(Type , &quot;Mail&quot;)   ||  ISPICKVAL(Type , &quot;Annat&quot;)) &amp;&amp;   ISPICKVAL( Status , &quot;Öppen&quot;)  &amp;&amp; ActivityDate &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Task_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
