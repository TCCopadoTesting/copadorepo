<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cygate_Lead_Email_to_Owner</fullName>
        <description>Cygate Lead - Email to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@cygate.se</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Cygate_Lead_Emails/Cygate_Lead_to_lead_owner</template>
    </alerts>
    <alerts>
        <fullName>Cygate_Lead_Email_to_Sales_Manager</fullName>
        <description>Cygate Lead - Email to Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Owner_s_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply-salesforce@cygate.se</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Cygate_Lead_Emails/Cygate_Lead_to_sales_manager</template>
    </alerts>
    <alerts>
        <fullName>Lead_Email_Reminder_1</fullName>
        <description>Lead Email Reminder 1</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Owner_Changed/Lead_Reminder_Notification_1</template>
    </alerts>
    <alerts>
        <fullName>Lead_Email_Reminder_2</fullName>
        <description>Lead Email Reminder 2</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Owner_s_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Owner_Changed/Lead_Reminder_Notification_2</template>
    </alerts>
    <alerts>
        <fullName>Notify_TAM_queue_members_when_a_lead_is_assigned</fullName>
        <description>Notify &apos;TAM&apos; queue members when a lead is assigned.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Owner_Changed/Notification_To_TAM_Queue_Users</template>
    </alerts>
    <alerts>
        <fullName>Notify_Volume_Sales_queue_members_when_a_lead_is_assigned</fullName>
        <description>Notify &apos;Volume Sales&apos; queue members when a lead is assigned.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Owner_Changed/Notification_To_Volume_Sales_Queue_Users</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Status_To_F_rkvalificera</fullName>
        <field>Status</field>
        <literalValue>Förkvalificera</literalValue>
        <name>Change Status To Förkvalificera</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Cygate_Lead_Escalated</fullName>
        <description>2350: used for reporting to show when a cygate lead has been escalated to the sales manager</description>
        <field>Cygate_Lead_Escalated_to_Sales_Manager__c</field>
        <literalValue>1</literalValue>
        <name>Check Cygate Lead Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cygate_Lead_source_Unknown_Set_Owner</fullName>
        <description>SALEF-639 - Assign leads to queue - (Cygate Lead source Unknown).</description>
        <field>OwnerId</field>
        <lookupValue>Cygate_Lead_source_Unknown</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Cygate Lead source Unknown- Set Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_to_Today</fullName>
        <description>Sets the close date to today. Then the user could change it manually.</description>
        <field>Close_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Close Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_by_Lead_Owner_to_NOW</fullName>
        <description>2062:</description>
        <field>Last_Modified_by_Lead_Owner__c</field>
        <formula>NOW()</formula>
        <name>Set Last Modified by Lead Owner to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_by_Lead_Owner_to_blank</fullName>
        <description>2062:</description>
        <field>Last_Modified_by_Lead_Owner__c</field>
        <name>Set Last Modified by Lead Owner to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Is_Converting_To_True</fullName>
        <field>Lead_Is_Converting__c</field>
        <literalValue>1</literalValue>
        <name>Set Lead Is Converting To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Owner_s_Manager_s_Email</fullName>
        <field>Lead_Owner_s_Manager_s_Email__c</field>
        <formula>IF( BEGINS( OwnerId ,&quot;005&quot; ),Owner:User.Manager.Email,&quot;&quot; )</formula>
        <name>Set Lead Owner&apos;s Manager&apos;s Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_1_Date_Time</fullName>
        <field>Telia_Reminder_1_Date__c</field>
        <formula>IF( 
MOD(Today()-DATE(1900,1,7),7)=6, 
DATETIMEVALUE(TEXT(today() +2)&amp;&quot; 05:00:00&quot;), /*if rules triggers on Saturday, set reminder date/time to Monday 6 am*/ 

IF( 
AND(MOD(Today()-DATE(1900,1,7),7)=5, 
HOUR(TIMEVALUE(now())) &gt;11 ), 
DATETIMEVALUE(TEXT(today()+3)&amp;&quot; 05:00:00&quot;), /*if rules triggers on Friday from 12 pm to 12 am, set reminder to Monday 6 am*/ 

IF( 
AND( 
OR( 
MOD(Today()-DATE(1900,1,7),7)=1, 
MOD(Today()-DATE(1900,1,7),7)=2, 
MOD(Today()-DATE(1900,1,7),7)=3, 
MOD(Today()-DATE(1900,1,7),7)=4 
), 

HOUR(TIMEVALUE(now())) &gt;11 
), 
DATETIMEVALUE(TEXT(today() +1)&amp;&quot; 05:00:00&quot;), /*if rules triggers on Monday-Thurday from 12 pm cet to 12 am cet, set reminder date/time to next day 6am*/ 
IF( 
AND( 
OR( 
MOD(Today()-DATE(1900,1,7),7)=1, 
MOD(Today()-DATE(1900,1,7),7)=2, 
MOD(Today()-DATE(1900,1,7),7)=3, 
MOD(Today()-DATE(1900,1,7),7)=4, 
MOD(Today()-DATE(1900,1,7),7)=5 
), 

HOUR(TIMEVALUE(now())) &lt;11 ), 
DATETIMEVALUE(TEXT(today())&amp;&quot; 15:00:00&quot;), /*if rules triggers on Monday-Thursday from 12 am cet to 12 pm cet, set reminder date/time to same day at 4 pm*/ 
DATETIMEVALUE(TEXT(today() +1)&amp;&quot; 05:00:00&quot;)) /*if rules triggers on Sunday, set reminder time to Monday 6 am*/ 
) 
))</formula>
        <name>Set Reminder 1 Date -Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_2_Date_Time</fullName>
        <field>Telia_Reminder_2_Date__c</field>
        <formula>IF( 
MOD(Today()-DATE(1900,1,7),7)=6, 
DATETIMEVALUE(TEXT(today() +2)&amp;&quot; 05:00:00&quot;), /*if rules triggers on Saturday, set reminder date/time to Monday 6 am*/

IF( 
MOD(Today()-DATE(1900,1,7),7)=5,
DATETIMEVALUE(TEXT(today()+3)&amp;&quot; 05:00:00&quot;), /*if rules triggers on Friday, set reminder time Monday 6 am*/

IF( 
AND( 
OR( 
MOD(Today()-DATE(1900,1,7),7)=1, 
MOD(Today()-DATE(1900,1,7),7)=2, 
MOD(Today()-DATE(1900,1,7),7)=3, 
MOD(Today()-DATE(1900,1,7),7)=4 
), 
HOUR(TIMEVALUE(now())) &gt;11 ), 
DATETIMEVALUE(TEXT(today() +1)&amp;&quot; 15:00:00&quot;), /*if rules triggers on Monday-Thurday from 12 pm cet to 12 am cet, set reminder date/time to next day 4pm*/
IF( 
AND( 
OR( 
MOD(Today()-DATE(1900,1,7),7)=1, 
MOD(Today()-DATE(1900,1,7),7)=2, 
MOD(Today()-DATE(1900,1,7),7)=3, 
MOD(Today()-DATE(1900,1,7),7)=4,
MOD(Today()-DATE(1900,1,7),7)=5
), 

HOUR(TIMEVALUE(now())) &lt;11), 
DATETIMEVALUE(TEXT(today()+1)&amp;&quot; 05:00:00&quot;), /*if rules triggers on Monday-Thursday from 12 am cet to 12 pm cet, set reminder date/time to next day day at 6 am*/

DATETIMEVALUE(TEXT(today() +1)&amp;&quot; 05:00:00&quot;)) /*if rules triggers on Sunday, set reminder time to Monday 6 am*/
) 
))</formula>
        <name>Set Reminder 2 Date -Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_Date_Lead_Owner_High</fullName>
        <field>Reminder_Date_Owner_High__c</field>
        <formula>IF( (HOUR(TIMEVALUE(Now()))&gt;16 || HOUR(TIMEVALUE(now()))&lt;7) , 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
5, DATETIMEVALUE(TEXT(today()+3)&amp;&quot; 07:00:00&quot;), 
6, DATETIMEVALUE(TEXT(today()+2)&amp;&quot; 07:00:00&quot;), 
DATETIMEVALUE(TEXT( today()+1)&amp;&quot; 07:00:00&quot;) 
), 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
5, now()+3, 
6, now()+2, 
now()+1))</formula>
        <name>Set Reminder Date - Lead Owner- High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_Date_Lead_Owner_Low_Lead</fullName>
        <field>Reminder_Date_Owner_Low__c</field>
        <formula>IF( (HOUR(TIMEVALUE(Now()))&gt;16 || HOUR(TIMEVALUE(now()))&lt;7) , 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
4, DATETIMEVALUE(TEXT(today()+4)&amp;&quot; 07:00:00&quot;), 
5, DATETIMEVALUE(TEXT(today()+3)&amp;&quot; 07:00:00&quot;), 
DATETIMEVALUE(TEXT( today()+2)&amp;&quot; 07:00:00&quot;) 
), 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
4, now()+4, 
5, now()+3, 
now()+2))</formula>
        <name>Set Reminder Date - Lead Owner- Low	Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_Date_Manager_High</fullName>
        <field>Reminder_Date_Manager_High__c</field>
        <formula>IF( (HOUR(TIMEVALUE(Now()))&gt;16 || HOUR(TIMEVALUE(now()))&lt;7) , 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
4, DATETIMEVALUE(TEXT(today()+4)&amp;&quot; 07:00:00&quot;), 
5, DATETIMEVALUE(TEXT(today()+3)&amp;&quot; 07:00:00&quot;), 
DATETIMEVALUE(TEXT( today()+2)&amp;&quot; 07:00:00&quot;) 
), 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
4, now()+4, 
5, now()+3, 
now()+2))</formula>
        <name>Set Reminder Date - Manager - High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_Date_Sales_Manager_Low</fullName>
        <field>Reminder_date_Manager_Low__c</field>
        <formula>IF( (HOUR(TIMEVALUE(Now()))&gt;16 || HOUR(TIMEVALUE(now()))&lt;7) , 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
2, DATETIMEVALUE(TEXT(today()+6)&amp;&quot; 07:00:00&quot;), 
3, DATETIMEVALUE(TEXT(today()+5)&amp;&quot; 07:00:00&quot;), 
DATETIMEVALUE(TEXT( today()+4)&amp;&quot; 07:00:00&quot;) 
), 
CASE( 
MOD( Today() - DATE( 1900, 1, 7 ), 7 ), 
2, now()+6, 
3, now()+5, 
now()+4))</formula>
        <name>Set Reminder Date -Sales Manager- Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_Post_after_4_hrs</fullName>
        <field>Reminder_Post_After_4_hrs__c</field>
        <literalValue>1</literalValue>
        <name>Set Reminder Post after 4 hrs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reminder_Post_after_6_hrs</fullName>
        <field>Reminder_Post_After_6_hrs__c</field>
        <literalValue>1</literalValue>
        <name>Set Reminder Post after 6 hrs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Response_Time</fullName>
        <field>Response_Time__c</field>
        <formula>(Time_to_first_contact__c -  CreatedDate)*24</formula>
        <name>Set Response Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Time_To_Now</fullName>
        <description>Store current time.</description>
        <field>Time_Owner_Changed__c</field>
        <formula>NOW()</formula>
        <name>Set Time To Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_value_in_Expected_Opp_Type_Text</fullName>
        <description>SAEN-2175 Sets the value in formula field Lead.Expected_Opportunitu_Type2__c to the field Lead.Expected_Opportunity_Type_Text__c.</description>
        <field>Expected_Opportunity_Type_Text__c</field>
        <formula>Expected_Opportunity_Type2__c</formula>
        <name>Set value in Expected Opp Type Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Cygate_Lead_Escalated</fullName>
        <description>2350: unchecks the Cygate Lead Escalated checkbox once the lead is not in Kvalifiera status</description>
        <field>Cygate_Lead_Escalated_to_Sales_Manager__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Cygate Lead Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Company_With_Account_Lookup</fullName>
        <description>Copying account name in company name.</description>
        <field>Company</field>
        <formula>Account__r.Name</formula>
        <name>Update Company With Account Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Change lead status when assigned to lead agents</fullName>
        <actions>
            <name>Change_Status_To_F_rkvalificera</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-1962 , When a lead agent assign a lead to themselves change status to  &quot;Förkvalificera&quot;.</description>
        <formula>ISPICKVAL( Status , &quot;Nytt&quot;)      &amp;&amp; Owner:User.Profile.Name = &quot;Telia - Leads Agent&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cygate - Lead Owner High workflow</fullName>
        <actions>
            <name>Set_Reminder_Date_Lead_Owner_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2350. send an email reminder to the Lead Owner after 1 day if the lead has no activity
SALEF - 1112 - Add new Source value &apos;Cygate - Telia Leadscentralen&apos;</description>
        <formula>RecordType.DeveloperName =&quot;Cygate_Lead&quot; &amp;&amp;  Lead_Convert__c=FALSE &amp;&amp;  ISPICKVAL( Status , &quot;Kvalificera&quot;) &amp;&amp; (LastActivityDate &lt;  DATEVALUE(Time_Owner_Changed__c ) || ISBLANK(LastActivityDate)) &amp;&amp; (ISPICKVAL( LeadSource ,&quot;Cygate Web – Contact Request&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Event&quot;)||ISPICKVAL( LeadSource ,&quot;Externt Event&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Intranät&quot;)||ISPICKVAL( LeadSource ,&quot;Tillverkare&quot;)||ISPICKVAL( LeadSource ,&quot;Telemarketing&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Internal Leads&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Web - Other&quot;)||ISPICKVAL( LeadSource ,&quot;Telia Intranet&quot;) ||ISPICKVAL( LeadSource ,&quot;Cygate - Telia Leadscentralen&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cygate_Lead_Email_to_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Reminder_Date_Owner_High__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cygate - Lead Owner Low%2Fmedium workflow</fullName>
        <actions>
            <name>Set_Reminder_Date_Lead_Owner_Low_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2350. send an email reminder to the Lead Owner after 2 day if the lead has no activity</description>
        <formula>RecordType.DeveloperName =&quot;Cygate_Lead&quot; &amp;&amp;
Lead_Convert__c=FALSE &amp;&amp; 
ISPICKVAL( Status , &quot;Kvalificera&quot;) &amp;&amp; (LastActivityDate &lt; DATEVALUE(Time_Owner_Changed__c ) || ISBLANK(LastActivityDate))  &amp;&amp; ISPICKVAL( LeadSource ,&quot;Cygate Web – High Score&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cygate_Lead_Email_to_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Reminder_Date_Owner_Low__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cygate - Sales Manager High workflow</fullName>
        <actions>
            <name>Set_Reminder_Date_Manager_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2350. send an email reminder to the Sales Manager after 1 day if the lead has no activity
SALEF - 1112 - Add new Source value &apos;Cygate - Telia Leadscentralen&apos;</description>
        <formula>RecordType.DeveloperName =&quot;Cygate_Lead&quot; &amp;&amp;  Lead_Convert__c=FALSE &amp;&amp;  ISPICKVAL( Status , &quot;Kvalificera&quot;) &amp;&amp; (LastActivityDate &lt;  DATEVALUE(Time_Owner_Changed__c ) || ISBLANK(LastActivityDate)) &amp;&amp; (ISPICKVAL( LeadSource ,&quot;Cygate Web – Contact Request&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Event&quot;)||ISPICKVAL( LeadSource ,&quot;Externt Event&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Intranät&quot;)||ISPICKVAL( LeadSource ,&quot;Tillverkare&quot;)||ISPICKVAL( LeadSource ,&quot;Telemarketing&quot;)||ISPICKVAL( LeadSource ,&quot;Cygate Internal Leads&quot;)||ISPICKVAL( LeadSource ,&quot;Telia Intranet&quot;) ||ISPICKVAL( LeadSource ,&quot;Cygate - Telia Leadscentralen&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cygate_Lead_Email_to_Sales_Manager</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Check_Cygate_Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Reminder_Date_Manager_High__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cygate - Sales Manager Low%2Fmedium workflow</fullName>
        <actions>
            <name>Set_Reminder_Date_Sales_Manager_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2350. send an email reminder to the Sales Manager after 4 day if the lead has no activity</description>
        <formula>RecordType.DeveloperName =&quot;Cygate_Lead&quot; &amp;&amp;
Lead_Convert__c=FALSE &amp;&amp; 
ISPICKVAL( Status , &quot;Kvalificera&quot;) &amp;&amp; (LastActivityDate &lt; DATEVALUE(Time_Owner_Changed__c ) || ISBLANK(LastActivityDate))  &amp;&amp; ISPICKVAL( LeadSource ,&quot;Cygate Web – High Score&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cygate_Lead_Email_to_Sales_Manager</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Check_Cygate_Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Reminder_date_Manager_Low__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cygate Lead source Unknown - Assign Leads</fullName>
        <actions>
            <name>Cygate_Lead_source_Unknown_Set_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF-639 - This Workflow rule will identify Leads which are of Record Type as Cygate Lead, which are not yet converted and the source value is other than the regular value.</description>
        <formula>RecordType.DeveloperName =&quot;Cygate_Lead&quot; &amp;&amp; Lead_Convert__c=FALSE &amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate Web – Contact Request&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate Web – Contact Request&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Externt Event&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate Intranät&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Tillverkare&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Telemarketing&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate Web - High Score&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate Internal Leads&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate Web - Other&quot;))&amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Telia Intranet&quot;) &amp;&amp; NOT(ISPICKVAL( LeadSource ,&quot;Cygate - Telia Leadscentralen&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cygate%3A Uncheck Cygate Lead Escalated</fullName>
        <actions>
            <name>Uncheck_Cygate_Lead_Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygate Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Cygate_Lead_Escalated_to_Sales_Manager__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Kvalificera</value>
        </criteriaItems>
        <description>2350: sets Cygate Lead Escalated to FALSE when the lead is not Kvalifiera or</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expected Opportunity Type Formula Updated</fullName>
        <actions>
            <name>Set_value_in_Expected_Opp_Type_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2175 When formula field Lead.Expected_Opportunity_Type2__c is updated, this WF rule will push the value to the field Lead.Expected_Opportunity_Type_Text__c which then is mapped to Opportunity.Expected_Opportunity_Type__c.</description>
        <formula>(ISNEW() &amp;&amp; NOT(ISBLANK(Expected_Opportunity_Type2__c))) || ISCHANGED(Expected_Opportunity_Type2__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>First contact since creation</fullName>
        <actions>
            <name>Set_Response_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Time to first contact is populated through a process builder flow, and when that is populated this workflow calculates the difference in hours between creation and first contact (task/event completed) and sets that value in Reponse Time field.</description>
        <formula>NOT(ISBLANK( Time_to_first_contact__c )) ||  ISCHANGED(Time_to_first_contact__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Kickstart Reminder Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Telia Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Volume Sales CLM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Reminder_Post_after_4_hrs</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Reminder_Post_after_6_hrs</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Reminder Notifications</fullName>
        <actions>
            <name>Set_Reminder_1_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Reminder_2_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2062: Used to send reminder emails to lead owner and their manager.</description>
        <formula>ISBLANK( Last_Modified_by_Lead_Owner__c ) &amp;&amp; RecordType.DeveloperName =&quot;Standard_Telia_Lead&quot; &amp;&amp; BEGINS(OwnerId, &quot;005&quot;) &amp;&amp; ISBLANK(Lead_Conversion_Time__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Email_Reminder_1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Telia_Reminder_1_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Email_Reminder_2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Telia_Reminder_2_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate Company Name</fullName>
        <actions>
            <name>Update_Company_With_Account_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-1954, To populate company on lead when account lookup is filled in.</description>
        <formula>NOT( ISBLANK( Account__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Last Modified by Lead Owner</fullName>
        <actions>
            <name>Set_Last_Modified_by_Lead_Owner_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2062: Sets the Last Modified by Lead Owner field to now so that we can see when the lead owner has edited the lead or not. This field is used in the Lead Reminder Notifications workflow.</description>
        <formula>OwnerId =  $User.Id &amp;&amp; RecordType.DeveloperName =&quot;Standard_Telia_Lead&quot; &amp;&amp; BEGINS(OwnerId, &quot;005&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Lead Close Date</fullName>
        <actions>
            <name>Set_Close_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Konverterat,Avfärdat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Close_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cygate Lead</value>
        </criteriaItems>
        <description>When a lead is either status=avfärdat or converted, this workflow sets the so called end date to todays date. That date is used for reporting purposes etc.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Lead Is Converting</fullName>
        <actions>
            <name>Set_Lead_Is_Converting_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SAEN-1867, To set the value of field &apos;Lead_Is_Converting__c&apos; during lead conversion because sometimes standard &apos;IsConverted&apos; is not possible to fetch.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Store Time When Owner Is Changed</fullName>
        <actions>
            <name>Set_Last_Modified_by_Lead_Owner_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lead_Owner_s_Manager_s_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Time_To_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-1908, Used to store the time when the Lead owner is changed. Useful when trying to send reminder notification etc to user. 
2062: also sets the Lead Owner&apos;s Manager&apos;s Email field
Used for Cygate in 2350 for the lead notifications as well.</description>
        <formula>ISCHANGED(OwnerId) || ( RecordType.DeveloperName =&quot;Cygate_Lead&quot; &amp;&amp; ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
