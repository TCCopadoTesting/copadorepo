<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_Cygate_AM_re_Cygate_Stop_Valid_Till</fullName>
        <ccEmails>elena.king@capgemini.com</ccEmails>
        <ccEmails>elena.king@teliasonera.com</ccEmails>
        <description>Send Email to Cygate AM re Cygate Stop Valid Till</description>
        <protected>false</protected>
        <recipients>
            <field>Cygate_Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cygate_Stop_Valid_Till_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Bearbetas_Ej</fullName>
        <field>Bearbetas_ej__c</field>
        <literalValue>0</literalValue>
        <name>Clear Bearbetas Ej</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Other_Reason</fullName>
        <field>Other_Reason__c</field>
        <name>Clear Other Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Reason</fullName>
        <field>Reason__c</field>
        <name>Clear Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Valid_To</fullName>
        <field>Valid_To__c</field>
        <name>Clear Valid To</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cygate_Sales_Team_Value_Update</fullName>
        <field>Cygate_Sales_Team_value__c</field>
        <formula>CASE( Cygate_Account_Manager__r.UserRole.Name , 

&quot;CG:Sälj Mellan Väst Säljare&quot;,&apos;Sälj Mellan Väst&apos;, 
&quot;CG:Sälj Mellan Väst Försäljningschef&quot;,&quot;Sälj Mellan Väst&quot;, 
&quot;CG:Sälj Mellan Öst Säljare&quot;,&quot;Sälj Mellan Öst&quot;, 
&quot;CG:Sälj Mellan Öst Försäljningschef&quot;,&quot;Sälj Mellan Öst&quot;, 
&quot;CG:Sälj Norr Säljare&quot;,&quot;Norr&quot;, 
&quot;CG:Sälj Norr Försäljningschef&quot;,&quot;Norr&quot;, 
&quot;CG:Sälj Syd Säljare&quot;,&quot;Syd&quot;, 
&quot;CG:Sälj Syd Försäljningschef&quot;,&quot;Syd&quot;, 
&quot;CG:Sälj Öst Enterprise Säljare&quot;,&quot;Öst Enterprise&quot;, 
&quot;CG:Sälj Öst Enterprise Försäljningschef&quot;,&quot;Öst Enterprise&quot;, 
&quot;CG:Sälj Öst Offentlig Säljare&quot;,&quot;Öst Offentlig&quot;, 
&quot;CG:Sälj Öst Offentlig Försäljningschef&quot;,&quot;Öst Offentlig&quot;, 
&quot;CG:Sälj Öst Midmarket Säljare&quot;,&quot;Öst Midmarket&quot;, 
&quot;CG:Sälj Öst Midmarket Försäljningschef&quot;,&quot;Öst Midmarket&quot;, 
&quot;CG:Infrastruktur Specialistsälj Chef&quot;,&quot;Infrastruktur&quot;, 
&quot;CG:Infrastruktur Specialistsälj&quot;,&quot;Infrastruktur&quot;, 
&quot;CG:Hybrid IT &amp; Cloud Solution Sales Chef&quot;,&quot;Specialistsälj Cloud&quot;, 
&quot;CG:Hybrid IT &amp; Cloud Solution Sales&quot;,&apos;Specialistsälj Cloud&apos;, 
&quot;CG:Säljsupport Chef&quot;,&quot;Säljsupport&quot;, 
&quot;CG:Säljsupport&quot;,&quot;Säljsupport&quot;, 
&quot;CG:Business Area&quot;,&quot;Business Area&quot;, 
&quot;CG:Bid Management&quot;,&quot;BID Management&quot;, 
&quot;CG:Business Area Chatter Plus&quot;,&quot;Business Area&quot;, 
&quot;CG:Business Area Chef&quot;,&quot;Business Area&quot;, 
&quot;CG:Cygate Försäljningsdirektör&quot;,&quot;Cygate Försäljningsdirektör&quot;, 
&quot;CG:Ekonomi&quot;,&quot;Ekonomi&quot;, 
&quot;CG:Financial Controller&quot;,&quot;Ekonomi&quot;, 
&quot;CG:Head of Sales - Region Öst&quot;,&quot;Head of Sales - Region Öst&quot;, 
&quot;CG:Inköp Avtal&quot;,&quot;Inköp Avtal&quot;, 
&quot;CG:Konsult Chef Norr&quot;,&quot;Konsult Chef&quot;, 
&quot;CG:Konsult Chef Syd&quot;,&quot;Konsult Chef&quot;,
&quot;CG:Service Presale &amp; Business Support Chef&quot;,&quot;Business Support/Service Presales&quot;,
&quot;CG:Business Support&quot;,&quot;Business Support/Service Presales&quot;,
&quot;CG:Service Presale&quot;,&quot;Business Support/Service Presales&quot;, 
&quot;CG:Konsult Direktor Syd&quot;,&quot;Konsult Direktor&quot;, 
&quot;CG:Kundreskontra&quot;,&quot;Kundreskontra&quot;, 
&quot;CG:Marknad&quot;,&quot;Marknad&quot;, 
&quot;CG:Marknad Chef&quot;,&quot;Marknad&quot;, 
&quot;CG:Paketerade tjänster&quot;,&quot;Paketerade tjänster&quot;, 
&quot;CG:Process&quot;,&quot;Process&quot;, 
&quot;CG:Service Manager Norr&quot;,&quot;Service Manager&quot;, 
&quot;CG:Telefoni Specialistsälj&quot;,&quot;Telefoni Specialistsälj&quot;, 
&quot;CG:Telefoni Specialistsälj chef&quot;,&quot;Telefoni Specialistsälj&quot;, 

&quot;Övriga&quot;)</formula>
        <name>Cygate Sales Team Value Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Get_Parent_KKnr</fullName>
        <field>Parents_KKnr__c</field>
        <formula>Parent_KKnr__c</formula>
        <name>Get 	Parent_KKnr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Has_Child_Perform</fullName>
        <description>Update field &quot;Has_Child_No_Of_Days_Performed__c&quot; is update to current date only while on creation</description>
        <field>Has_Child_No_Of_Days_Performed__c</field>
        <formula>TODAY()</formula>
        <name>Has Child Perform</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Kondkonto_Kundkonto_Klass_2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Kundkonto_S_k_2</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Kondkonto --&gt; Kundkonto Klass 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Kondkonto_Kundkonto_Klass_3_OR_Blank</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Sales_Aggregation_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Kondkonto --&gt; Kundkonto Klass 3 OR Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Kundkonto_Kundkonto_Klass_1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Kundkonto_S_k_1</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Kundkonto = Kundkonto Klass 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Large_Organisation_Large_Org_Klass_1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Large_Organisation_S_k_1</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Large Organisation --&gt; Large Org Klass 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Large_Organisation_Large_Org_Klass_2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Large_Organisation_S_k_2</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Large Organisation --&gt; Large Org Klass 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Large_Organisation_Large_Org_Klass_3</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Large_Sales_Organisation_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Large Organisation --&gt; Large Org Klass 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_C2B_org_nr</fullName>
        <field>C2B_Org_nr__c</field>
        <formula>&quot;16&quot;&amp;Org_Nr__c</formula>
        <name>Set C2B org nr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Acc_Owner_Email</fullName>
        <description>Fetched from user record, for Conga purposes.</description>
        <field>Conga_Cygate_Account_Manager_Email__c</field>
        <formula>Cygate_Account_Manager__r.Email</formula>
        <name>Set Conga Acc Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Acc_Owner_Mobile</fullName>
        <field>Conga_Cygate_Account_Manager_Mobile__c</field>
        <formula>Cygate_Account_Manager__r.MobilePhone</formula>
        <name>Set Conga Acc Owner Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Acc_Owner_Phone</fullName>
        <field>Conga_Cygate_Account_Manager_Phone__c</field>
        <formula>Cygate_Account_Manager__r.Phone</formula>
        <name>Set Conga Acc Owner Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Credit_Check_Performed_to_TODAY</fullName>
        <field>Credit_Check_Performed__c</field>
        <formula>TODAY()</formula>
        <name>Set Credit Check Performed to TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cygate_Account_Plan_to_Yes</fullName>
        <field>Cygate_Should_Have_Account_Plan__c</field>
        <literalValue>Ja</literalValue>
        <name>Set Cygate Account_Plan to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_From_Date_to_blank</fullName>
        <field>From_Date__c</field>
        <name>Set &apos;From Date&apos; to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quarantine_Dates_Filled_to_true</fullName>
        <field>Quarantine_Dates_Filled__c</field>
        <literalValue>1</literalValue>
        <name>Set &apos;Quarantine Dates Filled&apos; to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_To_Date_to_blank</fullName>
        <field>To_Date__c</field>
        <name>Set &apos;To Date&apos; to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_date_value_of_opt_out_change</fullName>
        <description>SAEN-2122 populates a field that will hold information of when the opt-out field changed + what the priorvalue was.</description>
        <field>Marketing_opt_out_changed_date_value__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; &quot; &amp; 

IF(AND(INCLUDES(PRIORVALUE(Marketing_Mails_Opt_Out__c), &apos;According to agreement&apos;),INCLUDES(PRIORVALUE(Marketing_Mails_Opt_Out__c), &apos;Upon Client request&apos;)), &quot;Enligt avtal; Samtycke återtagit av kund&quot;, &quot;&quot;) &amp; 

IF(AND(INCLUDES(PRIORVALUE(Marketing_Mails_Opt_Out__c), &apos;According to agreement&apos;),NOT(INCLUDES(PRIORVALUE(Marketing_Mails_Opt_Out__c), &apos;Upon Client request&apos;))), &quot;Enligt avtal&quot;, &quot;&quot;) &amp;

IF(AND(NOT(INCLUDES(PRIORVALUE(Marketing_Mails_Opt_Out__c), &apos;According to agreement&apos;)),INCLUDES(PRIORVALUE(Marketing_Mails_Opt_Out__c), &apos;Upon Client request&apos;)), &quot;Samtycke återtagit av kund&quot;, &quot;&quot;)</formula>
        <name>Set date/value of opt-out change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Chid_Account_Wanted_Customer</fullName>
        <field>Child_Account_Wanted_Customer__c</field>
        <literalValue>1</literalValue>
        <name>Update Chid Account Wanted Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Owner_Changed_Field</fullName>
        <field>Is_Owner_Changed__c</field>
        <formula>today()</formula>
        <name>Update Is Owner Changed Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SNI1_Avdelning_Text</fullName>
        <field>SNI1AvdelningTEXT__c</field>
        <formula>CASE( SNI1_Avdelning__c , &quot;A&quot;,&quot;JORDBRUK, SKOGSBRUK OCH FISKE&quot; , 
&quot;B&quot;, &quot;UTVINNING AV MINERAL&quot;,
&quot;C&quot;,&quot;TILLVERKNING&quot;,
&quot;D&quot;,&quot;FÖRSÖRJNING AV EL, GAS, VÄRME OCH KYLA&quot;,
&quot;E&quot;,&quot;VATTENFÖRSÖRJNING; AVLOPPSRENING, AVFALLSHANTERING OCH SANERING&quot;, &quot;F&quot;,&quot;BYGGVERKSAMHET&quot;, 
&quot;G&quot;,&quot;HANDEL; REPARATION AV MOTORFORDON OCH MOTORCYKLAR&quot;,
&quot;H&quot;,&quot;TRANSPORT OCH MAGASINERING&quot;,
&quot;I&quot;,&quot;HOTELL- OCH RESTAURANGVERKSAMHET&quot;,
&quot;J&quot;,&quot;INFORMATIONS- OCH KOMMUNIKATIONSVERKSAMHET&quot;,
&quot;K&quot;,&quot;FINANS- OCH FÖRSÄKRINGSVERKSAMHET&quot;,
&quot;L&quot;,&quot;FASTIGHETSVERKSAMHET&quot;,
&quot;M&quot;,&quot;VERKSAMHET INOM JURIDIK, EKONOMI, VETENSKAP OCH TEKNIK&quot;,
&quot;N&quot;,&quot;UTHYRNING, FASTIGHETSSERVICE, RESETJÄNSTER OCH ANDRA STÖDTJÄNSTER&quot;,
&quot;O&quot;,&quot;OFFENTLIG FÖRVALTNING OCH FÖRSVAR; OBLIGATORISK SOCIALFÖRSÄKRING&quot;,
&quot;P&quot;,&quot;UTBILDNING&quot;,
&quot;Q&quot;,&quot;VÅRD OCH OMSORG; SOCIALA TJÄNSTER&quot;,
&quot;R&quot;,&quot;KULTUR, NÖJE OCH FRITID&quot;,
&quot;S&quot;,&quot;ANNAN SERVICEVERKSAMHET&quot;,
&quot;T&quot;,&quot;FÖRVÄRVSARBETE I HUSHÅLL; HUSHÅLLENS PRODUKTION AV DIVERSE VAROR OCH TJÄNSTER FÖR EGET BRUK&quot;,
&quot;U&quot;,&quot;VERKSAMHET VID INTERNATIONELLA ORGANISATIONER, UTLÄNDSKA AMBASSADER O.D.&quot;,&quot; &quot;)</formula>
        <name>Update SNI1 Avdelning Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SNI2_Avdelning_Text</fullName>
        <field>SNI2AvdelningTEXT__c</field>
        <formula>CASE( SNI2_Avdelning__c , &quot;A&quot;,&quot;JORDBRUK, SKOGSBRUK OCH FISKE&quot; , 
&quot;B&quot;, &quot;UTVINNING AV MINERAL&quot;, 
&quot;C&quot;,&quot;TILLVERKNING&quot;, 
&quot;D&quot;,&quot;FÖRSÖRJNING AV EL, GAS, VÄRME OCH KYLA&quot;, 
&quot;E&quot;,&quot;VATTENFÖRSÖRJNING; AVLOPPSRENING, AVFALLSHANTERING OCH SANERING&quot;, &quot;F&quot;,&quot;BYGGVERKSAMHET&quot;, 
&quot;G&quot;,&quot;HANDEL; REPARATION AV MOTORFORDON OCH MOTORCYKLAR&quot;, 
&quot;H&quot;,&quot;TRANSPORT OCH MAGASINERING&quot;, 
&quot;I&quot;,&quot;HOTELL- OCH RESTAURANGVERKSAMHET&quot;, 
&quot;J&quot;,&quot;INFORMATIONS- OCH KOMMUNIKATIONSVERKSAMHET&quot;, 
&quot;K&quot;,&quot;FINANS- OCH FÖRSÄKRINGSVERKSAMHET&quot;, 
&quot;L&quot;,&quot;FASTIGHETSVERKSAMHET&quot;, 
&quot;M&quot;,&quot;VERKSAMHET INOM JURIDIK, EKONOMI, VETENSKAP OCH TEKNIK&quot;, 
&quot;N&quot;,&quot;UTHYRNING, FASTIGHETSSERVICE, RESETJÄNSTER OCH ANDRA STÖDTJÄNSTER&quot;, 
&quot;O&quot;,&quot;OFFENTLIG FÖRVALTNING OCH FÖRSVAR; OBLIGATORISK SOCIALFÖRSÄKRING&quot;, 
&quot;P&quot;,&quot;UTBILDNING&quot;, 
&quot;Q&quot;,&quot;VÅRD OCH OMSORG; SOCIALA TJÄNSTER&quot;, 
&quot;R&quot;,&quot;KULTUR, NÖJE OCH FRITID&quot;, 
&quot;S&quot;,&quot;ANNAN SERVICEVERKSAMHET&quot;, 
&quot;T&quot;,&quot;FÖRVÄRVSARBETE I HUSHÅLL; HUSHÅLLENS PRODUKTION AV DIVERSE VAROR OCH TJÄNSTER FÖR EGET BRUK&quot;, 
&quot;U&quot;,&quot;VERKSAMHET VID INTERNATIONELLA ORGANISATIONER, UTLÄNDSKA AMBASSADER O.D.&quot;,&quot; &quot;)</formula>
        <name>Update SNI2 Avdelning Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SNI3_Avdelning_Text</fullName>
        <field>SNI3AvdelningTEXT__c</field>
        <formula>CASE( SNI3_Avdelning__c , &quot;A&quot;,&quot;JORDBRUK, SKOGSBRUK OCH FISKE&quot; , 
&quot;B&quot;, &quot;UTVINNING AV MINERAL&quot;, 
&quot;C&quot;,&quot;TILLVERKNING&quot;, 
&quot;D&quot;,&quot;FÖRSÖRJNING AV EL, GAS, VÄRME OCH KYLA&quot;, 
&quot;E&quot;,&quot;VATTENFÖRSÖRJNING; AVLOPPSRENING, AVFALLSHANTERING OCH SANERING&quot;, &quot;F&quot;,&quot;BYGGVERKSAMHET&quot;, 
&quot;G&quot;,&quot;HANDEL; REPARATION AV MOTORFORDON OCH MOTORCYKLAR&quot;, 
&quot;H&quot;,&quot;TRANSPORT OCH MAGASINERING&quot;, 
&quot;I&quot;,&quot;HOTELL- OCH RESTAURANGVERKSAMHET&quot;, 
&quot;J&quot;,&quot;INFORMATIONS- OCH KOMMUNIKATIONSVERKSAMHET&quot;, 
&quot;K&quot;,&quot;FINANS- OCH FÖRSÄKRINGSVERKSAMHET&quot;, 
&quot;L&quot;,&quot;FASTIGHETSVERKSAMHET&quot;, 
&quot;M&quot;,&quot;VERKSAMHET INOM JURIDIK, EKONOMI, VETENSKAP OCH TEKNIK&quot;, 
&quot;N&quot;,&quot;UTHYRNING, FASTIGHETSSERVICE, RESETJÄNSTER OCH ANDRA STÖDTJÄNSTER&quot;, 
&quot;O&quot;,&quot;OFFENTLIG FÖRVALTNING OCH FÖRSVAR; OBLIGATORISK SOCIALFÖRSÄKRING&quot;, 
&quot;P&quot;,&quot;UTBILDNING&quot;, 
&quot;Q&quot;,&quot;VÅRD OCH OMSORG; SOCIALA TJÄNSTER&quot;, 
&quot;R&quot;,&quot;KULTUR, NÖJE OCH FRITID&quot;, 
&quot;S&quot;,&quot;ANNAN SERVICEVERKSAMHET&quot;, 
&quot;T&quot;,&quot;FÖRVÄRVSARBETE I HUSHÅLL; HUSHÅLLENS PRODUKTION AV DIVERSE VAROR OCH TJÄNSTER FÖR EGET BRUK&quot;, 
&quot;U&quot;,&quot;VERKSAMHET VID INTERNATIONELLA ORGANISATIONER, UTLÄNDSKA AMBASSADER O.D.&quot;,&quot; &quot;)</formula>
        <name>Update SNI3 Avdelning Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SNI4_Avdelning_Text</fullName>
        <field>SNI4AvdelningTEXT__c</field>
        <formula>CASE( SNI4_Avdelning__c , &quot;A&quot;,&quot;JORDBRUK, SKOGSBRUK OCH FISKE&quot; , 
&quot;B&quot;, &quot;UTVINNING AV MINERAL&quot;, 
&quot;C&quot;,&quot;TILLVERKNING&quot;, 
&quot;D&quot;,&quot;FÖRSÖRJNING AV EL, GAS, VÄRME OCH KYLA&quot;, 
&quot;E&quot;,&quot;VATTENFÖRSÖRJNING; AVLOPPSRENING, AVFALLSHANTERING OCH SANERING&quot;, &quot;F&quot;,&quot;BYGGVERKSAMHET&quot;, 
&quot;G&quot;,&quot;HANDEL; REPARATION AV MOTORFORDON OCH MOTORCYKLAR&quot;, 
&quot;H&quot;,&quot;TRANSPORT OCH MAGASINERING&quot;, 
&quot;I&quot;,&quot;HOTELL- OCH RESTAURANGVERKSAMHET&quot;, 
&quot;J&quot;,&quot;INFORMATIONS- OCH KOMMUNIKATIONSVERKSAMHET&quot;, 
&quot;K&quot;,&quot;FINANS- OCH FÖRSÄKRINGSVERKSAMHET&quot;, 
&quot;L&quot;,&quot;FASTIGHETSVERKSAMHET&quot;, 
&quot;M&quot;,&quot;VERKSAMHET INOM JURIDIK, EKONOMI, VETENSKAP OCH TEKNIK&quot;, 
&quot;N&quot;,&quot;UTHYRNING, FASTIGHETSSERVICE, RESETJÄNSTER OCH ANDRA STÖDTJÄNSTER&quot;, 
&quot;O&quot;,&quot;OFFENTLIG FÖRVALTNING OCH FÖRSVAR; OBLIGATORISK SOCIALFÖRSÄKRING&quot;, 
&quot;P&quot;,&quot;UTBILDNING&quot;, 
&quot;Q&quot;,&quot;VÅRD OCH OMSORG; SOCIALA TJÄNSTER&quot;, 
&quot;R&quot;,&quot;KULTUR, NÖJE OCH FRITID&quot;, 
&quot;S&quot;,&quot;ANNAN SERVICEVERKSAMHET&quot;, 
&quot;T&quot;,&quot;FÖRVÄRVSARBETE I HUSHÅLL; HUSHÅLLENS PRODUKTION AV DIVERSE VAROR OCH TJÄNSTER FÖR EGET BRUK&quot;, 
&quot;U&quot;,&quot;VERKSAMHET VID INTERNATIONELLA ORGANISATIONER, UTLÄNDSKA AMBASSADER O.D.&quot;,&quot; &quot;)</formula>
        <name>Update SNI4 Avdelning Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SNI5_Avdelning_Text</fullName>
        <field>SNI5AvdelningTEXT__c</field>
        <formula>CASE( SNI5_Avdelning__c , &quot;A&quot;,&quot;JORDBRUK, SKOGSBRUK OCH FISKE&quot; , 
&quot;B&quot;, &quot;UTVINNING AV MINERAL&quot;, 
&quot;C&quot;,&quot;TILLVERKNING&quot;, 
&quot;D&quot;,&quot;FÖRSÖRJNING AV EL, GAS, VÄRME OCH KYLA&quot;, 
&quot;E&quot;,&quot;VATTENFÖRSÖRJNING; AVLOPPSRENING, AVFALLSHANTERING OCH SANERING&quot;, &quot;F&quot;,&quot;BYGGVERKSAMHET&quot;, 
&quot;G&quot;,&quot;HANDEL; REPARATION AV MOTORFORDON OCH MOTORCYKLAR&quot;, 
&quot;H&quot;,&quot;TRANSPORT OCH MAGASINERING&quot;, 
&quot;I&quot;,&quot;HOTELL- OCH RESTAURANGVERKSAMHET&quot;, 
&quot;J&quot;,&quot;INFORMATIONS- OCH KOMMUNIKATIONSVERKSAMHET&quot;, 
&quot;K&quot;,&quot;FINANS- OCH FÖRSÄKRINGSVERKSAMHET&quot;, 
&quot;L&quot;,&quot;FASTIGHETSVERKSAMHET&quot;, 
&quot;M&quot;,&quot;VERKSAMHET INOM JURIDIK, EKONOMI, VETENSKAP OCH TEKNIK&quot;, 
&quot;N&quot;,&quot;UTHYRNING, FASTIGHETSSERVICE, RESETJÄNSTER OCH ANDRA STÖDTJÄNSTER&quot;, 
&quot;O&quot;,&quot;OFFENTLIG FÖRVALTNING OCH FÖRSVAR; OBLIGATORISK SOCIALFÖRSÄKRING&quot;, 
&quot;P&quot;,&quot;UTBILDNING&quot;, 
&quot;Q&quot;,&quot;VÅRD OCH OMSORG; SOCIALA TJÄNSTER&quot;, 
&quot;R&quot;,&quot;KULTUR, NÖJE OCH FRITID&quot;, 
&quot;S&quot;,&quot;ANNAN SERVICEVERKSAMHET&quot;, 
&quot;T&quot;,&quot;FÖRVÄRVSARBETE I HUSHÅLL; HUSHÅLLENS PRODUKTION AV DIVERSE VAROR OCH TJÄNSTER FÖR EGET BRUK&quot;, 
&quot;U&quot;,&quot;VERKSAMHET VID INTERNATIONELLA ORGANISATIONER, UTLÄNDSKA AMBASSADER O.D.&quot;,&quot; &quot;)</formula>
        <name>Update SNI5 Avdelning Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Cygate_Account_Manager_change</fullName>
        <apiVersion>36.0</apiVersion>
        <description>Sent to GESB when Cygate Account Manager changes</description>
        <endpointUrl>https://google.com</endpointUrl>
        <fields>Cygate_Account_ID__c</fields>
        <fields>Cygate_Account_Manager_Employee_ID__c</fields>
        <fields>Cygate_Account_Type__c</fields>
        <fields>Cygate_Has_Sub_Account__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration.user@teliasonera.com</integrationUser>
        <name>Cygate Account Manager change</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>One_Time_Cygate_Account_Manager_change</fullName>
        <apiVersion>38.0</apiVersion>
        <description>Sent to GESB when Cygate Account Manager changes</description>
        <endpointUrl>https://google.com</endpointUrl>
        <fields>Cygate_Account_ID__c</fields>
        <fields>Cygate_Account_Manager_Employee_ID__c</fields>
        <fields>Cygate_Account_Type__c</fields>
        <fields>Cygate_Has_Sub_Account__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>shruti.singh@teliacompany.com</integrationUser>
        <name>One Time Cygate Account Manager change</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Account Quarantine Dates</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.From_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.To_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>SAEN-1826</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Quarantine_Dates_Filled_to_true</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.To_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Set_From_Date_to_blank</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_To_Date_to_blank</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.To_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Bearbetas Ej</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Bearbetas_ej__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Valid_To__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>SME/SOHO Organisation</value>
        </criteriaItems>
        <description>For SME accounts, when the checkbox Bearbetas ej is checked: Sets Valid To, Reason and Other Reason to blank values. It also unchecks the Bearbetas ej checkbox.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_Bearbetas_Ej</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Clear_Other_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Clear_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Clear_Valid_To</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Valid_To__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cygate Account Manager Set New Account</fullName>
        <actions>
            <name>Cygate_Account_Manager_change</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send OBM to GESB when an account is inserted and the Cygate Account Manager-lookup field is set</description>
        <formula>NOT(ISBLANK(Cygate_Account_Manager__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Cygate Account Manager change</fullName>
        <actions>
            <name>Cygate_Account_Manager_change</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send OBM to GESB when the account is Cygate Account Manager-lookup field is changed on an Account.</description>
        <formula>ISCHANGED( Cygate_Account_Manager__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cygate Account Manager change One Time</fullName>
        <actions>
            <name>One_Time_Cygate_Account_Manager_change</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>One time Send OBM to GESB when the account is Cygate Account Manager-lookup field is changed on an Account.</description>
        <formula>Cygate_Account_Manager__c != null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cygate Sales Team Value</fullName>
        <actions>
            <name>Cygate_Sales_Team_Value_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Cygate_Sales_Team__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This Workflow fetch the value from field &quot;Cygate Sales Team&quot; as to be used in Pardot.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Get Parent KKnr</fullName>
        <actions>
            <name>Get_Parent_KKnr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This will fetch the Parent_KKnr__c(Formula Field) into the Text Field to use into the Sharing Rule criteria</description>
        <formula>NOT(ISBLANK( Parent_KKnr__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Kondkonto --%3E Kundkonto Klass 1</fullName>
        <actions>
            <name>Kundkonto_Kundkonto_Klass_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
            <value>Klass 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Kundkonto,Kundkonto Säk 1,Kundkonto Säk 2</value>
        </criteriaItems>
        <description>If the account Security Category is changed to class 1, update the record type (if kundkonto) to be Kundkonto Klass 1</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Kondkonto --%3E Kundkonto Klass 2</fullName>
        <actions>
            <name>Kondkonto_Kundkonto_Klass_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
            <value>Klass 2,Klass 2B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Kundkonto,Kundkonto Säk 1,Kundkonto Säk 2</value>
        </criteriaItems>
        <description>If the account Security Category is changed to class 2, update the record type (if kundkonto) to be Kundkonto Klass 2.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Kondkonto --%3E Kundkonto Klass 3 OR Blank</fullName>
        <actions>
            <name>Kondkonto_Kundkonto_Klass_3_OR_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
            <value>Klass 3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Kundkonto,Kundkonto Säk 1,Kundkonto Säk 2</value>
        </criteriaItems>
        <description>If the account Security Category is changed to class 3 or a blank value, update the record type (if kundkonto) to be Kundkonto</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Large Organisation --%3E Large Org Klass 1</fullName>
        <actions>
            <name>Large_Organisation_Large_Org_Klass_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
            <value>Klass 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Large Organisation Säk 1,Large Organisation Säk 2,Large Organisation</value>
        </criteriaItems>
        <description>If the account Security Category is changed to class 1, update the record type (if Large Organisation) to be Large Organisation class 1.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Large Organisation --%3E Large Org Klass 2</fullName>
        <actions>
            <name>Large_Organisation_Large_Org_Klass_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
            <value>Klass 2,Klass 2B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Large Organisation Säk 1,Large Organisation Säk 2,Large Organisation</value>
        </criteriaItems>
        <description>If the account Security Category is changed to class 2, update the record type (if Large Organisation) to be Large Organisation class 2.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Large Organisation --%3E Large Org Klass 3 OR Blank</fullName>
        <actions>
            <name>Large_Organisation_Large_Org_Klass_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
            <value>Klass 3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Security_Category__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Large Organisation Säk 1,Large Organisation Säk 2,Large Organisation</value>
        </criteriaItems>
        <description>If the account Security Category is changed to class 3 or a blank value, update the record type (if Large Organisation) to be Large Organisation.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing opt-out field changed</fullName>
        <actions>
            <name>Set_date_value_of_opt_out_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2122 when marketing opt-out is changed this workflow rule will populate date+priorvalue in a new field since field tracking related list isn&apos;t visible in lighting.</description>
        <formula>ISCHANGED(Marketing_Mails_Opt_Out__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Account Has Child</fullName>
        <actions>
            <name>Has_Child_Perform</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This Workflow will update the field Has_Child_No_Of_Days_Performed__c to current date only on creation of a KKNR account with out a ORG account tagged.</description>
        <formula>AND  (OR(RecordTypeId = &apos;012240000008l9O&apos;,  RecordTypeId = &apos;012240000002Nde&apos;,  RecordTypeId = &apos;012240000002Ndf&apos;),  ISBLANK(Parent.ParentId ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Plan Yes If Cygate A%2FKey-Customer</fullName>
        <actions>
            <name>Set_Cygate_Account_Plan_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Cygate_Should_Have_Account_Plan__c</field>
            <operation>notEqual</operation>
            <value>Ja</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Cygate_Customer_Category__c</field>
            <operation>equals</operation>
            <value>A-kund,Key-kund</value>
        </criteriaItems>
        <description>If the Accounts Cygate Customer Category is set to Key/A-customer, the picklist field Cygate Account Plan should always be set to Yes.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set C2B OrgNr</fullName>
        <actions>
            <name>Set_C2B_org_nr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Org_Nr__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create a 10-digit organisation number</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Chid Account Wanted Customer field</fullName>
        <actions>
            <name>Update_Chid_Account_Wanted_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( RecordType.DeveloperName = &apos;Sales_Organisation_Account&apos;, RecordType.DeveloperName = &apos;Large_Organisation_S_k_2&apos;, RecordType.DeveloperName = &apos;Large_Organisation_S_k_1&apos;, RecordType.DeveloperName = &apos;Large_Sales_Organisation_Account&apos;), Parent.Name != null, OR( Parent.RecordType.DeveloperName = &apos;Sales_Aggregation_Account&apos;, Parent.RecordType.DeveloperName = &apos;Kundkonto_S_k_1&apos;, Parent.RecordType.DeveloperName = &apos;Kundkonto_S_k_2&apos; ), Child_Account_Wanted_Customer__c = false, Parent.New_Market_Development__c = true )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Conga Info on Account</fullName>
        <actions>
            <name>Set_Conga_Acc_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Conga_Acc_Owner_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Conga_Acc_Owner_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(Cygate_Account_Manager__r.Id))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Credit Check Performed</fullName>
        <actions>
            <name>Set_Credit_Check_Performed_to_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates the Credit Check Performed field when the Recommended Credit Amount is updated</description>
        <formula>ISCHANGED( Recommended_Credit_Amount__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Is Account Owner Changed</fullName>
        <actions>
            <name>Update_Is_Owner_Changed_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF-368 - Captures the date when KKNR owner change happens</description>
        <formula>AND(AND(NOT(ISNEW()),ISCHANGED(OwnerId) , NOT(ISBLANK(OwnerId)), PRIORVALUE(OwnerId) != OwnerId), OR(RecordTypeId = &apos;012240000008l9O&apos; ,RecordTypeId = &apos;012240000002Nde&apos;, RecordTypeId = &apos;012240000002Ndf&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SNI1 Avdelning Text</fullName>
        <actions>
            <name>Update_SNI1_Avdelning_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populating  SNI1 Avdelning Text with  text values depending on values from SNI1 Avdelning.</description>
        <formula>ISNEW() ||  ISCHANGED( SNI1_Avdelning__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SNI2 Avdelning Text</fullName>
        <actions>
            <name>Update_SNI2_Avdelning_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populating  SNI2 Avdelning Text with  text values depending on values from SNI2 Avdelning.</description>
        <formula>ISNEW() ||  ISCHANGED( SNI2_Avdelning__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SNI3 Avdelning Text</fullName>
        <actions>
            <name>Update_SNI3_Avdelning_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populating  SNI3 Avdelning Text with  text values depending on values from SNI3 Avdelning.</description>
        <formula>ISNEW() ||  ISCHANGED( SNI3_Avdelning__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SNI4 Avdelning Text</fullName>
        <actions>
            <name>Update_SNI4_Avdelning_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populating  SNI4 Avdelning Text with  text values depending on values from SNI4 Avdelning.</description>
        <formula>ISNEW() ||  ISCHANGED( SNI4_Avdelning__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SNI5 Avdelning Text</fullName>
        <actions>
            <name>Update_SNI5_Avdelning_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populating  SNI5 Avdelning Text with  text values depending on values from SNI5 Avdelning.</description>
        <formula>ISNEW() ||  ISCHANGED( SNI5_Avdelning__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
