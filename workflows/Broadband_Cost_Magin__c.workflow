<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PPC_Update_BBCM_ID_Unique</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Unique_ID__c</field>
        <formula>Name</formula>
        <name>PPC Update BBCM ID Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PPC Set BBCM ID Unique</fullName>
        <actions>
            <name>PPC_Update_BBCM_ID_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Broadband_Cost_Magin__c.Unique_ID__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>PPC Workflow Rule to Update Broadband Cost Margin Records.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
