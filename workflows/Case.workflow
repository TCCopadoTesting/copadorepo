<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AM_2nd_line_Support_Team_to_receive_notification_on_Case_Resolver</fullName>
        <description>AM 2nd line Support Team to receive notification on Case Resolver</description>
        <protected>false</protected>
        <recipients>
            <recipient>X2nd_Line_Support_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AM_Notification_on_Case_Resolver</template>
    </alerts>
    <alerts>
        <fullName>AM_2nd_line_Support_Team_to_receive_notification_on_ownership</fullName>
        <description>AM 2nd line Support Team to receive notification on ownership</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>shweta.z.agarwal@teliacompany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AM_Notification_on_Ownership</template>
    </alerts>
    <alerts>
        <fullName>Enterprise_Case_Submitter_Notification</fullName>
        <description>Enterprise Case: Submitter Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Enterprise_Case_Notification_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Enterprise_Case_Emails/Notification_to_case_submitter</template>
    </alerts>
    <alerts>
        <fullName>High_priority_Cases_Notification</fullName>
        <description>High priority Cases Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>george.huth@teliacompany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shweta.z.agarwal@teliacompany.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply-salesforce@teliacompany.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/High_priority_Cases</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_2nd_Line_after_Approval</fullName>
        <description>Notification to 2nd Line after Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Resolver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Changes_for_Case_Approved</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_2nd_Line_after_Rejection</fullName>
        <description>Notification to 2nd Line after Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Resolver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Changes_for_Case_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Sales_Coordinator</fullName>
        <description>Notification to Sales Coordinator</description>
        <protected>false</protected>
        <recipients>
            <field>Named_Sales_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Mail_to_Sales_Coordinator_on_Case</template>
    </alerts>
    <alerts>
        <fullName>Notify_Case_Resolver_of_Expected_Delivery_Date</fullName>
        <description>Notify Case Resolver of Expected Delivery Date</description>
        <protected>false</protected>
        <recipients>
            <field>Resolver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AM_Notification_for_Expected_Delivery_Date</template>
    </alerts>
    <alerts>
        <fullName>Notify_on_Case_Type_Task_Force</fullName>
        <description>Notify on Case Type - Task Force</description>
        <protected>false</protected>
        <recipients>
            <recipient>maria.x.karlsson@teliasonera.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>monika.taylan@teliacompany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_on_Case_Type_Task_Force</template>
    </alerts>
    <fieldUpdates>
        <fullName>AM_Update_Resolved_Date</fullName>
        <field>Resolved_Date__c</field>
        <formula>NOW()</formula>
        <name>AM:Update Resolved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Opportunity_Owner_Role_on_Case</fullName>
        <description>2216: sets the Account/Opportunity Owner Role on Case to power the case sharing rules for Enterprise</description>
        <field>Account_Opportunity_Owner_Role__c</field>
        <formula>IF(
NOT(ISBLANK( Opportunity__c )),   Opportunity__r.Seller__r.Sales_Team_Name__c  , 

IF( NOT(ISBLANK( AccountId )),  Account.Owner.UserRole.Name , &quot;&quot;))</formula>
        <name>Account/Opportunity Owner Role on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Security_Catgegory_on_Case</fullName>
        <description>2216: sets the Account Security Catgegory on Case for the sharing rules</description>
        <field>Account_Security_Category__c</field>
        <formula>IF(ISBLANK( AccountId ), &quot;&quot;,  TEXT(Account.Security_Category__c ))</formula>
        <name>Account Security Catgegory on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Stage_Update_Approved</fullName>
        <field>Approval__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Stage Update - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Stage_Update_Rejected</fullName>
        <field>Approval__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Stage Update - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case_Automatically</fullName>
        <description>Closes Cases Automatically when its 15 days after the Case has been put to Resolved Status</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Case Automatically</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enterprise_Case_Set_Queue_Name</fullName>
        <description>2186. puts the queue name into the Queue Name field</description>
        <field>Queue_Name__c</field>
        <formula>Owner:Queue.QueueName</formula>
        <name>Enterprise Case: Set Queue Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Confidential_on_Case</fullName>
        <description>2216: updates the Opportunity Confidential? checkbix to TRUE</description>
        <field>Opportunity_Confidential__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Confidential on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OwnerChange</fullName>
        <field>Ownership_Changed__c</field>
        <formula>Now()</formula>
        <name>OwnerChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OwnerChange_Date1stLine</fullName>
        <description>For first line</description>
        <field>Ownership_Changed1__c</field>
        <formula>Now()</formula>
        <name>OwnerChange-Date1stLine</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ResponseTime</fullName>
        <field>StatusChanged__c</field>
        <formula>Now()</formula>
        <name>ResponseTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Group_to_Business_Design</fullName>
        <description>SAEN-2247 update field Case Group to &quot;Business Design&quot;</description>
        <field>Case_Group__c</field>
        <literalValue>Business Design</literalValue>
        <name>Set Case Group to Business Design</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Group_to_SAT</fullName>
        <description>SAEN-2247 update field Case Group to &quot;SAT&quot;</description>
        <field>Case_Group__c</field>
        <literalValue>SAT</literalValue>
        <name>Set Case Group to SAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Group_to_UC_Presales</fullName>
        <description>update field Case Group to UC Sales Specialist</description>
        <field>Case_Group__c</field>
        <literalValue>UC Presales</literalValue>
        <name>Set Case Group to UC Presales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Group_to_UC_Sales_Specialist</fullName>
        <description>update field Case Group to UC Sales Specialist</description>
        <field>Case_Group__c</field>
        <literalValue>UC Telefonilösningar</literalValue>
        <name>Set Case Group to UC Sales Specialist</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Group_to_Volume_Design</fullName>
        <description>SAEN-2247 update field Case Group to Volume Design</description>
        <field>Case_Group__c</field>
        <literalValue>Volume Design</literalValue>
        <name>Set Case Group to Volume Design</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Category_field_to_blank</fullName>
        <description>SAEN-2247 when chaning case group field the category field should be blank</description>
        <field>Category__c</field>
        <name>Set Category field to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusChange_1st_line</fullName>
        <description>for 1st line</description>
        <field>StatusChanged1__c</field>
        <formula>Now()</formula>
        <name>StatusChange-1st line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Enterprise_Case_Notification_Emai</fullName>
        <description>SAEN-2269 update notification email field when the formula field is changed.</description>
        <field>Enterprise_Case_Notification_Email__c</field>
        <formula>Enterprise_Case_Notification_Email_Formu__c</formula>
        <name>Update Enterprise Case Notification Emai</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AM %3A Notification to 2nd line support</fullName>
        <actions>
            <name>AM_2nd_line_Support_Team_to_receive_notification_on_ownership</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to 2nd Line Support on Case Ownership. Only valid for Telia Support Case record type.</description>
        <formula>OR(Owner:Queue.QueueName = &quot;SF Support 2nd Line - Cygate&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - OCC&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - Enterprise&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - SOHO&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - Leads&quot;,
Owner:Queue.QueueName = &quot;SF Support - Maintenance&quot; ) &amp;&amp; PRIORVALUE(OwnerId) &lt;&gt; OwnerId &amp;&amp; RecordType.DeveloperName= &quot;Telia_Company_Support&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AM%3AAutomatically Close Case on Resolution</fullName>
        <active>true</active>
        <description>To close cases in 2 weeks after it has been put on Resolved. Only valid for Telia Support Case record type.</description>
        <formula>AND( OR(Owner:Queue.QueueName = &quot;SF Support 2nd Line - Cygate&quot;,  Owner:Queue.QueueName = &quot;SF Support 2nd Line - OCC&quot;,  Owner:Queue.QueueName = &quot;SF Support 2nd Line - Enterprise&quot;,  Owner:Queue.QueueName = &quot;SF Support 2nd Line - SOHO&quot;, Owner:Queue.QueueName = &quot;SF Support 2nd Line - Leads&quot;), RecordType.DeveloperName=&quot;Telia_Company_Support&quot;, ISPICKVAL(Status,&quot;Resolved&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_Case_Automatically</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Resolved_Date__c</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>AM%3ANotification on Expected Delivery Date</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 3  OR 5 OR 6 OR 7 OR 9) AND 2 AND 4 AND 8</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SF Support 2nd Line - Cygate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Expected_Delivery_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SF Support 2nd Line - Enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Resolver__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SF Support 2nd Line - SOHO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SF Support 2nd Line - OCC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SF Support 2nd Line - Leads</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Telia Company Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>SF Support - Maintenance</value>
        </criteriaItems>
        <description>to send notification to AM Team Resolver when the Expected Delivery Date is approaching. Only valid for Telia Support Case record type.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Case_Resolver_of_Expected_Delivery_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Expected_Delivery_Date__c</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>AM%3AUpdate Resolved Date</fullName>
        <actions>
            <name>AM_Update_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.DeveloperName=&quot;Telia_Company_Support&quot;, ISPICKVAL(Status,&apos;Resolved&apos;), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case resolver Notification to 2nd line support</fullName>
        <actions>
            <name>AM_2nd_line_Support_Team_to_receive_notification_on_Case_Resolver</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to 2nd Line Support on Case Resolver. Only valid for Telia Support Case record type.</description>
        <formula>OR(Owner:Queue.QueueName = &quot;SF Support 2nd Line - Cygate&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - OCC&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - Enterprise&quot;,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - SOHO&quot;, Owner:Queue.QueueName = &quot;SF Support 2nd Line - Leads&quot;) &amp;&amp; ISCHANGED(Resolver__c)&amp;&amp; RecordType.DeveloperName=&quot;Telia_Company_Support&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CaseOwnerChange - Date</fullName>
        <actions>
            <name>OwnerChange</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates OwnerChange field with todays date. Only valid for Telia Support Case record type. Only valid for Telia Company Support case record type.</description>
        <formula>AND(  RecordType.DeveloperName=&quot;Telia_Company_Support&quot;, ISCHANGED(OwnerId),  OR(                          NOT(PRIORVALUE(OwnerId) = &apos;00G24000000R0dn&apos;),                           NOT(PRIORVALUE(OwnerId) = &apos;00G24000001IVDF&apos;),                          NOT(PRIORVALUE(OwnerId) = &apos;00G24000001SHn5&apos;),                           NOT(PRIORVALUE(OwnerId) = &apos;00G24000000gFdW&apos;),                          NOT(PRIORVALUE(OwnerId) = &apos;00G24000000gtAv&apos;) ),                      OR(                         Owner:Queue.QueueName = &quot;SF Support 2nd Line - Enterprise&quot;,                        Owner:Queue.QueueName = &quot;SF Support 2nd Line - Cygate&quot;, Owner:Queue.QueueName = &quot;SF Support 2nd Line - OCC&quot;, Owner:Queue.QueueName = &quot;SF Support 2nd Line - SOHO&quot;, Owner:Queue.QueueName = &quot;SF Support 2nd Line - Leads&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CaseOwnerChange1 - Date</fullName>
        <actions>
            <name>OwnerChange_Date1stLine</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates OwnerChange field with todays date.- 1st Line</description>
        <formula>RecordType.DeveloperName=&quot;Telia_Company_Support&quot; &amp;&amp; AND( ISCHANGED(OwnerId),  OR(NOT(PRIORVALUE(OwnerId) = &apos;00G24000000R0di&apos;), NOT(PRIORVALUE(OwnerId) = &apos;00G24000001IVDE&apos;), NOT(PRIORVALUE(OwnerId) = &apos;00G24000001SHn4&apos;), NOT(PRIORVALUE(OwnerId) = &apos;00G24000000gFdb&apos;) ), OR(  Owner:Queue.QueueName = &quot;SF Support 1st Line - Enterprise&quot;, Owner:Queue.QueueName = &quot;SF Support 1st Line - Cygate&quot;, Owner:Queue.QueueName = &quot;SF Support 1st Line - OCC&quot;, Owner:Queue.QueueName = &quot;SF Support 1st Line - SOHO&quot; )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise Case Notification Email Changed</fullName>
        <actions>
            <name>Update_Enterprise_Case_Notification_Emai</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2269 when the field displaying the correct email notification email changes, this wf rule will update a proper email field that is used for sending the notifications through email alerts.</description>
        <formula>(ISNEW() || ISCHANGED(Enterprise_Case_Notification_Email_Formu__c)) &amp;&amp; NOT(ISBLANK(Enterprise_Case_Notification_Email_Formu__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise Case%3A Set Queue Name</fullName>
        <actions>
            <name>Enterprise_Case_Set_Queue_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2186: puts the Queue name value into a text field for email template</description>
        <formula>RecordType.DeveloperName = &quot;Enterprise&quot; &amp;&amp;  LEFT(OwnerId ,3) = &quot;00G&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Case Queue %3D Business Design</fullName>
        <actions>
            <name>Set_Case_Group_to_Business_Design</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Category_field_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2247 when case queue is changed to business design we want to update field Case Group</description>
        <formula>ISCHANGED(Queue_Name__c) &amp;&amp;  Queue_Name__c=&quot;Business Design&quot; &amp;&amp;  NOT(ISCHANGED(Case_Group__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Case Queue %3D SAT</fullName>
        <actions>
            <name>Set_Case_Group_to_SAT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Category_field_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2247 when case queue is changed to SAT we want to update field Case Group</description>
        <formula>ISCHANGED(Queue_Name__c) &amp;&amp;  Queue_Name__c=&quot;SAT&quot; &amp;&amp;  NOT(ISCHANGED(Case_Group__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Case Queue %3D UC Presales</fullName>
        <actions>
            <name>Set_Case_Group_to_UC_Presales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF-1670 when case queue is changed to UC Presales we want to update field Case Group</description>
        <formula>ISCHANGED(Queue_Name__c) &amp;&amp; Queue_Name__c=&quot;UC Presales&quot; &amp;&amp;  NOT(ISCHANGED(Case_Group__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Case Queue %3D UC Sales Specialist</fullName>
        <actions>
            <name>Set_Case_Group_to_UC_Sales_Specialist</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF-1670 when case queue is changed to UC Sales Specialist we want to update field Case Group</description>
        <formula>ISCHANGED(Queue_Name__c) &amp;&amp; Queue_Name__c=&quot;UC Sales Specialist&quot; &amp;&amp;  NOT(ISCHANGED(Case_Group__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Case Queue %3D Volume Design</fullName>
        <actions>
            <name>Set_Case_Group_to_Volume_Design</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Category_field_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2247 when case queue is changed to volume design we want to update field Case Group</description>
        <formula>ISCHANGED(Queue_Name__c) &amp;&amp; Queue_Name__c=&quot;Volume Design&quot; &amp;&amp;  NOT(ISCHANGED(Case_Group__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Opportunity Confidential on Case</fullName>
        <actions>
            <name>Opportunity_Confidential_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2216: Updates the Case from the Opportuntiy when the Opp is first linked to the Case and is set to Confidential</description>
        <formula>RecordType.DeveloperName = &quot;Enterprise&quot; &amp;&amp;   Opportunity__r.Confidential__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise%3A Set Account%2FOpp Owner Role %26 Account Security Category</fullName>
        <actions>
            <name>Account_Opportunity_Owner_Role_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Security_Catgegory_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>2216: Copies the Account Security Categoty and Opportunity Owner Role for the Opportunity that the case is related to. if the Account/Opp are updated it will not trigger an update on the case. we dont want to use Process builder so edit on case only is ok</description>
        <formula>RecordType.DeveloperName = &quot;Enterprise&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>High priority Cases</fullName>
        <actions>
            <name>High_priority_Cases_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>Support 2nd Line</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Telia Company Support</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Sales Coordinator</fullName>
        <actions>
            <name>Notification_to_Sales_Coordinator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>SALEF 480, sends notification to Sales Coordinator when a User gets attached to a Case as Sales Coordinator</description>
        <formula>AND( NOT(ISBLANK(Named_Sales_Coordinator__c)), IsClosed = False )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify on Task Force Case Category</fullName>
        <actions>
            <name>Notify_on_Case_Type_Task_Force</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>SALEF 1115</description>
        <formula>AND( ISPICKVAL(Category__c,&apos;Task Force&apos;), RecordTypeId = &apos;01224000000LSYE&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StatusChange-1st line</fullName>
        <actions>
            <name>StatusChange_1st_line</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(Owner:Queue.QueueName = &quot;SF Support 1st Line - Enterprise&quot; ,    Owner:Queue.QueueName = &quot;SF Support 1st Line - Cygate&quot; ,    Owner:Queue.QueueName = &quot;SF Support 1st Line - OCC&quot; ,    Owner:Queue.QueueName = &quot;SF Support 1st Line - SOHO&quot; ), RecordType.DeveloperName=&quot;Telia_Company_Support&quot;, ISPICKVAL(PRIORVALUE(Status ),&quot;New&quot;),ISPICKVAL(Status ,&quot;In Progress&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>StatusChange-2nd Line</fullName>
        <actions>
            <name>ResponseTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName=&quot;Telia_Company_Support&quot; &amp;&amp; AND( OR(Owner:Queue.QueueName = &quot;SF Support 2nd Line - Enterprise&quot; , Owner:Queue.QueueName = &quot;SF Support 2nd Line - Cygate&quot; ,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - OCC&quot; ,    Owner:Queue.QueueName = &quot;SF Support 2nd Line - SOHO&quot;, Owner:Queue.QueueName = &quot;SF Support 2nd Line - Leads&quot; ), ISPICKVAL(PRIORVALUE(Status ),&quot;New&quot;), OR(ISPICKVAL(Status ,&quot;In Progress&quot;), ISPICKVAL(Status ,&quot;Effort Estimation&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
