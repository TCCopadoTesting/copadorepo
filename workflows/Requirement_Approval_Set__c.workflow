<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_submitter_about_approval_decision</fullName>
        <description>Send email to submitter about approval decision</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Requirement_Set_Approval_Decision_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_status_to_Approved</fullName>
        <description>SAEN-2138 when a Requirement Approaval Set record gets submitted for approval, and also gets approved, this field update will change the status field accordingly.</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Change status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_Rejected</fullName>
        <description>SAEN-2138 when a Requirement Approaval Set record gets submitted for approval, but it gets rejected, this field update will change the status field accordingly.</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Change status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_Submitted_for_Approval</fullName>
        <description>SAEN-2138 when a Requirement Approaval Set record gets submitted for approval this field update will change the status field accordingly.</description>
        <field>Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Change status to Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Approver_Name</fullName>
        <description>SAEN-2138 This will update the Solution Approver Name when record is approved.</description>
        <field>Solution_Approved_By__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Populate Approver Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_field_Initial_Submitter_Email</fullName>
        <description>SAEN-2138 when record is submitted, this field update will store the logged in users email adress in a field called Initial Submitter Email. That email will later be used when sending notifications after record has been approved or rejected.</description>
        <field>Initial_Submitter_Email__c</field>
        <formula>$User.Email</formula>
        <name>Populate field Initial Submitter Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
