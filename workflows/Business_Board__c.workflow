<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Business_Board_Notification_to_Business_Design</fullName>
        <ccEmails>pricing-entproductssolution@teliacompany.com</ccEmails>
        <description>Send Business Board Notification to Business Design</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Business_Board_Stage_Change_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Business_Board_Created_to_true</fullName>
        <description>SAEN-2323 this field update will set &quot;Business Board Created?&quot; to true.</description>
        <field>Business_Board_Created__c</field>
        <literalValue>1</literalValue>
        <name>Set Business Board Created to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Business Board Changed Stage</fullName>
        <actions>
            <name>Send_Business_Board_Notification_to_Business_Design</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Board__c.Stage__c</field>
            <operation>equals</operation>
            <value>Inför BB1</value>
        </criteriaItems>
        <description>saen-2384 when business board is changed to BB1 we will send an email to Business Design giving them a heads up about it.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Business Board Created</fullName>
        <actions>
            <name>Set_Business_Board_Created_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2323 when a new business board is created, we should populate &quot;Business Board Created?&quot; field on Opportunity so we dont create several BB&apos;s. That field controlles the validation rule.</description>
        <formula>Opportunity__r.Business_Board_Created__c=FALSE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
