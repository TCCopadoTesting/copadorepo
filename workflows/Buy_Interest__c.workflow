<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Date_Owner_Changed</fullName>
        <field>Date_Owner_Changed__c</field>
        <formula>NOW()</formula>
        <name>Date Owner Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_time_Status_Changed_from_Nytt</fullName>
        <field>Date_time_Status_Changed_from_New__c</field>
        <formula>NOW()</formula>
        <name>Date/time Status Changed from Nytt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_time_Status_Changed_from_Prequalify</fullName>
        <field>Date_time_Status_Changed_from_Prequalify__c</field>
        <formula>NOW()</formula>
        <name>Date/time Status Changed from Prequalify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_time_Status_Changed_from_Qualify</fullName>
        <field>Date_time_Status_Changed_from_Qualify__c</field>
        <formula>NOW()</formula>
        <name>Date/time Status Changed from Qualify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_time_Status_Changed_from_Returned</fullName>
        <field>Date_time_Status_Changed_from_Returned__c</field>
        <formula>NOW()</formula>
        <name>Date/time Status Changed from Returned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_When_Set_to_Prequalify</fullName>
        <description>1840 - for lead reporting to see who the owner is when the lead is set to prequalify</description>
        <field>Owner_at_Prequalify__c</field>
        <formula>Owner:User.FirstName &amp; &quot; &quot; &amp; Owner:User.LastName</formula>
        <name>Owner When Set to Prequalify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Store_TAM_Owner_Name</fullName>
        <description>SAEN-2119 This workflow field update populates the field Buy_Interest.TAM_User_Owning_Buy_Interest__c with the owner name. The name will later be used in reporting.</description>
        <field>Tam_User_Owning_Buy_Interest__c</field>
        <formula>Owner:User.FirstName + &quot; &quot; + Owner:User.LastName</formula>
        <name>Store TAM Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timestamp_for_Owner_TAM_user</fullName>
        <description>SAEN-2119 timestamp set when owner is changed from TAM queue to TAM user.</description>
        <field>Date_time_Owner_Changed_to_TAM_User__c</field>
        <formula>NOW()</formula>
        <name>Timestamp for Owner = TAM user</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Date Owner Changed</fullName>
        <actions>
            <name>Date_Owner_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1840. this workflow sets the value in the Date_Owner_Changed__c field.</description>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date%2Ftime Status Changed from New</fullName>
        <actions>
            <name>Date_time_Status_Changed_from_Nytt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1840: Updates the Date/time Status Changed from New field so that we can track how long a Kopintresse is waiting to be processed</description>
        <formula>ISCHANGED(Status__c)&amp;&amp; ISBLANK( Date_time_Status_Changed_from_New__c )&amp;&amp; TEXT(PRIORVALUE(Status__c) ) = &quot;Nytt&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date%2Ftime Status Changed from Prequalify</fullName>
        <actions>
            <name>Date_time_Status_Changed_from_Prequalify</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1840: Updates the Date/time Status Changed from Prequalify field so that we can track how long a Kopintresse is waiting to be processed</description>
        <formula>ISCHANGED(Status__c)&amp;&amp; ISBLANK( Date_time_Status_Changed_from_Prequalify__c )&amp;&amp; TEXT(PRIORVALUE(Status__c) ) = &quot;Förkvalificera&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date%2Ftime Status Changed from Qualify</fullName>
        <actions>
            <name>Date_time_Status_Changed_from_Qualify</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1840: Updates the Date/time Status Changed from Qualify field so that we can track how long a Kopintresse is waiting to be processed</description>
        <formula>ISCHANGED(Status__c)&amp;&amp; ISBLANK( Date_time_Status_Changed_from_Qualify__c )&amp;&amp; TEXT(PRIORVALUE(Status__c) ) = &quot;Kvalificera&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date%2Ftime Status Changed from Returned</fullName>
        <actions>
            <name>Date_time_Status_Changed_from_Returned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1840: Updates the Date/time Status Changed from Returned field so that we can track how long a Kopintresse is waiting to be processed</description>
        <formula>ISCHANGED(Status__c)&amp;&amp; ISBLANK( Date_time_Status_Changed_from_Returned__c )&amp;&amp; TEXT(PRIORVALUE(Status__c) )  = &quot;Returnerad&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date%2Ftime owner changed to TAM user</fullName>
        <actions>
            <name>Store_TAM_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timestamp_for_Owner_TAM_user</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAEN-2119 timestamp set when Buy Interest owner changes from TAM queue to TAM user.</description>
        <formula>PRIORVALUE(TAM_Owner_or_TAM_Owner_Profile__c)=&quot;TAM&quot; &amp;&amp; TAM_Owner_or_TAM_Owner_Profile__c=&quot;Telia Sales - SME&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set to Prequalify</fullName>
        <actions>
            <name>Owner_When_Set_to_Prequalify</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1840. used for lead reporting to show who the owner of the lead was when the lead is set to Prequalify</description>
        <formula>ISPICKVAL(Status__c ,&quot;Förkvalificera&quot;) &amp;&amp;   BEGINS( OwnerId  , &quot;005&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
