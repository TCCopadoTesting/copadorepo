<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>X313_Set_Continuation_Sales_Commit</fullName>
        <field>Continuation_Sales_Commit__c</field>
        <formula>Continuation_Sales_Target__c</formula>
        <name>313 Set Continuation Sales Commit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X313_Set_New_Sales_Commit</fullName>
        <field>New_Sales_Commit__c</field>
        <formula>New_Sales_Target__c</formula>
        <name>313 Set New Sales Commit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>313 - Set initial New Sales Commit</fullName>
        <actions>
            <name>X313_Set_Continuation_Sales_Commit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>X313_Set_New_Sales_Commit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a weekly forecast item record is created, the CS/NS Sales target value is set as the default value for the fields Commit Continuation Sales and Commit New Sales
Edited for Case 2685 and 2746 so that the workflow does not work for SOHO PR &amp; SOHO DS.</description>
        <formula>AND( NOT(ISBLANK(New_Sales_Target__c)), NOT(ISBLANK(Continuation_Sales_Target__c)), NOT(OR( (Seller__r.User__r.Profile.Name  = &apos;Telia Sales - SOHO PR&apos;), (CONTAINS(Seller__r.User__r.UserRole.Name,&apos;Direktsälj&apos;) ) ) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
