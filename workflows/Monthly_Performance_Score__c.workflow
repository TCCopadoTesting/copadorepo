<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SUM_Product_and_Service_to_MonthlyTarget</fullName>
        <description>SALEF - 873</description>
        <field>Sales_Revenue_Target__c</field>
        <formula>Product_Sales_Revenue_Target__c +  Services_Sales_Revenue_Target__c</formula>
        <name>SUM Product and Service to MonthlyTarget</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Client_Meetings_Target_Manually_true</fullName>
        <description>Sets the checbox Client Meetings Target Manually Set to true.</description>
        <field>Client_Meetings_Target_Manually_Set__c</field>
        <literalValue>1</literalValue>
        <name>Set Client Meetings Target Manually true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PrSales_Revenue_Target_Manually_true</fullName>
        <description>Sets the checbox Product Sales Revenue Target Manually Set to true.</description>
        <field>Product_Sales_Revenue_Target_ManuallySet__c</field>
        <literalValue>1</literalValue>
        <name>Set PrSales Revenue Target Manually true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sales_Revenue_Target_Manually_true</fullName>
        <description>Sets the checbox Sales Revenue Target Manually Set to true.</description>
        <field>Sales_Revenue_Target_Manually_Set__c</field>
        <literalValue>1</literalValue>
        <name>Set Sales Revenue Target Manually true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SeSales_Revenue_Target_Manually_true</fullName>
        <description>Sets the checbox Service Sales Revenue Target Manually Set to true.</description>
        <field>Service_Sales_Revenue_Target_ManuallySet__c</field>
        <literalValue>1</literalValue>
        <name>Set SeSales Revenue Target Manually true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Update_Client_Meetings_From_Rollup</fullName>
        <description>Sets the checkbox Update Client Meetings From Rollup on Yearly Performance Score to true.</description>
        <field>Update_Client_Meetings_From_Rollup__c</field>
        <literalValue>1</literalValue>
        <name>Set Update Client Meetings From Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Yearly_Performance_Score__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Update_PrSales_Revenue_From_Rollup</fullName>
        <description>Sets the checkbox Update Sales Revenue From Rollup on Yearly Performance Score to true.</description>
        <field>Update_Product_Sales_Revenue_From_Rollup__c</field>
        <literalValue>1</literalValue>
        <name>Set Update PrSales Revenue From Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Yearly_Performance_Score__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Update_Sales_Revenue_From_Rollup</fullName>
        <description>Sets the checkbox Update Sales Revenue From Rollup on Yearly Performance Score to true.</description>
        <field>Update_Sales_Revenue_From_Rollup__c</field>
        <literalValue>1</literalValue>
        <name>Set Update Sales Revenue From Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Yearly_Performance_Score__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Update_SeSales_Revenue_From_Rollup</fullName>
        <description>Sets the checkbox Update Service Sales Revenue From Rollup on Yearly Performance Score to true.</description>
        <field>Update_ServiceSales_Revenue_From_Rollup__c</field>
        <literalValue>1</literalValue>
        <name>Set Update SeSales Revenue From Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Yearly_Performance_Score__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MPS</fullName>
        <description>salef 1196</description>
        <field>QPS_Update_Helper_Checkbox__c</field>
        <literalValue>1</literalValue>
        <name>Update MPS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MP%3ATotal target sum of Product and Service</fullName>
        <actions>
            <name>SUM_Product_and_Service_to_MonthlyTarget</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Sales_Revenue_Target_Manually_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Update_Sales_Revenue_From_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF - 873</description>
        <formula>AND( NOT(ISNEW()), OR( ISCHANGED( Product_Sales_Revenue_Target__c ), ISCHANGED( Services_Sales_Revenue_Target__c ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Meetings Manual to true</fullName>
        <actions>
            <name>Set_Client_Meetings_Target_Manually_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Update_Client_Meetings_From_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Client Meetings Target Manually Set to true, and forces an update of the Client Meetings Target on the Yearly Performance Score when Client Meetings target on month is edited (i.e. not equal to 1/12 of the yearly Client Meetings Target)</description>
        <formula>ROUND(Client_Meetings_Target__c, 0) &lt;&gt; ROUND((Yearly_Performance_Score__r.Client_Meetings_Target__c/ 12), 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Product Sales Revenue Manual to true</fullName>
        <actions>
            <name>Set_PrSales_Revenue_Target_Manually_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Update_PrSales_Revenue_From_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Sales Revenue Target Manually Set to true, and forces an update of the Sales Revenue Target on the Yearly Performance Score when Sales Revenue target on month is edited (i.e. not equal to 1/12 of the yearly Sales Revenue)</description>
        <formula>ROUND(Product_Sales_Revenue_Target__c, 0)  &lt;&gt;  ROUND(( Yearly_Performance_Score__r.Product_Sales_Revenue_Target__c / 12), 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Sales Revenue Manual to true</fullName>
        <actions>
            <name>Set_Sales_Revenue_Target_Manually_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Update_Sales_Revenue_From_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets the Sales Revenue Target Manually Set to true, and forces an update of the Sales Revenue Target on the Yearly Performance Score when Sales Revenue target on month is edited (i.e. not equal to 1/12 of the yearly Sales Revenue)</description>
        <formula>ROUND(Sales_Revenue_Target__c, 0)  &lt;&gt;  ROUND((Yearly_Performance_Score__r.Sales_Revenue_Target__c / 12), 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Service Sales Revenue Manual to true</fullName>
        <actions>
            <name>Set_SeSales_Revenue_Target_Manually_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Update_SeSales_Revenue_From_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Service Sales Revenue Target Manually Set to true, and forces an update of the ServiceSales Revenue Target on the Yearly Performance Score when Service Sales Revenue target on month is edited (i.e.not equal to 1/12 of the yearly SerSales Revenue)</description>
        <formula>ROUND( Services_Sales_Revenue_Target__c , 0)  &lt;&gt;  ROUND((  Yearly_Performance_Score__r.Services_Sales_Revenue_Target__c / 12), 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update MPS for QPS Update</fullName>
        <active>false</active>
        <description>SALEF - 1196</description>
        <formula>ISBLANK(Quarterly_Performance_Score__c)</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_MPS</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Monthly_Performance_Score__c.QPS_Update_Helper__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
