<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PPC_Update_MBCM_ID_Unique</fullName>
        <description>PPC Update Field for Workflow Rule</description>
        <field>Unique_ID__c</field>
        <formula>Name</formula>
        <name>PPC Update MBCM ID Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PPC Set MBCM ID Unique</fullName>
        <actions>
            <name>PPC_Update_MBCM_ID_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mobility_Cost_margin__c.Unique_ID__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>PPC Workflow Rule to Update Mobility Cost Margin Records.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
