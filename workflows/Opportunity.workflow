<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reminder_Opportunity</fullName>
        <description>Reminder Opportunity</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Reminder_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>T_Model_Aff_rsm_jlighet_Opportunity_Notification_for_180_days_left</fullName>
        <description>T-Model Affärsmöjlighet Opportunity Notification for 180 days left</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/T_Model_Opportunity_Notification_for_180_days_left</template>
    </alerts>
    <alerts>
        <fullName>T_Model_Aff_rsm_jlighet_Opportunity_Notification_for_365_days_left</fullName>
        <description>T-Model Affärsmöjlighet Opportunity Notification for 365 days left</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/T_Model_Opportunity_Notification_for_365_days_left</template>
    </alerts>
    <alerts>
        <fullName>T_Model_Aff_rsm_jlighet_Opportunity_Notification_for_90_days_left</fullName>
        <description>T-Model Affärsmöjlighet Opportunity Notification for 90 days left</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/T_Model_Opportunity_Notification_for_90_days_left</template>
    </alerts>
    <fieldUpdates>
        <fullName>Close_Date_update_lost_broken_Opp</fullName>
        <description>updated the close date to today when an opportunity is  lost or broken.</description>
        <field>CloseDate</field>
        <formula>today()</formula>
        <name>Close Date update lost/ broken Opp.</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Converted_from_Prospect</fullName>
        <field>Converted_from_prospect__c</field>
        <literalValue>1</literalValue>
        <name>Converted from Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_First_Set_to_Closed_Won</fullName>
        <field>Date_First_Set_to_Closed_Won__c</field>
        <formula>TODAY()</formula>
        <name>Date First Set to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Order_Account_Id_on_Opportunity</fullName>
        <field>Order_Account_Id__c</field>
        <formula>OrganisationalUnit__r.Order_Account_Id__c</formula>
        <name>Populate Order Account Id on Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Probability_Update_for_F_rlorad_Avbruten</fullName>
        <description>SALEF - 1125</description>
        <field>Probability</field>
        <formula>0</formula>
        <name>Probability Update for Förlorad Avbruten</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Probability_Update_for_Vunnen</fullName>
        <description>SALEF - 1125</description>
        <field>Probability</field>
        <formula>1</formula>
        <name>Probability Update for Vunnen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Owner_Role</fullName>
        <field>Account_Owner_Role__c</field>
        <formula>Account.Owner.UserRole.Name</formula>
        <name>Set Account Owner Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Security_Category</fullName>
        <field>Account_Security_Category__c</field>
        <formula>TEXT(Account.Security_Category__c)</formula>
        <name>Set Account Security Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Email</fullName>
        <description>Fetched from opp owners user record. Used for Conga purposes-</description>
        <field>Conga_Opportunity_Owner_Email__c</field>
        <formula>Owner.Email</formula>
        <name>Set Conga Opp Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Mobile</fullName>
        <field>Conga_Opportunity_Owner_Mobile__c</field>
        <formula>Owner.MobilePhone</formula>
        <name>Set Conga Opp Owner Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Phone</fullName>
        <description>Fetches the users phone nr from user record</description>
        <field>Conga_Opportunity_Owner_Phone__c</field>
        <formula>Owner.Phone</formula>
        <name>Set Conga Opp Owner Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conga_Opp_Owner_Title</fullName>
        <field>Conga_Opportunity_Owner_Title__c</field>
        <formula>Owner.Title</formula>
        <name>Set Conga Opp Owner Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Date_Quote_Sent</fullName>
        <field>Date_Quote_Sent__c</field>
        <formula>TODAY()</formula>
        <name>Set Date Quote Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Delivery_Date_of_Cygate_Opportunity</fullName>
        <description>Case 2414 - Setting expected delivery date 3 months ahead from created date</description>
        <field>Expected_Delivery_Date__c</field>
        <formula>CreatedDate + 90</formula>
        <name>Set Delivery Date of Cygate Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Expected_Conversion_Date</fullName>
        <description>Using the value of &quot;Startdatum för affärsdialog&quot; from the lead</description>
        <field>Expected_Conversion_Date__c</field>
        <formula>Lead_Business_Dialogue_Start_Date__c</formula>
        <name>Set Expected Conversion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Is_Contract_Prospect_Opportunity</fullName>
        <description>Setting field &apos;Is_Contract_Prospect_Opportunity__c&apos; to false</description>
        <field>Is_Contract_Prospect_Opportunity__c</field>
        <literalValue>0</literalValue>
        <name>Set &apos;Is_Contract_Prospect_Opportunity__&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Close_Date_From_Lead_Date</fullName>
        <description>Set the Opportunity Close Date to be 90 days after the Lead Business Dialogue Start Date. If this date is less than today, today will be set as the close date.</description>
        <field>CloseDate</field>
        <formula>IF( Lead_Business_Dialogue_Start_Date__c + 90 &gt; TODAY(),
 Lead_Business_Dialogue_Start_Date__c + 90,
 TODAY()
)</formula>
        <name>Set Opp Close Date From Lead Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Owner_Role</fullName>
        <field>Opportunity_Owner_Role__c</field>
        <formula>Owner.UserRole.Name</formula>
        <name>Set Opportunity Owner Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Record_Type_to_Cygate_F</fullName>
        <description>Updates the record type to &quot;Cygate Förenklad&quot; if expected opp type = &quot;Cygate förenklad affärsmöjlighet&quot; (value set when user clicks action &quot;Convert to Cygate förenklad affärsmöjlighet&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Cygate_Utokning</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opportunity Record Type to Cygate Fö</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Aktivt_prospect</fullName>
        <field>StageName</field>
        <literalValue>Aktivt prospect</literalValue>
        <name>Set Stage to Aktivt prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Kvalificera</fullName>
        <field>StageName</field>
        <literalValue>Kvalificera</literalValue>
        <name>Set Stage to Kvalificera</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_record_type_to_aff_rsm_jlighet</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cygate_Standard_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set record type to affärsmöjlighet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_record_type_to_prospect</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Prospect_process</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set record type to prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Aktivt_prospect</fullName>
        <field>StageName</field>
        <literalValue>Aktivt prospect</literalValue>
        <name>Stage=Aktivt prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Vilande_prospect</fullName>
        <field>StageName</field>
        <literalValue>Vilande prospect</literalValue>
        <name>Stage=Vilande prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tidpunkt_f_r_konvertering</fullName>
        <field>Expected_Conversion_Date__c</field>
        <formula>TODAY()</formula>
        <name>Tidpunkt för konvertering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CloseDate_on_isClosed</fullName>
        <description>Update CloseDate when Opportunity get&apos;s closed.</description>
        <field>CloseDate</field>
        <formula>today()</formula>
        <name>Update CloseDate on isClosed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date_for_Prospect</fullName>
        <field>CloseDate</field>
        <formula>TODAY() + 90</formula>
        <name>Update Close Date for Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Large_Continuation_Sales</fullName>
        <field>Large_Continuation_Sales__c</field>
        <formula>Retain_Revenue_12_month__c</formula>
        <name>Update Large Continuation Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Large_New_Sales</fullName>
        <field>Large_New_Sales__c</field>
        <formula>Winback_Revenue_12_month__c</formula>
        <name>Update Large New Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Max_Revenue_solution_area</fullName>
        <description>Update Max Revenue solution area</description>
        <field>Max_Revenue_Solution_Area__c</field>
        <formula>IF (Sales_IBS__c  &gt; ( Max( Sales_Other__c , Sales_UC__c , Sales_CN__c  , Sales_Services__c, Sales_MMBA__c  )),&quot;IBS&quot;,

IF (Sales_UC__c &gt; ( Max( Sales_Other__c , Sales_CN__c , Sales_IBS__c , Sales_Services__c,Sales_MMBA__c )),&quot;Unified and Mobile Communications&quot;,

IF( Sales_CN__c &gt; ( Max( Sales_Other__c , Sales_UC__c  , Sales_IBS__c , Sales_Services__c, Sales_MMBA__c )),&quot;Cloud Networking&quot;,

IF( Sales_Services__c   &gt; ( Max( Sales_Other__c , Sales_UC__c , Sales_CN__c , Sales_IBS__c ,Sales_MMBA__c )),&quot;Services&quot;,

IF( Sales_MMBA__c   &gt; ( Max( Sales_Other__c , Sales_UC__c , Sales_CN__c , Sales_IBS__c , Sales_Services__c )),&quot;Mobility Management and Business App&quot;,


&quot;Other&quot;)))))</formula>
        <name>Update Max Revenue solution area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_No_External_ID</fullName>
        <field>Opportunity_No_External_ID__c</field>
        <formula>Opportunity_id__c</formula>
        <name>Update Opportunity No (External ID)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Original_Close_Date</fullName>
        <field>Original_Closed_Date__c</field>
        <formula>CloseDate</formula>
        <name>Update Original Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Stage</fullName>
        <field>Previous_stage__c</field>
        <formula>TEXT(PRIORVALUE( StageName ))</formula>
        <name>Update  Previous Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_change_date</fullName>
        <field>Stage_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Stage change date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Vunnen_Time_Stamp_on_Oppty</fullName>
        <field>Vunnen_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Update Vunnen Time Stamp on Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_WS_enligt_T_Modellen</fullName>
        <field>WS_enligt_T_Modellen__c</field>
        <literalValue>1</literalValue>
        <name>Update WS enligt T-Modellen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Year_to_Win</fullName>
        <field>Year_Must_Win__c</field>
        <formula>TEXT(YEAR(TODAY()))</formula>
        <name>Update Year to Win</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_conversion_date</fullName>
        <field>Prospect_conversion_date__c</field>
        <formula>TODAY()</formula>
        <name>Update record type conversion date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Create a task in %22Sluta avtal%22</fullName>
        <actions>
            <name>Win_loss_analys_tillsammans_med_kund</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>CE/TE/SpS creates a task in the &quot;Sluta avtal&quot; phase to do the win/loss analysis together with the customer if the opportunity value excessing &gt;1MSEK</description>
        <formula>ISPICKVAL(StageName,&quot;Sluta avtal&quot;) &amp;&amp; ( RecordType.DeveloperName = &quot;Large_Standard_process&quot; ||  RecordType.DeveloperName = &quot;Ut_kning_process&quot; ) &amp;&amp; Large_New_Sales__c + Large_Continuation_Sales__c &gt;1000000</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Date First Set to Closed Won</fullName>
        <actions>
            <name>Date_First_Set_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Vunnen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Date_First_Set_to_Closed_Won__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>1890 - sets the &quot;Date First Set to Closed Won&quot; to be the date that the opp is set to Closed Won, the first time only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Förenklad Opp Notification after 5 days</fullName>
        <active>false</active>
        <description>Notify the Account Manager 5 days after the opportunity is created if it is still in stage Sluta avtal</description>
        <formula>ISPICKVAL(StageName,&apos;Sluta avtal&apos;) &amp;&amp; RecordType.DeveloperName = &apos;SME_Quick&apos; &amp;&amp;  Owner.Profile.Name != &apos;API User&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Opportunity</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opportunity Stage Changed</fullName>
        <actions>
            <name>Update_Previous_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Stage_change_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When opportunity stage name is changed, copy the old stage value to the field Previous Stage and set the Stage Change Date to today.</description>
        <formula>ISCHANGED( StageName  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity record type changed</fullName>
        <actions>
            <name>Converted_from_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_record_type_conversion_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule criteria is that record type changes from Prospect rec type to another (opportunity) rec type.
Sets the Converted from prospect checkbox to true and the Prospect converted date to today.</description>
        <formula>ISCHANGED( RecordTypeId  ) &amp;&amp;  PRIORVALUE(RecordTypeId )= &quot;01224000000AXKo&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Order Account Id</fullName>
        <actions>
            <name>Populate_Order_Account_Id_on_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an Opportunity is linked to an Organisation Unit, copy the value for Order Account Id from the org unit to the opportunity.</description>
        <formula>NOT(ISBLANK( OrganisationalUnit__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Probability Update %3A For Förlorad And Avbruten</fullName>
        <actions>
            <name>Probability_Update_for_F_rlorad_Avbruten</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF 1125</description>
        <formula>AND( OR( ISPICKVAL(StageName,&apos;Förlorad&apos;),  ISPICKVAL(StageName,&apos;Avbruten&apos;) ), Probability&lt;&gt;0 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Probability Update %3A For Vunnen</fullName>
        <actions>
            <name>Probability_Update_for_Vunnen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF - 1125</description>
        <formula>AND( ISPICKVAL(StageName,&apos;Vunnen&apos;), Probability &lt;&gt; 1 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prospect Conversion - Less than 3 months</fullName>
        <actions>
            <name>Stage_Aktivt_prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For prospects, if the field Expected Conversion Date is less than 90 days from today, set the stage to &quot;Aktivt Prospect&quot;</description>
        <formula>RecordType.DeveloperName = &quot;Prospect_process&quot; &amp;&amp; Expected_Conversion_Date__c &lt; TODAY()+90&amp;&amp; Expected_Conversion_Date__c &gt; TODAY()&amp;&amp; IsClosed =FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prospect Conversion Reminder - 3 months</fullName>
        <actions>
            <name>Stage_Vilande_prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For prospects, if the field Expected Conversion Date is more than 90 days from today, set the stage to &quot;Vilande Prospect&quot;. Also adds a time based update that triggers 90 days before the conv. date that sets stage to &quot;Aktivt&quot; and assigns task to opp owner.</description>
        <formula>RecordType.DeveloperName = &quot;Prospect_process&quot; &amp;&amp;  Expected_Conversion_Date__c   &gt;  TODAY()+90&amp;&amp;  IsClosed =FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Stage_Aktivt_prospect</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>omvandlings_p_minnelse</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Expected_Conversion_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Reminder Task - Rule Lead in Alpha</fullName>
        <actions>
            <name>Update_Lead_In_Alpha</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Lead_in_Alpha__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Lead in Alpha checkbox is checked, creates a task assigned to the opp owner to update the lead in Alpha.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SOHO PR Date Quote Sent</fullName>
        <actions>
            <name>Set_Date_Quote_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Quote_Sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Soho PR förenklad affärsmöjlighet,Soho PR standard affärsmöjlighet</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SOHO PR Quote Follow Up Meeting</fullName>
        <actions>
            <name>Planmeetingforquotefeedback</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Quote_Sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Soho PR förenklad affärsmöjlighet,Soho PR standard affärsmöjlighet</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Telia Sales - SOHO PR</value>
        </criteriaItems>
        <description>This workflow rule assigns a task to the user to remind them to contact the client after the quote has been sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Owner Role</fullName>
        <actions>
            <name>Set_Account_Owner_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Account_Security_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opportunity_Owner_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(IsClosed = FALSE) || (ISNEW() &amp;&amp; IsClosed = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Default Expected Delivery Date for Cygate Opportunity</fullName>
        <actions>
            <name>Set_Delivery_Date_of_Cygate_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 2414 - Set a default value on expected delivery date in cygate opportunities</description>
        <formula>(RecordType.DeveloperName = &apos;Cygate_Standard_Opportunity&apos;) &amp;&amp; ISNULL(  Expected_Delivery_Date__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Close Date Upon Lead Conversion</fullName>
        <actions>
            <name>Set_Expected_Conversion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opp_Close_Date_From_Lead_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a lead is converted to an opportunity, the field Lead Business Dialogue Start Date will be set. This WF-rule updates the Opoortunity Close Date to be 90 days + the Lead Business Dialogue Start Date.</description>
        <formula>NOT(ISNULL(Lead_Business_Dialogue_Start_Date__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Record Type to Affärsmöjlighet</fullName>
        <actions>
            <name>Set_Stage_to_Kvalificera</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_record_type_to_aff_rsm_jlighet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Expected_Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Cygate Affärsmöjlighet</value>
        </criteriaItems>
        <description>When Cygate converts a lead there is logic that calculates the wanted opportunity record type, and maps that value to the field Expected Opportuntity Type. So when that value is populated, this workflow change the Opp record type to the expected one.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Record Type to Cygate Förenklad</fullName>
        <actions>
            <name>Set_Opportunity_Record_Type_to_Cygate_F</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Expected_Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Cygate förenklad affärsmöjlighet</value>
        </criteriaItems>
        <description>Used when converting a prospekt to a Cygate förenklad.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Record Type to Prospect</fullName>
        <actions>
            <name>Set_Stage_to_Aktivt_prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_record_type_to_prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Expected_Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Prospekt</value>
        </criteriaItems>
        <description>When Cygate converts a lead there is logic that calculates the wanted opportunity record type, and maps that value to the field Expected Opportuntity Type. So when that value is populated, this workflow change the Opp record type to the expected one.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Standard Opp Notification after 20 days</fullName>
        <active>false</active>
        <description>Notify the Account Manager 20 days after the opportunity is created if it is still in stage Kvalificera or Utveckla</description>
        <formula>(ISPICKVAL(StageName,&apos;Kvalificera&apos;) || ISPICKVAL(StageName,&apos;Utveckla&apos;)) &amp;&amp;  RecordType.DeveloperName = &apos;SME_standard&apos; &amp;&amp; Owner.Profile.Name != &apos;API User&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Opportunity</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Standard Opp Notification after 20 days - 2</fullName>
        <active>false</active>
        <description>Notify the Account Manager 20 days after the opportunity enters stage Offerera &amp; Förhandla or Sluta avtal if it is still in one of those stages</description>
        <formula>(ISPICKVAL(StageName,&apos;Offerera och förhandla&apos;) || ISPICKVAL(StageName,&apos;Sluta avtal&apos;)) &amp;&amp;  RecordType.DeveloperName = &apos;SME_standard&apos; &amp;&amp; Owner.Profile.Name != &apos;API User&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Opportunity</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>T - Model when Close date %3C%3D 90 %3F</fullName>
        <actions>
            <name>T_Model_Aff_rsm_jlighet_Opportunity_Notification_for_90_days_left</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &quot;Large_Standard_process&quot; &amp;&amp; Turn_Off_T_Model__c = false &amp;&amp; (Large_New_Sales__c + Large_Continuation_Sales__c &gt;=5000000.00) &amp;&amp; WS_enligt_T_Modellen__c = true &amp;&amp; ((CloseDate - DATEVALUE(CreatedDate))&lt;= 90 ) &amp;&amp;  ((Owner.UserRoleId != null) &amp;&amp;  (CONTAINS( Owner.UserRole.Name , &apos;Large Corp North&apos;) || CONTAINS( Owner.UserRole.Name , &apos;Large Corp South&apos;) ||  CONTAINS( Owner.UserRole.Name , &apos;Large Key Accounts&apos;) || CONTAINS( Owner.UserRole.Name , &apos;Large Attack Team&apos;)||  CONTAINS( Owner.UserRole.Name , &apos;Large Manufacturing&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>T - Model when Close date %3E 180 %26 Close Date %3C%3D 365</fullName>
        <actions>
            <name>T_Model_Aff_rsm_jlighet_Opportunity_Notification_for_365_days_left</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &quot;Large_Standard_process&quot; &amp;&amp;  Turn_Off_T_Model__c = false &amp;&amp; (Large_New_Sales__c + Large_Continuation_Sales__c &gt;=5000000.00) &amp;&amp; WS_enligt_T_Modellen__c = true &amp;&amp; ((( CloseDate - DATEVALUE( CreatedDate)) &gt; 180) &amp;&amp;  (( CloseDate - DATEVALUE( CreatedDate)) &lt;= 365)) &amp;&amp;  ((Owner.UserRoleId != null) &amp;&amp;  (CONTAINS( Owner.UserRole.Name , &apos;Large Corp North&apos;) || CONTAINS( Owner.UserRole.Name , &apos;Large Corp South&apos;) ||  CONTAINS( Owner.UserRole.Name , &apos;Large Key Accounts&apos;) || CONTAINS( Owner.UserRole.Name , &apos;Large Attack Team&apos;)||  CONTAINS( Owner.UserRole.Name , &apos;Large Manufacturing&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>T - Model when Close date %3E 90 %26 Close date %3C%3D 180 days</fullName>
        <actions>
            <name>T_Model_Aff_rsm_jlighet_Opportunity_Notification_for_180_days_left</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &quot;Large_Standard_process&quot; &amp;&amp;  Turn_Off_T_Model__c = false &amp;&amp; (Large_New_Sales__c + Large_Continuation_Sales__c &gt;=5000000.00) &amp;&amp; WS_enligt_T_Modellen__c = true &amp;&amp; ((( CloseDate - DATEVALUE( CreatedDate)) &gt; 90) &amp;&amp;  (( CloseDate - DATEVALUE( CreatedDate)) &lt;= 180)) &amp;&amp;  ((Owner.UserRoleId != null) &amp;&amp;  (CONTAINS( Owner.UserRole.Name , &apos;Large Corp North&apos;) || CONTAINS( Owner.UserRole.Name , &apos;Large Corp South&apos;) ||  CONTAINS( Owner.UserRole.Name , &apos;Large Key Accounts&apos;) || CONTAINS( Owner.UserRole.Name , &apos;Large Attack Team&apos;)||  CONTAINS( Owner.UserRole.Name , &apos;Large Manufacturing&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>T- Model for Affärsmöjlighet Opportunity</fullName>
        <actions>
            <name>Update_WS_enligt_T_Modellen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &apos;Large_Standard_process&apos; &amp;&amp;  Turn_Off_T_Model__c = false &amp;&amp; ((Large_Continuation_Sales__c + Large_New_Sales__c) &gt;= 5000000.00) &amp;&amp; OwnerId != null &amp;&amp;  (  CONTAINS(Owner.UserRole.Name, &apos;Large Corp North&apos;) || CONTAINS(Owner.UserRole.Name, &apos;Large Corp South&apos;) ||  CONTAINS(Owner.UserRole.Name, &apos;Large Key Accounts&apos;)||  CONTAINS(Owner.UserRole.Name, &apos;Large Attack Team&apos;) ||  CONTAINS(Owner.UserRole.Name, &apos;Large Manufacturing&apos;)  ) &amp;&amp; WS_enligt_T_Modellen__c == false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tidpunkt för konvertering</fullName>
        <actions>
            <name>Tidpunkt_f_r_konvertering</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Tidpunkt för konvertering with the actual date that the Prospect was converted to a Large Opportunity record type</description>
        <formula>ISCHANGED( RecordTypeId ) &amp;&amp; PRIORVALUE( RecordTypeId  ) = &quot;01224000000AXKo&quot; &amp;&amp; RecordType.DeveloperName = &quot;Large_Standard_process&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Close Date</fullName>
        <actions>
            <name>Close_Date_update_lost_broken_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CloseDate_on_isClosed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This was previously setting the close date on closed/cancelled opportunities. But from some while back we covered this functionality via code instead, why 1. this workflow is inactive and 2. this workflow could potentially be removed in late 2016 if need.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Close Date of Opportunity for Prospect</fullName>
        <actions>
            <name>Update_Close_Date_for_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>UPDATE FROM SIMON H: had to add &quot;NOT(ISCHANGED(Expected_Opportunity_Type__c)&quot; because for some reason the workflow fired when my quick action for Cygate called &quot;Konvertera till förenklad&quot; changed the record type from prospekt to Cygate förenklad.</description>
        <formula>AND(  RecordType.DeveloperName = &quot;Prospect_process&quot;, ISCHANGED(RecordTypeId),  NOT(ISCHANGED(CloseDate)), NOT(ISCHANGED(Expected_Opportunity_Type__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Conga Info on Opportunity</fullName>
        <actions>
            <name>Set_Conga_Opp_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Conga_Opp_Owner_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Conga_Opp_Owner_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Conga_Opp_Owner_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is potentially used for Conga purposes. For now we use a custom Conga Query to get this information instead. The Conga solution is still not 100% set, but if we&apos;re going to use Conga Query in the future this workflow rule could be deleted.</description>
        <formula>(RecordType.DeveloperName = &quot;Cygate_Standard_Opportunity&quot; || RecordType.DeveloperName = &quot;Cygate_Efterregistrering&quot; || RecordType.DeveloperName = &quot;Cygate_Utokning&quot;) &amp;&amp; ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Is_Contract_Prospect_Opportunity%5F%5Fc</fullName>
        <actions>
            <name>Set_Is_Contract_Prospect_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Is_Contract_Prospect_Opportunity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>To set &apos;Is_Contract_Prospect_Opportunity__c&apos; to false so that validation rule &apos;Amounts_CS_or_NS_Greater_Than_0&apos; can fire on saving the record.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Large Continuation Sales Currency field</fullName>
        <actions>
            <name>Update_Large_Continuation_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow over-writes the value in the Large_Continuation_Sales field based on the products that are added</description>
        <formula>ISCHANGED(  Retain_Revenue_12_month__c  )|| ( ISCHANGED( Large_Continuation_Sales__c ) &amp;&amp; NOT(Retain_Revenue_12_month__c=0))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Large New Sales Currency field</fullName>
        <actions>
            <name>Update_Large_New_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow over-writes the value in the Large_New_Sales field based on the products that are added</description>
        <formula>ISCHANGED( Winback_Revenue_12_month__c )|| ( ISCHANGED(Large_New_Sales__c) &amp;&amp; NOT(Winback_Revenue_12_month__c=0))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Large New%2FCont Sales Currency field</fullName>
        <actions>
            <name>Update_Large_Continuation_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Large_New_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow over-writes the value in the Large_New_Sales and Large_Continuation_Sales fields based on the products that are added</description>
        <formula>HasOpportunityLineItem =TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Max Revenue solution area</fullName>
        <actions>
            <name>Update_Max_Revenue_solution_area</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Max Revenue solution area when the product are added</description>
        <formula>ISCHANGED (Sales_CN__c) || ISCHANGED ( Sales_IBS__c ) || ISCHANGED ( Sales_MMBA__c )  || ISCHANGED ( Sales_Other__c ) || ISCHANGED ( Sales_Services__c ) ||  ISCHANGED ( Sales_UC__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity No %28External ID%29</fullName>
        <actions>
            <name>Update_Opportunity_No_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_No_External_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Pushes the autonumber for the opp into a unique external id field. This is to enable the requirement users to upload requirements with the data import wizard
2155</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Original Closed Date</fullName>
        <actions>
            <name>Update_Original_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNULL(CloseDate))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Vunnen Time Stamp</fullName>
        <actions>
            <name>Update_Vunnen_Time_Stamp_on_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SALEF-1114 - Updating the Vunnen Time stamp to bring 10 latest closed Oppty on Home page</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Vunnen&quot;), IsClosed == true,  ISNULL(Vunnen_Time_Stamp__c), (Owner.Profile.Name == &apos;Telia Sales - Large&apos;|| Owner.Profile.Name == &apos;Telia Sales - Service&apos;|| Owner.Profile.Name == &apos;Telia Sales - Solution&apos;||
Owner.Profile.Name == &apos;Telia Cygate - Sales&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Year to Win</fullName>
        <actions>
            <name>Update_Year_to_Win</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISPICKVAL(Must_win_new__c,&apos;None&apos;)), NOT(ISPICKVAL(Quarter_must_win__c,&apos;&apos;)), ISBLANK(Year_Must_Win__c), NOT(ISCHANGED(Year_Must_Win__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Planmeetingforquotefeedback</fullName>
        <assignedToType>owner</assignedToType>
        <description>- kontrollera att kunden har förstått citatet och öka möjligheten att vinna affären</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Öppen</status>
        <subject>Planera möte för feedback på offert</subject>
    </tasks>
    <tasks>
        <fullName>Update_Lead_In_Alpha</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Öppen</status>
        <subject>Uppdatera Lead i Alpha</subject>
    </tasks>
    <tasks>
        <fullName>Win_loss_analys_tillsammans_med_kund</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Öppen</status>
        <subject>Win/loss analys tillsammans med kund</subject>
    </tasks>
    <tasks>
        <fullName>omvandlings_p_minnelse</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Expected_Conversion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Öppen</status>
        <subject>omvandlings påminnelse</subject>
    </tasks>
</Workflow>
